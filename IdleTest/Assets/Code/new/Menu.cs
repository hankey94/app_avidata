﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    public Button buttonFacebook;
    public Button buttonInstagram;
    public Button buttonTwitter;

    // Opens the Facebook Page
    public void openFacebook() {
        StartCoroutine(waitForURL("https://www.facebook.com/idlefactorytycoon/", buttonFacebook));
    }

    // Opens the Twitter Page
    public void openTwitter() {
        StartCoroutine(waitForURL("https://twitter.com/idlefactory", buttonTwitter));
    }

    // Opens the Instagram Page
    public void openInstagram() {
        StartCoroutine(waitForURL("https://www.instagram.com/idlefactorytycoon/", buttonInstagram));
    }

    // Opens the Youtube Page
    public void openYoutube() {
        Application.OpenURL("https://www.youtube.com/channel/UCK6G-6BKEZuAmCBWhHFU94g");
    }

    // Wait and open the url
    IEnumerator waitForURL(string url, Button button) {
        button.GetComponent<Animator>().SetBool("click", true);
        yield return new WaitForSeconds(.25f);
        button.GetComponent<Animator>().SetBool("click", false);
        Application.OpenURL(url);
        StopCoroutine(waitForURL(url, button));
    }
}
