﻿using Spine.Unity;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class WorkerNew : MonoBehaviour {

    public GameObject band;
    public GameObject data;
    public GameObject arrowWorker;
    public GameObject elevator;
    public GameObject elevatorClick;
    private bool workingBool = false;
    private bool firstElevator = false;
    public float coinsEtage1Value;
    public Text coinseEtage1Text;

    public GameObject worker2; // dann worker1 auf x 0.765 und worker 2 auf 0.58
    public GameObject worker3; // 3 auf 0.375
    public GameObject worker4; // 4 auf 0.155

    void Start() {
        //PlayerPrefs.SetInt("automaticEtage1ON", 0);
    }

    void Update() {
        if (PlayerPrefs.GetInt("firstEvelator", 0) == 1 && !firstElevator) {
            firstElevator = true;
            arrowWorker.SetActive(false);
            elevator.SetActive(true);
            elevatorClick.SetActive(true);
        }

        // START AUTOMATIC INSTANCE AND DONT LET THEM TOUCH THE WORKERS
        if (PlayerPrefs.GetInt("automaticEtage1", 0) == 1) {
            workingBool = true;
            if (PlayerPrefs.GetInt("automaticEtage1ON", 0) == 0) {
                StartCoroutine(automaton());
                PlayerPrefs.SetInt("automaticEtage1ON", 1);
            }
        }

        if (worker2 == null) {
            worker2 = GameObject.Find("HARDWORKER (2)");
        }
        if (worker3 == null) {
            worker3 = GameObject.Find("HARDWORKER (3)");
        }
        if (worker4 == null) {
            worker4 = GameObject.Find("HARDWORKER (4)");
        }

        // 2b - 2nd worker
        if (PlayerPrefs.GetInt("levelEtage1", 1) >= 10) {
            worker2.SetActive(true);
        }
        // 3b - 3rd worker
        if (PlayerPrefs.GetInt("levelEtage1", 1) >= 50) {
            worker3.SetActive(true);
        }
        // 4b - 4th worker
        if (PlayerPrefs.GetInt("levelEtage1", 1) >= 100) {
            worker4.SetActive(true);
        }
    }

    // Call it on the first steps of the game to start production
    public void workerTutorialClick() {
        GameObject.Find("SFX-Control").GetComponent<AudioSource>().Play();
        if (!workingBool) {
            arrowWorker.SetActive(false);
            workingBool = true;
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor 1b";
            StartCoroutine(working());
            StartCoroutine(addMoney());
        }
    }

    // AUTOMATIC THREAD
    IEnumerator automaton() {
        // 2 worker setting
        if (PlayerPrefs.GetInt("levelEtage1") >= 100) {
            // worker4
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor 4b";
            yield return new WaitForSeconds(.75f);
            worker4.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(.3f);
            worker3.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(.3f);
            worker2.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(.3f);
            this.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(.8f);
            worker4.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            worker3.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            worker2.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            this.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            yield return new WaitForSeconds(.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            yield return new WaitForSeconds(1f);
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor idle";
            yield return new WaitForSeconds(1f);
        } else if (PlayerPrefs.GetInt("levelEtage1") >= 50) {
            // worker3
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor 3b";
            yield return new WaitForSeconds(1f);
            worker3.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(.3f);
            worker2.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(.3f);
            this.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(1f);
            worker3.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(.65f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            worker2.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            this.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor idle";
            yield return new WaitForSeconds(.5f);
        } else if (PlayerPrefs.GetInt("levelEtage1") >= 10) {
            // worker4
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor 2b";
            yield return new WaitForSeconds(1.25f);
            worker2.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(.5f);
            this.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(1f);
            worker2.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(.5f);
            this.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            yield return new WaitForSeconds(.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor idle";
            yield return new WaitForSeconds(.5f);
        } else {
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor 1b";
            yield return new WaitForSeconds(.666f);
            this.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
            yield return new WaitForSeconds(1.5f);
            this.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
            yield return new WaitForSeconds(1.5f);
            coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);
            coinseEtage1Text.text = coinsEtage1Value.ToString();
            band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor idle";
            yield return new WaitForSeconds(.525f);
        }
        StartCoroutine(automaton());
    }


    // ----------------- TUTORIAL CLICKER THREAD STUFF ----------------- //

    // Wait to work
    IEnumerator working() {
        // 1 Bottle
        yield return new WaitForSeconds(.666f);
        this.GetComponent<SkeletonAnimation>().AnimationName = "Lemon squeeze";
        yield return new WaitForSeconds(1.5f);
        this.GetComponent<SkeletonAnimation>().AnimationName = "Idle";
        yield return new WaitForSeconds(1.5f);
        band.GetComponent<SkeletonAnimation>().AnimationName = "conveyor idle";
        PlayerPrefs.SetInt("firstEvelator", 1);
        PlayerPrefs.Save();
        yield return new WaitForSeconds(.5f);
        workingBool = false;
    }

    // add the money
    IEnumerator addMoney() {
        //b4:
        //yield return new WaitForSeconds(2.75f);
        //Debug.Log("MONEY");
        // yield return new WaitForSeconds(.5f);
        //Debug.Log("MONEY");
        //yield return new WaitForSeconds(.5f);
        //Debug.Log("MONEY");
        //yield return new WaitForSeconds(.5f);
        //Debug.Log("MONEY");

        //b1:
        yield return new WaitForSeconds(3.6f);
        coinsEtage1Value += PlayerPrefs.GetFloat("etage1CashGain", .33f);

        //PlayerPrefs.SetFloat("cash", data.GetComponent<Data>().cash += .33f);
        //  data.GetComponent<Data>().cashText.text = PlayerPrefs.GetFloat("cash").ToString();
        // PlayerPrefs.Save();

        coinseEtage1Text.text = coinsEtage1Value.ToString();
        yield return new WaitForSeconds(.525f);
        //StartCoroutine(addMoney());
    }
}
