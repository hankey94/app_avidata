﻿using System.Collections;
using UnityEngine;

public class Trucker : MonoBehaviour {

    private bool truckerWalk = false;
    public GameObject arrow;
    public GameObject pipe;
    public float truckerMoney;
    public GameObject data;
    public GameObject upgradeEtage1;

    // INITIALIZE STUFF
    void Start(){
        PlayerPrefs.SetInt("automaticTruckerON", 0);
    }

    // Update is called once per frame
    void Update () {
        if (PlayerPrefs.GetInt("firstUpgrader", 0) == 1) {
            upgradeEtage1.SetActive(true);
        }
        // START AUTOMATIC INSTANCE AND DONT LET THEM TOUCH THE WORKERS
        if (PlayerPrefs.GetInt("automaticEtage1_3", 0) == 1)   {
            truckerWalk = true;
            if (PlayerPrefs.GetInt("automaticTruckerON", 0) == 0)  {
                StartCoroutine(truckerWalkThread());
                PlayerPrefs.SetInt("automaticTruckerON", 1);
            }
        }
    }

    public void startTrucker() {
        GameObject.Find("SFX-Control").GetComponent<AudioSource>().Play();
        if (!truckerWalk)  {
            arrow.SetActive(false);
            truckerWalk = true;
            StartCoroutine(truckerWalkThread());
        }
    }

    IEnumerator truckerWalkThread()  {
        for (float i = 269; i > -117; i -= 3) {
            GameObject.Find("trucker").GetComponent<Transform>().localPosition = new Vector3(i, -149, -32000);
            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(.5f);
        GameObject.Find("elevator101").GetComponent<Elevator>().machineText.text = "0";
        GameObject.Find("trucker").GetComponent<Transform>().Rotate(0, 180, 0);

        for (float i = -117; i < 269; i += 3) {
            GameObject.Find("trucker").GetComponent<Transform>().localPosition = new Vector3(i, -149, -32000);
            yield return new WaitForSeconds(0.01f);
        }
        PlayerPrefs.SetFloat("cash", PlayerPrefs.GetFloat("cash") + truckerMoney);
        truckerMoney = 0;
        data.GetComponent<Data>().cashText.text = PlayerPrefs.GetFloat("cash").ToString();
        truckerWalk = false;
        PlayerPrefs.SetInt("firstUpgrader", 1);
        PlayerPrefs.Save();

        if (PlayerPrefs.GetInt("automaticEtage1_3", 0) == 1)  {
            yield return new WaitForSeconds(1f);
            StartCoroutine(truckerWalkThread());
        }
        GameObject.Find("trucker").GetComponent<Transform>().Rotate(0, 180, 0);
    }
}
