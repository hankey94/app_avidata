﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour {
    
    public GameObject loadingImage;
    public GameObject panelShop;
    public GameObject panelSettings;
    public GameObject panelUpgrade;
    public GameObject spine_characters;
    public GameObject panelBot;
    public GameObject panelTop;
    public Slider loading;
    public Text loadingText;
    public Text loadingTextHint1;
    private ArrayList list = new ArrayList();

    // Use this for initialization
    void Start () {
        StartCoroutine(loadingTextThread());
        StartCoroutine(newLoadingTextHint());
    }

    // New Random Loading Hints
    IEnumerator newLoadingTextHint() {
        for (int i = 0; i < 3; i++) {
            int random = Random.Range(1, 15);
            while (list.Contains(random)) {
                random = Random.Range(1, 15);
            }
            switch (random)  {
                case 1:
                    list.Add(1);
                    loadingTextHint1.text = "Zitronen werden gepresst...";
                    break;
                case 2:
                    list.Add(2);
                    loadingTextHint1.text = "LKW wird entladen...";
                    break;
                case 3:
                    list.Add(3);
                    loadingTextHint1.text = "Flaschen werden ausgespült...";
                    break;
                case 4:
                    list.Add(4);
                    loadingTextHint1.text = "Kamin wird gesäubert...";
                    break;
                case 5:
                    list.Add(5);
                    loadingTextHint1.text = "Igel wird gestreichelt...";
                    break;
                case 6:
                    list.Add(6);
                    loadingTextHint1.text = "Fließband wird eingeschaltet...";
                    break;
                case 7:
                    list.Add(7);
                    loadingTextHint1.text = "Speiseplan wird ausgehängt...";
                    break;
                case 8:
                    list.Add(8);
                    loadingTextHint1.text = "Jobs werden generiert...";
                    break;
                case 9:
                    list.Add(9);
                    loadingTextHint1.text = "Stellen werden ausgeschrieben...";
                    break;
                case 10:
                    list.Add(10);
                    loadingTextHint1.text = "Stellen werden ausgeschrieben...";
                    break;
                case 11:
                    list.Add(11);
                    loadingTextHint1.text = "Produktion wird gestartet...";
                    break;
                case 12:
                    list.Add(12);
                    loadingTextHint1.text = "Schichtplan wird aufgestellt...";
                    break;
                case 13:
                    list.Add(13);
                    loadingTextHint1.text = "Büropflanzen werden gegossen...";
                    break;
                case 14:
                    list.Add(14);
                    loadingTextHint1.text = "Meetings werden vereinbart...";
                    break;
                case 15:
                    list.Add(15);
                    loadingTextHint1.text = "Geheimrezept wird geladen...";
                    break;
                default:
                    list.Add(1);
                    loadingTextHint1.text = "Zitronen werden gepresst...";
                    break;
            }
            yield return new WaitForSeconds(3);
        }
    }

    // ------------------------------- THREAD -------------------------------- //

    // Wait for Text Thread
    IEnumerator loadingTextThread() {
        for(int i = 0; i <= 100; i++) {
            loading.value += 1;
            loadingText.text = loading.value + " %";
            yield return new WaitForSeconds(.035f);
        }
        StopCoroutine(newLoadingTextHint());
        StopCoroutine(loadingTextThread());
        loadingImage.SetActive(false);
        panelBot.SetActive(true);
        panelTop.SetActive(true);
    }

    // ------------------------------- OPEN / CLOSE PANELS -------------------------------- //

    // Opens the Shop Panel
    public void openPanelShop() {
        GameObject.Find("SFX-Control").GetComponent<AudioSource>().Play();
        panelShop.SetActive(true);
        panelUpgrade.SetActive(false);
        panelSettings.SetActive(false);
        spine_characters.GetComponent<Transform>().localScale = new Vector3(0,0,0);
    }

    // Opens the Settings Panel
    public void openPanelSettings()  {
        GameObject.Find("SFX-Control").GetComponent<AudioSource>().Play();
        panelShop.SetActive(false);
        panelUpgrade.SetActive(false);
        panelSettings.SetActive(true);
        spine_characters.GetComponent<Transform>().localScale = new Vector3(0, 0, 0);
    }

    // Opens the Upgrade Panel
    public void openPanelUpgrade()  {
        GameObject.Find("SFX-Control").GetComponent<AudioSource>().Play();
        panelUpgrade.SetActive(true);
        panelShop.SetActive(false);
        panelSettings.SetActive(false);
        spine_characters.GetComponent<Transform>().localScale = new Vector3(0, 0, 0);
    }

    // Close all UI Panels
    public void closeAllPanels() {
        GameObject.Find("SFX-Control").GetComponent<AudioSource>().Play();
        panelShop.SetActive(false);
        panelUpgrade.SetActive(false);
        panelSettings.SetActive(false);
        spine_characters.GetComponent<Transform>().localScale = new Vector3(480.7692f, 480.7692f, 480.7692f);
    }
}
