﻿using System.Collections;
using UnityEngine;

public class Clouds : MonoBehaviour {

    float posY;
    float posX;

    void Awake() {
        posY = Random.Range(350, 640);
      //  posX = Random.Range(-430, 430);
        GetComponent<RectTransform>().localPosition = new Vector3(GetComponent<RectTransform>().localPosition.x, posY, 0);
        StartCoroutine(cloudy());
    }

    IEnumerator cloudy() { 
        while (true) {
            float posX = GetComponent<RectTransform>().localPosition.x;
            posX += 1;
            GetComponent<RectTransform>().localPosition = new Vector3(posX, posY, 0);
            yield return new WaitForSeconds(0.02f);

            if(GetComponent<RectTransform>().localPosition.x >= 750) {
                GetComponent<RectTransform>().localPosition = new Vector3(-760, posY, 0);
            }
        }
    }
}
