﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class kellerWorkerMovement : MonoBehaviour {

    float xStandard = -74f;
    Boolean goLeft = true;
    float value = 0;
    TreppenWorker treppenWorker;
    public Text valueText;
    ClickerValue clickerValue;

    // Use this for initialization
    void Start () {
        StartCoroutine(movement());
        GameObject container = GameObject.FindGameObjectWithTag("walker");
        treppenWorker = container.GetComponent<TreppenWorker>();
        GameObject main = GameObject.Find("MAIN");
        clickerValue = main.GetComponent<ClickerValue>();
    }

    // The standard Update method
    IEnumerator movement() {
        if (goLeft) {
            xStandard -= 0.05f;
            transform.position += Vector3.left * xStandard * 0.02f;
            yield return new WaitForSeconds(0.01f);
        } else {
            xStandard += 0.05f;
            transform.position += Vector3.right * xStandard * 0.02f;
            yield return new WaitForSeconds(0.01f);
        }
        StartCoroutine(movement());
    }

    void OnCollisionEnter2D(Collision2D coll){ 
        if (coll.gameObject.tag == "containerRight") {
            this.goLeft = false;
            clickerValue.clickerValueMax += value;
            clickerValue.txt_clickerValueMax.text = 
                clickerValue.clickerValueMax.ToString() + " $";
            this.value = 0;
            valueText.text = "0 $";   
        }
        if (coll.gameObject.tag == "containerLeft") {
            this.goLeft = true;
            this.value += treppenWorker.containerValue;
            valueText.text = value.ToString() + " $";
            treppenWorker.containerValue = 0;
            treppenWorker.containerText.text = "0 $";
        }
    }
}
