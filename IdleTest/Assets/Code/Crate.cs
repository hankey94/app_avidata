﻿using UnityEngine.UI;
using UnityEngine;

public class Crate : MonoBehaviour {

    public Text crateText;
    public float crateValue = 0;

    upgradeTable moveFlask;

    void Start()  {
        GameObject container = GameObject.Find("MAIN");
        moveFlask = container.GetComponent<upgradeTable>();
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "flask")   {
            crateValue += moveFlask.valueFlaskStockwerk1;
            crateText.text = crateValue.ToString() + " $";  
        }
    }
}
