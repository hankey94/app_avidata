﻿using UnityEngine;
using UnityEngine.UI;

public class UpgradeEtage : MonoBehaviour {

    public GameObject panelUpgrade;
    private int panelClickEtage1;

    // PANEL
    public Text floorName;
    public Text costText;

    // ETAGE 1
    private int levelEtage1 = 1;
    public GameObject upgradeArrowEtage1;
    public Text textEtage1;
    private float kostenEtage;

    private float[] kostenEtageGesamt = new float[800];

    // TEXTS
    public Text bottleGainText;
    public Text bottleGainTextFuture;
    public Text currentWorkersText;
    public Text futureWorkersText;

    // ------------------------------------------------------------- //

    void Start() {
        // INIT
        PlayerPrefs.SetFloat("kostenEtage1", 1f);
        PlayerPrefs.SetInt("levelEtage1", 1);
        floorName.text = "FLOOR LEVEL " + PlayerPrefs.GetInt("levelEtage1", 1);
        kostenEtage = 1f;
        levelEtage1 = 1;
        textEtage1.text = "LEVEL\n" + levelEtage1.ToString();
    }

    void Update() {
        // SHOW UPGRADE ARROW
        if (PlayerPrefs.GetFloat("cash", 0) >= kostenEtage) {
            upgradeArrowEtage1.SetActive(true);
        } else {
            upgradeArrowEtage1.SetActive(false);
        }

        // Cash >= 1.000 == 1k
        if (PlayerPrefs.GetFloat("kostenEtage1") >= 1000) {
            costText.text = System.Math.Round((PlayerPrefs.GetFloat("kostenEtage1") / 1000), 1).ToString() + "k";
            bottleGainText.text = System.Math.Round((PlayerPrefs.GetFloat("etage1CashGain") / 1000), 1).ToString() + "k";
            bottleGainTextFuture.text = "+" + System.Math.Round(((PlayerPrefs.GetFloat("etage1CashGainFuture")
                - PlayerPrefs.GetFloat("etage1CashGain")) / 1000), 1).ToString() + "k";
        }
        // Cash >= 1.000.000 == 1m
        if (PlayerPrefs.GetFloat("kostenEtage1") >= (1000*1000)) {
            costText.text = System.Math.Round((PlayerPrefs.GetFloat("kostenEtage1") / 10000), 1).ToString() + "m";
            bottleGainText.text = System.Math.Round((PlayerPrefs.GetFloat("etage1CashGain") / 10000), 1).ToString() + "m";
            bottleGainTextFuture.text = "+" + System.Math.Round(((PlayerPrefs.GetFloat("etage1CashGainFuture")
                - PlayerPrefs.GetFloat("etage1CashGain")) / 10000), 1).ToString() + "m";
        }
        // Cash >= 1.000.000.000 == 1t
        if (PlayerPrefs.GetFloat("kostenEtage1") >= (1000*1000*1000)) {
            costText.text = System.Math.Round((PlayerPrefs.GetFloat("kostenEtage1") / 10000000), 1).ToString() + "t";
            bottleGainText.text = System.Math.Round((PlayerPrefs.GetFloat("etage1CashGain") / 10000000), 1).ToString() + "t";
            bottleGainTextFuture.text = "+" + System.Math.Round(((PlayerPrefs.GetFloat("etage1CashGainFuture")
                - PlayerPrefs.GetFloat("etage1CashGain")) / 10000000), 1).ToString() + "t";
        }
    }

    //
    public void clickEtage1() {
        costText.text = kostenEtage.ToString();
        panelUpgrade.SetActive(true);
        panelClickEtage1 = 1;
    }

    public void everEtageTheSame() {
        levelEtage1++;
        PlayerPrefs.SetInt("levelEtage1", levelEtage1);
        GameObject.Find("CASHCASHCASHTEXT").GetComponent<Text>().text = PlayerPrefs.GetFloat("cash").ToString();
        PlayerPrefs.SetFloat("kostenEtage1", kostenEtage);
        PlayerPrefs.Save();
        // check if > 1000 and so on
        costText.text = System.Math.Round(kostenEtage, 2).ToString();
        textEtage1.text = "LEVEL\n" + levelEtage1.ToString();
        bottleGainText.text = PlayerPrefs.GetFloat("etage1CashGain").ToString();
        bottleGainTextFuture.text = "+" + System.Math.Round((PlayerPrefs.GetFloat("etage1CashGainFuture") - PlayerPrefs.GetFloat("etage1CashGain")), 2).ToString();
        floorName.text = "FLOOR LEVEL " + PlayerPrefs.GetInt("levelEtage1", 1);
    }

    void Awake() {
        kostenEtageGesamt[0] = 1.16f;
        kostenEtageGesamt[1] = 1.31f;
        kostenEtageGesamt[2] = 1.5f;
        kostenEtageGesamt[3] = 1.76f;
        kostenEtageGesamt[4] = 2.04f;
        kostenEtageGesamt[5] = 2.38f;
        kostenEtageGesamt[6] = 2.76f;
        kostenEtageGesamt[7] = 3.14f;
        kostenEtageGesamt[8] = 3.49f;
        kostenEtageGesamt[9] = 4.01f;
        kostenEtageGesamt[10] = 4.7f;
        kostenEtageGesamt[11] = 5.33f;
        kostenEtageGesamt[12] = 6.03f;
        kostenEtageGesamt[13] = 6.93f;
        kostenEtageGesamt[14] = 7.82f;
        kostenEtageGesamt[15] = 9.19f;
        kostenEtageGesamt[16] = 10.4f;
        kostenEtageGesamt[17] = 11.92f;
        kostenEtageGesamt[18] = 13.6f;
        kostenEtageGesamt[19] = 15.49f;
        kostenEtageGesamt[20] = 17.69f;
        kostenEtageGesamt[21] = 20.17f;
        kostenEtageGesamt[22] = 23.02f;
        kostenEtageGesamt[23] = 26.2f;
        kostenEtageGesamt[24] = 29.86f;
        kostenEtageGesamt[25] = 34.07f;
        kostenEtageGesamt[26] = 38.94f;
        kostenEtageGesamt[27] = 44.31f;
        kostenEtageGesamt[28] = 50.64f;
        kostenEtageGesamt[29] = 57.78f;
        kostenEtageGesamt[30] = 65.96f;
        kostenEtageGesamt[31] = 75.19f;
        kostenEtageGesamt[32] = 85.67f;
        kostenEtageGesamt[33] = 97.44f;
        kostenEtageGesamt[34] = 111.52f;
        kostenEtageGesamt[35] = 126.97f;
        kostenEtageGesamt[36] = 144.84f;
        kostenEtageGesamt[37] = 166.04f;
        kostenEtageGesamt[38] = 189.53f;
        kostenEtageGesamt[39] = 217.01f;
        kostenEtageGesamt[40] = 247.95f;
        kostenEtageGesamt[41] = 283.18f;
        kostenEtageGesamt[42] = 322.64f;
        kostenEtageGesamt[43] = 368.72f;
        kostenEtageGesamt[44] = 421.04f;
        kostenEtageGesamt[45] = 481.52f;
        kostenEtageGesamt[46] = 549.6f;
        kostenEtageGesamt[47] = 628.73f;
        kostenEtageGesamt[48] = 717.04f;
        kostenEtageGesamt[49] = 817.99f;
        kostenEtageGesamt[50] = 931.23f;
        kostenEtageGesamt[51] = 1063.05f;
        kostenEtageGesamt[52] = 1214.11f;
        kostenEtageGesamt[53] = 1386.68f;
        kostenEtageGesamt[54] = 1580.35f;
        kostenEtageGesamt[55] = 1798.03f;
        kostenEtageGesamt[56] = 2042.7f;
        kostenEtageGesamt[57] = 2319.52f;
        kostenEtageGesamt[58] = 2639.74f;
        kostenEtageGesamt[59] = 3009.37f;
        kostenEtageGesamt[60] = 3431.58f;
        kostenEtageGesamt[61] = 3910.55f;
        kostenEtageGesamt[62] = 4460.84f;
        kostenEtageGesamt[63] = 5097f;
        kostenEtageGesamt[64] = 5809.21f;
        kostenEtageGesamt[65] = 6652.72f;
        kostenEtageGesamt[66] = 7594.51f;
        kostenEtageGesamt[67] = 8630.9f;
        kostenEtageGesamt[68] = 9782.38f;
        kostenEtageGesamt[69] = 11123.97f;
        kostenEtageGesamt[70] = 12651.74f;
        kostenEtageGesamt[71] = 14440.17f;
        kostenEtageGesamt[72] = 16331.97f;
        kostenEtageGesamt[73] = 18613.98f;
        kostenEtageGesamt[74] = 21204.25f;
        kostenEtageGesamt[75] = 24207.47f;
        kostenEtageGesamt[76] = 27602.59f;
        kostenEtageGesamt[77] = 31561.04f;
        kostenEtageGesamt[78] = 35949.66f;
        kostenEtageGesamt[79] = 41011.57f;
        kostenEtageGesamt[80] = 46799.59f;
        kostenEtageGesamt[81] = 53200.69f;
        kostenEtageGesamt[82] = 60455f;
        kostenEtageGesamt[83] = 68777.85f;
        kostenEtageGesamt[84] = 78162.24f;
        kostenEtageGesamt[85] = 89094.22f;
        kostenEtageGesamt[86] = 101594.81f;
        kostenEtageGesamt[87] = 115773.69f;
        kostenEtageGesamt[88] = 132389.16f;
        kostenEtageGesamt[89] = 150786.48f;
        kostenEtageGesamt[90] = 171661.8f;
        kostenEtageGesamt[91] = 195724.99f;
        kostenEtageGesamt[92] = 222297.59f;
        kostenEtageGesamt[93] = 253316.34f;
        kostenEtageGesamt[94] = 288847.73f;
        kostenEtageGesamt[95] = 328968.21f;
        kostenEtageGesamt[96] = 375615.15f;
        kostenEtageGesamt[97] = 428300.33f;
        kostenEtageGesamt[98] = 472020.55f;
        kostenEtageGesamt[99] = 519643.63f;
        kostenEtageGesamt[100] = 573845.14f;
        kostenEtageGesamt[101] = 634171.19f;
        kostenEtageGesamt[102] = 697698.94f;
        kostenEtageGesamt[103] = 768508.02f;
        kostenEtageGesamt[104] = 845266.39f;
        kostenEtageGesamt[105] = 929259.57f;
        kostenEtageGesamt[106] = 1022230.93f;
        kostenEtageGesamt[107] = 1122971.8f;
        kostenEtageGesamt[108] = 1239164.88f;
        kostenEtageGesamt[109] = 1363243.03f;
        kostenEtageGesamt[110] = 1505643.41f;
        kostenEtageGesamt[111] = 1653290.3f;
        kostenEtageGesamt[112] = 1820219.14f;
        kostenEtageGesamt[113] = 2002529.82f;
        kostenEtageGesamt[114] = 2207366.58f;
        kostenEtageGesamt[115] = 2436560.35f;
        kostenEtageGesamt[116] = 2677205.93f;
        kostenEtageGesamt[117] = 2954438.06f;
        kostenEtageGesamt[118] = 3249483.63f;
        kostenEtageGesamt[119] = 3580616.79f;
        kostenEtageGesamt[120] = 3940170.12f;
        kostenEtageGesamt[121] = 4341969.48f;
        kostenEtageGesamt[122] = 4763426.66f;
        kostenEtageGesamt[123] = 5248859.37f;
        kostenEtageGesamt[124] = 5779134.73f;
        kostenEtageGesamt[125] = 6362561.4f;
        kostenEtageGesamt[126] = 7012534.98f;
        kostenEtageGesamt[127] = 7727589.44f;
        kostenEtageGesamt[128] = 8517856.07f;
        kostenEtageGesamt[129] = 9358023.31f;
        kostenEtageGesamt[130] = 10303313.46f;
        kostenEtageGesamt[131] = 11351592.7f;
        kostenEtageGesamt[132] = 12477952.55f;
        kostenEtageGesamt[133] = 13712152.79f;
        kostenEtageGesamt[134] = 15068557.2f;
        kostenEtageGesamt[135] = 16542347.7f;
        kostenEtageGesamt[136] = 18152718.43f;
        kostenEtageGesamt[137] = 19963109.76f;
        kostenEtageGesamt[138] = 21947443.6f;
        kostenEtageGesamt[139] = 24166774.29f;
        kostenEtageGesamt[140] = 26531674.28f;
        kostenEtageGesamt[141] = 29114676.47f;
        kostenEtageGesamt[142] = 31989415.85f;
        kostenEtageGesamt[143] = 35167791.54f;
        kostenEtageGesamt[144] = 38628986.83f;
        kostenEtageGesamt[145] = 42404229.09f;
        kostenEtageGesamt[146] = 46656129.23f;
        kostenEtageGesamt[147] = 51211932.05f;
        kostenEtageGesamt[148] = 56415231.18f;
        kostenEtageGesamt[149] = 62192999.26f;
        kostenEtageGesamt[150] = 68520261.95f;
        kostenEtageGesamt[151] = 75363837.79f;
        kostenEtageGesamt[152] = 82842417.13f;
        kostenEtageGesamt[153] = 91251534.89f;
        kostenEtageGesamt[154] = 100389759.43f;
        kostenEtageGesamt[155] = 110349668.88f;
        kostenEtageGesamt[156] = 121636750.72f;
        kostenEtageGesamt[157] = 133968734.21f;
        kostenEtageGesamt[158] = 147203252.15f;
        kostenEtageGesamt[159] = 162010779.83f;
        kostenEtageGesamt[160] = 178761333.15f;
        kostenEtageGesamt[161] = 196580655.74f;
        kostenEtageGesamt[162] = 215960289.9f;
        kostenEtageGesamt[163] = 238044719.96f;
        kostenEtageGesamt[164] = 261690348.09f;
        kostenEtageGesamt[165] = 288301282.71f;
        kostenEtageGesamt[166] = 317162046.74f;
        kostenEtageGesamt[167] = 349051500.34f;
        kostenEtageGesamt[168] = 384782584.48f;
        kostenEtageGesamt[169] = 422547343.29f;
        kostenEtageGesamt[170] = 465223335.1f;
        kostenEtageGesamt[171] = 512427797.56f;
        kostenEtageGesamt[172] = 563220711.51f;
        kostenEtageGesamt[173] = 619695863.6f;
        kostenEtageGesamt[174] = 682104337.77f;
        kostenEtageGesamt[175] = 750331270.27f;
        kostenEtageGesamt[176] = 825844040.75f;
        kostenEtageGesamt[177] = 908804545.2f;
        kostenEtageGesamt[178] = 1001398322.29f;
        kostenEtageGesamt[179] = 1103288859.08f;
        kostenEtageGesamt[180] = 1216602819.78f;
        kostenEtageGesamt[181] = 1338059166.13f;
        kostenEtageGesamt[182] = 1470131564.23f;
        kostenEtageGesamt[183] = 1617600112.83f;
        kostenEtageGesamt[184] = 1779663860.61f;
        kostenEtageGesamt[185] = 1954560949.59f;
        kostenEtageGesamt[186] = 2154276985.63f;
        kostenEtageGesamt[187] = 2373761046.06f;
        kostenEtageGesamt[188] = 2608325264.07f;
        kostenEtageGesamt[189] = 2864979391.37f;
        kostenEtageGesamt[190] = 3153214591.35f;
        kostenEtageGesamt[191] = 3474051601.98f;
        kostenEtageGesamt[192] = 3829974144.27f;
        kostenEtageGesamt[193] = 4226353598.24f;
        kostenEtageGesamt[194] = 4660427733f;
        kostenEtageGesamt[195] = 5122842655.31f;
        kostenEtageGesamt[196] = 5629479987.22f;
        kostenEtageGesamt[197] = 6198926323.85f;
        kostenEtageGesamt[198] = 6828532869.36f;
        kostenEtageGesamt[199] = 7505232516.38f;
        kostenEtageGesamt[200] = 8241750198.13f;
        kostenEtageGesamt[201] = 9066122841.82f;
        kostenEtageGesamt[202] = 9980735275.29f;
        kostenEtageGesamt[203] = 10951512562.35f;
        kostenEtageGesamt[204] = 12081313870.52f;
        kostenEtageGesamt[205] = 13322039011.48f;
        kostenEtageGesamt[206] = 14637291687.89f;
        kostenEtageGesamt[207] = 16122261008.71f;
        kostenEtageGesamt[208] = 17727953616.9f;
        kostenEtageGesamt[209] = 19466881131.01f;
        kostenEtageGesamt[210] = 21406230483.28f;
        kostenEtageGesamt[211] = 23475582287.7f;
        kostenEtageGesamt[212] = 25820326394.8f;
        kostenEtageGesamt[213] = 28401597145.73f;
        kostenEtageGesamt[214] = 31263440992.28f;
        kostenEtageGesamt[215] = 34331133177.47f;
        kostenEtageGesamt[216] = 37698179249.81f;
        kostenEtageGesamt[217] = 41537014983.5f;
        kostenEtageGesamt[218] = 45681704584.3f;
        kostenEtageGesamt[219] = 50313915300.53f;
        kostenEtageGesamt[220] = 55502726858.75f;
        kostenEtageGesamt[221] = 60921001246.08f;
        kostenEtageGesamt[222] = 66969633263.36f;
        kostenEtageGesamt[223] = 73748537130.2f;
        kostenEtageGesamt[224] = 80888099583.57f;
        kostenEtageGesamt[225] = 88799760082.67f;
        kostenEtageGesamt[226] = 97781459519.48f;
        kostenEtageGesamt[227] = 107762852554.58f;
        kostenEtageGesamt[228] = 118523689772.55f;
        kostenEtageGesamt[229] = 130562308884.46f;
        kostenEtageGesamt[230] = 143448745041.66f;
        kostenEtageGesamt[231] = 157815301731.9f;
        kostenEtageGesamt[232] = 173540647325.42f;
        kostenEtageGesamt[233] = 190512705061.38f;
        kostenEtageGesamt[234] = 209323697930.96f;
        kostenEtageGesamt[235] = 230045071829.03f;
        kostenEtageGesamt[236] = 253183142412.2f;
        kostenEtageGesamt[237] = 278265441796.84f;
        kostenEtageGesamt[238] = 305187109568.45f;
        kostenEtageGesamt[239] = 336003894177.28f;
        kostenEtageGesamt[240] = 368394090869.09f;
        kostenEtageGesamt[241] = 404199717314.39f;
        kostenEtageGesamt[242] = 443479577436.18f;
        kostenEtageGesamt[243] = 487163456963.67f;
        kostenEtageGesamt[244] = 535790686382.3f;
        kostenEtageGesamt[245] = 589962596560.29f;
        kostenEtageGesamt[246] = 646616924896.44f;
        kostenEtageGesamt[247] = 710647542473.63f;
        kostenEtageGesamt[248] = 780906472261.74f;
        kostenEtageGesamt[249] = 860222375529.15f;
        kostenEtageGesamt[250] = 947231735993.9f;
        kostenEtageGesamt[251] = 1038239085083.23f;
        kostenEtageGesamt[252] = 1141245643432f;
        kostenEtageGesamt[253] = 1256303477836.51f;
        kostenEtageGesamt[254] = 1382565163246.85f;
        kostenEtageGesamt[255] = 1523853590380.52f;
        kostenEtageGesamt[256] = 1679693154053.64f;
        kostenEtageGesamt[257] = 1848668448010.59f;
        kostenEtageGesamt[258] = 2040886083284.57f;
        kostenEtageGesamt[259] = 2243585878548.39f;
        kostenEtageGesamt[260] = 2465166341417.94f;
        kostenEtageGesamt[261] = 2710188806972.23f;
        kostenEtageGesamt[262] = 2976693655988.26f;
        kostenEtageGesamt[263] = 3271881997099.75f;
        kostenEtageGesamt[264] = 3599300231521.72f;
        kostenEtageGesamt[265] = 3955721325914.01f;
        kostenEtageGesamt[266] = 4368134040972.29f;
        kostenEtageGesamt[267] = 4795457738837.12f;
        kostenEtageGesamt[268] = 5269202333436.07f;
        kostenEtageGesamt[269] = 5780822099470.57f;
        kostenEtageGesamt[270] = 6370135129282.68f;
        kostenEtageGesamt[271] = 7004400491994.72f;
        kostenEtageGesamt[272] = 7727311734549.13f;
        kostenEtageGesamt[273] = 8520019425973.04f;
        kostenEtageGesamt[274] = 9365619508715.46f;
        kostenEtageGesamt[275] = 10295382487371.5f;
        kostenEtageGesamt[276] = 11330799690980.5f;
        kostenEtageGesamt[277] = 12456028653453.3f;
        kostenEtageGesamt[278] = 13698533557527.5f;
        kostenEtageGesamt[279] = 15048348574163.7f;
        kostenEtageGesamt[280] = 16524150916089.1f;
        kostenEtageGesamt[281] = 18135373848806.8f;
        kostenEtageGesamt[282] = 19989135871112.7f;
        kostenEtageGesamt[283] = 22063255066079.2f;
        kostenEtageGesamt[284] = 24335269009627.3f;
        kostenEtageGesamt[285] = 26798642209306.1f;
        kostenEtageGesamt[286] = 29477471800388.3f;
        kostenEtageGesamt[287] = 32480197395539.4f;
        kostenEtageGesamt[288] = 35760285116291.6f;
        kostenEtageGesamt[289] = 39368907625571.7f;
        kostenEtageGesamt[290] = 43242401588653.7f;
        kostenEtageGesamt[291] = 47623651163542f;
        kostenEtageGesamt[292] = 52333512022357.1f;
        kostenEtageGesamt[293] = 57409810072853.6f;
        kostenEtageGesamt[294] = 62982599038417.9f;
        kostenEtageGesamt[295] = 69346000220500.8f;
        kostenEtageGesamt[296] = 76276358270855.7f;
        kostenEtageGesamt[297] = 84036096333138.4f;
        kostenEtageGesamt[298] = 92739521140109.5f;
        kostenEtageGesamt[299] = 102064034422764f;
        kostenEtageGesamt[300] = 112269129644264f;
        kostenEtageGesamt[301] = 123230716707632f;
        kostenEtageGesamt[302] = 135417010992404f;
        kostenEtageGesamt[303] = 149096508208666f;
        kostenEtageGesamt[304] = 164220627242777f;
        kostenEtageGesamt[305] = 180965438348357f;
        kostenEtageGesamt[306] = 199305799961601f;
        kostenEtageGesamt[307] = 218679822828579f;
        kostenEtageGesamt[308] = 240014643157226f;
        kostenEtageGesamt[309] = 263929823819430f;
        kostenEtageGesamt[310] = 290292163381413f;
        kostenEtageGesamt[311] = 320209949205091f;
        kostenEtageGesamt[312] = 351843865654899f;
        kostenEtageGesamt[313] = 387340317111120f;
        kostenEtageGesamt[314] = 425839629938846f;
        kostenEtageGesamt[315] = 469302723037550f;
        kostenEtageGesamt[316] = 514691286367926f;
        kostenEtageGesamt[317] = 566019762762564f;
        kostenEtageGesamt[318] = 620668005724478f;
        kostenEtageGesamt[319] = 680743923042943f;
        kostenEtageGesamt[320] = 748633403249892f;
        kostenEtageGesamt[321] = 823508917563205f;
        kostenEtageGesamt[322] = 908571850875785f;
        kostenEtageGesamt[323] = 1003621648656210f;
        kostenEtageGesamt[324] = 1104171183238460f;
        kostenEtageGesamt[325] = 1218047655659860f;
        kostenEtageGesamt[326] = 1343295039107490f;
        kostenEtageGesamt[327] = 1475769429779730f;
        kostenEtageGesamt[328] = 1622361859231030f;
        kostenEtageGesamt[329] = 1786394131676920f;
        kostenEtageGesamt[330] = 1964807186622700f;
        kostenEtageGesamt[331] = 2158044136984250f;
        kostenEtageGesamt[332] = 2379374492018900f;
        kostenEtageGesamt[333] = 2616953751246490f;
        kostenEtageGesamt[334] = 2877324352500230f;
        kostenEtageGesamt[335] = 3161166118055720f;
        kostenEtageGesamt[336] = 3485909003581840f;
        kostenEtageGesamt[337] = 3832362573104530f;
        kostenEtageGesamt[338] = 4209188091048370f;
        kostenEtageGesamt[339] = 4623262794858650f;
        kostenEtageGesamt[340] = 5092357365524260f;
        kostenEtageGesamt[341] = 5597488214235680f;
        kostenEtageGesamt[342] = 6180353327161700f;
        kostenEtageGesamt[343] = 6799358216443060f;
        kostenEtageGesamt[344] = 7481175606278270f;
        kostenEtageGesamt[345] = 8225995613570320f;
        kostenEtageGesamt[346] = 9057412074445540f;
        kostenEtageGesamt[347] = 9952481253752250f;
        kostenEtageGesamt[348] = 10907082147119300f;
        kostenEtageGesamt[349] = 11971859982441300f;
        kostenEtageGesamt[350] = 13183130205291900f;
        kostenEtageGesamt[351] = 14509711179419500f;
        kostenEtageGesamt[352] = 16003078938736700f;
        kostenEtageGesamt[353] = 17628817699030300f;
        kostenEtageGesamt[354] = 19377657722021000f;
        kostenEtageGesamt[355] = 21340328592484000f;
        kostenEtageGesamt[356] = 23418376465194100f;
        kostenEtageGesamt[357] = 25791462521416600f;
        kostenEtageGesamt[358] = 28383715139657700f;
        kostenEtageGesamt[359] = 31284566383724800f;
        kostenEtageGesamt[360] = 34441867250633300f;
        kostenEtageGesamt[361] = 37873057142523200f;
        kostenEtageGesamt[362] = 41700389113197800f;
        kostenEtageGesamt[363] = 45839303591385200f;
        kostenEtageGesamt[364] = 50418573495528100f;
        kostenEtageGesamt[365] = 55443314524380000f;
        kostenEtageGesamt[366] = 60960249186807200f;
        kostenEtageGesamt[367] = 67104424124758100f;
        kostenEtageGesamt[368] = 73828643556022500f;
        kostenEtageGesamt[369] = 81114018643514100f;
        kostenEtageGesamt[370] = 89153599004078400f;
        kostenEtageGesamt[371] = 98123304899359000f;
        kostenEtageGesamt[372] = 108035057336280000f;
        kostenEtageGesamt[373] = 118806243237716000f;
        kostenEtageGesamt[374] = 130521232891548000f;
        kostenEtageGesamt[375] = 143694942764305000f;
        kostenEtageGesamt[376] = 157774360869055000f;
        kostenEtageGesamt[377] = 173035174667398000f;
        kostenEtageGesamt[378] = 189766056313141000f;
        kostenEtageGesamt[379] = 208566250270699000f;
        kostenEtageGesamt[380] = 228853765335870000f;
        kostenEtageGesamt[381] = 251680386534491000f;
        kostenEtageGesamt[382] = 277035177759792000f;
        kostenEtageGesamt[383] = 305444082746661000f;
        kostenEtageGesamt[384] = 335989217862825000f;
        kostenEtageGesamt[385] = 370186993518569000f;
        kostenEtageGesamt[386] = 408045516426330000f;
        kostenEtageGesamt[387] = 448773451578063000f;
        kostenEtageGesamt[388] = 494457122161153000f;
        kostenEtageGesamt[389] = 543738916094991000f;
        kostenEtageGesamt[390] = 597196592081549000f;
        kostenEtageGesamt[391] = 657285496346525000f;
        kostenEtageGesamt[392] = 722959948283158000f;
        kostenEtageGesamt[393] = 796410055777717000f;
        kostenEtageGesamt[394] = 875588237204521000f;
        kostenEtageGesamt[395] = 960910467853396000f;
        kostenEtageGesamt[396] = 1055921272945370000f;
        kostenEtageGesamt[397] = 1160460229990930000f;
        kostenEtageGesamt[398] = 1275503682753600000f;
        kostenEtageGesamt[399] = 1406773128791440000f;
        kostenEtageGesamt[400] = 1543613588997450000f;
        kostenEtageGesamt[401] = 1702441007934230000f;
        kostenEtageGesamt[402] = 1873687657437500000f;
        kostenEtageGesamt[403] = 2062378411550430000f;
        kostenEtageGesamt[404] = 2270220547529940000f;
        kostenEtageGesamt[405] = 2496726773684210000f;
        kostenEtageGesamt[406] = 2746673170656570000f;
        kostenEtageGesamt[407] = 3028379752368820000f;
        kostenEtageGesamt[408] = 3323284303592510000f;
        kostenEtageGesamt[409] = 3654988816787370000f;
        kostenEtageGesamt[410] = 4017178142750580000f;
        kostenEtageGesamt[411] = 4406232761582370000f;
        kostenEtageGesamt[412] = 4843778826069230000f;
        kostenEtageGesamt[413] = 5332277632243690000f;
        kostenEtageGesamt[414] = 5866052517675460000f;
        kostenEtageGesamt[415] = 6444278151246060000f;
        kostenEtageGesamt[416] = 7101908776656390000f;
        kostenEtageGesamt[417] = 7820479671211080000f;
        kostenEtageGesamt[418] = 8567296449872080000f;
        kostenEtageGesamt[419] = 9432274998787830000f;
        kostenEtageGesamt[420] = 10398152096594800000f;
        kostenEtageGesamt[421] = 11471417858911100000f;
        kostenEtageGesamt[422] = 12591227271983000000f;
        kostenEtageGesamt[423] = 13843461628351500000f;
        kostenEtageGesamt[424] = 15226825159706200000f;
        kostenEtageGesamt[425] = 16758784164889200000f;
        kostenEtageGesamt[426] = 18439728340254300000f;
        kostenEtageGesamt[427] = 20265533111211100000f;
        kostenEtageGesamt[428] = 22305652462999100000f;
        kostenEtageGesamt[429] = 24518275365993300000f;
        kostenEtageGesamt[430] = 26909778654797400000f;
        kostenEtageGesamt[431] = 29653329632063900000f;
        kostenEtageGesamt[432] = 32606379429439200000f;
        kostenEtageGesamt[433] = 35867801049323500000f;
        kostenEtageGesamt[434] = 39552493366263000000f;
        kostenEtageGesamt[435] = 43588223590902300000f;
        kostenEtageGesamt[436] = 47940316925954800000f;
        kostenEtageGesamt[437] = 52626722434066200000f;
        kostenEtageGesamt[438] = 57909881397622800000f;
        kostenEtageGesamt[439] = 63724078525465700000f;
        kostenEtageGesamt[440] = 70129107918519300000f;
        kostenEtageGesamt[441] = 76993451257527400000f;
        kostenEtageGesamt[442] = 84638321204830100000f;
        kostenEtageGesamt[443] = 92855432345386400000f;
        kostenEtageGesamt[444] = 1.02025462734503E+20f;
        kostenEtageGesamt[445] = 1.12258776175709E+20f;
        kostenEtageGesamt[446] = 1.23575316721351E+20f;
        kostenEtageGesamt[447] = 1.3641679470228E+20f;
        kostenEtageGesamt[448] = 1.50259825384555E+20f;
        kostenEtageGesamt[449] = 1.65252176480079E+20f;
        kostenEtageGesamt[450] = 1.81409786793126E+20f;
        kostenEtageGesamt[451] = 1.99744424117432E+20f;
        kostenEtageGesamt[452] = 2.1962303987723E+20f;
        kostenEtageGesamt[453] = 2.41571539444481E+20f;
        kostenEtageGesamt[454] = 2.65291621194762E+20f;
        kostenEtageGesamt[455] = 2.91799472933075E+20f;
        kostenEtageGesamt[456] = 3.20626516232171E+20f;
        kostenEtageGesamt[457] = 3.53583318697114E+20f;
        kostenEtageGesamt[458] = 3.89263259370997E+20f;
        kostenEtageGesamt[459] = 4.28810332658889E+20f;
        kostenEtageGesamt[460] = 4.7101700547655E+20f;
        kostenEtageGesamt[461] = 5.1827564442861E+20f;
        kostenEtageGesamt[462] = 5.68952744545398E+20f;
        kostenEtageGesamt[463] = 6.24588838924779E+20f;
        kostenEtageGesamt[464] = 6.86758749727586E+20f;
        kostenEtageGesamt[465] = 7.55290653822425E+20f;
        kostenEtageGesamt[466] = 8.27943729914076E+20f;
        kostenEtageGesamt[467] = 9.11450081410232E+20f;
        kostenEtageGesamt[468] = 1.00612041746767E+21f;
        kostenEtageGesamt[469] = 1.10957695511379E+21f;
        kostenEtageGesamt[470] = 1.2222383349775E+21f;
        kostenEtageGesamt[471] = 1.34763791270865E+21f;
        kostenEtageGesamt[472] = 1.48669673066116E+21f;
        kostenEtageGesamt[473] = 1.63034224896122E+21f;
        kostenEtageGesamt[474] = 1.79179244964641E+21f;
        kostenEtageGesamt[475] = 1.97250285780307E+21f;
        kostenEtageGesamt[476] = 2.16164802557713E+21f;
        kostenEtageGesamt[477] = 2.38422067627427E+21f;
        kostenEtageGesamt[478] = 2.62142933078065E+21f;
        kostenEtageGesamt[479] = 2.8872585491248E+21f;
        kostenEtageGesamt[480] = 3.17931222724136E+21f;
        kostenEtageGesamt[481] = 3.51326482651796E+21f;
        kostenEtageGesamt[482] = 3.86446191323592E+21f;
        kostenEtageGesamt[483] = 4.24194881300485E+21f;
        kostenEtageGesamt[484] = 4.67316122505378E+21f;
        kostenEtageGesamt[485] = 5.1269467376853E+21f;
        kostenEtageGesamt[486] = 5.64451548424853E+21f;
        kostenEtageGesamt[487] = 6.21434330252246E+21f;
        kostenEtageGesamt[488] = 6.82052834499023E+21f;
        kostenEtageGesamt[489] = 7.50937590922822E+21f;
        kostenEtageGesamt[490] = 8.25910747840897E+21f;
        kostenEtageGesamt[491] = 9.08689750485607E+21f;
        kostenEtageGesamt[492] = 9.97886596510861E+21f;
        kostenEtageGesamt[493] = 1.10135231484324E+22f;
        kostenEtageGesamt[494] = 1.21534581301517E+22f;
        kostenEtageGesamt[495] = 1.33468731324443E+22f;
        kostenEtageGesamt[496] = 1.46497271582113E+22f;
        kostenEtageGesamt[497] = 1.60999955882741E+22f;
        kostenEtageGesamt[498] = 1.77212606464088E+22f;
        kostenEtageGesamt[499] = 1.95092602647831E+22f;
        kostenEtageGesamt[500] = 2.14182596576121E+22f;
        kostenEtageGesamt[501] = 2.35442165824955E+22f;
        kostenEtageGesamt[502] = 2.58736640668516E+22f;
        kostenEtageGesamt[503] = 2.85057960847262E+22f;
        kostenEtageGesamt[504] = 3.13992084765417E+22f;
        kostenEtageGesamt[505] = 3.45636973879483E+22f;
        kostenEtageGesamt[506] = 3.80327709921845E+22f;
        kostenEtageGesamt[507] = 4.18133538149025E+22f;
        kostenEtageGesamt[508] = 4.59953160594333E+22f;
        kostenEtageGesamt[509] = 5.05575967369489E+22f;
        kostenEtageGesamt[510] = 5.55355977564131E+22f;
        kostenEtageGesamt[511] = 6.1193758049E+22f;
        kostenEtageGesamt[512] = 6.72883853381275E+22f;
        kostenEtageGesamt[513] = 7.40484417665765E+22f;
        kostenEtageGesamt[514] = 8.14430374064404E+22f;
        kostenEtageGesamt[515] = 8.92706942221846E+22f;
        kostenEtageGesamt[516] = 9.85011317811238E+22f;
        kostenEtageGesamt[517] = 1.08289847063208E+23f;
        kostenEtageGesamt[518] = 1.1908006832661E+23f;
        kostenEtageGesamt[519] = 1.31326593272233E+23f;
        kostenEtageGesamt[520] = 1.43863573810674E+23f;
        kostenEtageGesamt[521] = 1.58110497324883E+23f;
        kostenEtageGesamt[522] = 1.74115242927088E+23f;
        kostenEtageGesamt[523] = 1.91782556085464E+23f;
        kostenEtageGesamt[524] = 2.1014815999666E+23f;
        kostenEtageGesamt[525] = 2.30893534249258E+23f;
        kostenEtageGesamt[526] = 2.54074419953357E+23f;
        kostenEtageGesamt[527] = 2.8000054036073E+23f;
        kostenEtageGesamt[528] = 3.07763686955366E+23f;
        kostenEtageGesamt[529] = 3.38389367843884E+23f;
        kostenEtageGesamt[530] = 3.72083599041623E+23f;
        kostenEtageGesamt[531] = 4.08553896779833E+23f;
        kostenEtageGesamt[532] = 4.49971339781465E+23f;
        kostenEtageGesamt[533] = 4.96266361673096E+23f;
        kostenEtageGesamt[534] = 5.46418918145619E+23f;
        kostenEtageGesamt[535] = 6.00996981104943E+23f;
        kostenEtageGesamt[536] = 6.61620636746774E+23f;
        kostenEtageGesamt[537] = 7.29505781169333E+23f;
        kostenEtageGesamt[538] = 8.00248011419045E+23f;
        kostenEtageGesamt[539] = 8.83438405564095E+23f;
        kostenEtageGesamt[540] = 9.70379995079387E+23f;
        kostenEtageGesamt[541] = 1.06805502389206E+24f;
        kostenEtageGesamt[542] = 1.17543627948963E+24f;
        kostenEtageGesamt[543] = 1.29103949559405E+24f;
        kostenEtageGesamt[544] = 1.42312949732337E+24f;
        kostenEtageGesamt[545] = 1.56246936386439E+24f;
        kostenEtageGesamt[546] = 1.71702539656903E+24f;
        kostenEtageGesamt[547] = 1.89085541118784E+24f;
        kostenEtageGesamt[548] = 2.07635630827694E+24f;
        kostenEtageGesamt[549] = 2.2808253357498E+24f;
        kostenEtageGesamt[550] = 2.50621545493515E+24f;
        kostenEtageGesamt[551] = 2.7498065219365E+24f;
        kostenEtageGesamt[552] = 3.02741909551207E+24f;
        kostenEtageGesamt[553] = 3.34147914868848E+24f;
        kostenEtageGesamt[554] = 3.6815023196965E+24f;
        kostenEtageGesamt[555] = 4.05782426969709E+24f;
        kostenEtageGesamt[556] = 4.46098914765061E+24f;
        kostenEtageGesamt[557] = 4.90998104701799E+24f;
        kostenEtageGesamt[558] = 5.38775012600678E+24f;
        kostenEtageGesamt[559] = 5.9092318150233E+24f;
        kostenEtageGesamt[560] = 6.49890426656009E+24f;
        kostenEtageGesamt[561] = 7.1298008378061E+24f;
        kostenEtageGesamt[562] = 7.87982816487457E+24f;
        kostenEtageGesamt[563] = 8.66998616750315E+24f;
        kostenEtageGesamt[564] = 9.50827253903573E+24f;
        kostenEtageGesamt[565] = 1.04604253121179E+25f;
        kostenEtageGesamt[566] = 1.15239502791116E+25f;
        kostenEtageGesamt[567] = 1.2693937533185E+25f;
        kostenEtageGesamt[568] = 1.39908523842077E+25f;
        kostenEtageGesamt[569] = 1.53757712765506E+25f;
        kostenEtageGesamt[570] = 1.68854556799307E+25f;
        kostenEtageGesamt[571] = 1.85787202402332E+25f;
        kostenEtageGesamt[572] = 2.04238235655237E+25f;
        kostenEtageGesamt[573] = 2.24320947458735E+25f;
        kostenEtageGesamt[574] = 2.47673497476488E+25f;
        kostenEtageGesamt[575] = 2.73409391027444E+25f;
        kostenEtageGesamt[576] = 3.00356389264891E+25f;
        kostenEtageGesamt[577] = 3.30437292030334E+25f;
        kostenEtageGesamt[578] = 3.63235315664976E+25f;
        kostenEtageGesamt[579] = 3.99438919246464E+25f;
        kostenEtageGesamt[580] = 4.39435301491881E+25f;
        kostenEtageGesamt[581] = 4.83754298802628E+25f;
        kostenEtageGesamt[582] = 5.32916648380851E+25f;
        kostenEtageGesamt[583] = 5.85044185593071E+25f;
        kostenEtageGesamt[584] = 6.44604203587069E+25f;
        kostenEtageGesamt[585] = 7.09974746347446E+25f;
        kostenEtageGesamt[586] = 7.82795068419261E+25f;
        kostenEtageGesamt[587] = 8.61109505589716E+25f;
        kostenEtageGesamt[588] = 9.47514341060289E+25f;
        kostenEtageGesamt[589] = 1.03809836294719E+26f;
        kostenEtageGesamt[590] = 1.14189134943333E+26f;
        kostenEtageGesamt[591] = 1.25890908978426E+26f;
        kostenEtageGesamt[592] = 1.38534725036477E+26f;
        kostenEtageGesamt[593] = 1.5256436056537E+26f;
        kostenEtageGesamt[594] = 1.68000739515195E+26f;
        kostenEtageGesamt[595] = 1.85423507333155E+26f;
        kostenEtageGesamt[596] = 2.03948599932575E+26f;
        kostenEtageGesamt[597] = 2.23885166793491E+26f;
        kostenEtageGesamt[598] = 2.45618560355883E+26f;
        kostenEtageGesamt[599] = 2.70887051084556E+26f;
        kostenEtageGesamt[600] = 2.98182145278955E+26f;
        kostenEtageGesamt[601] = 3.27895826848056E+26f;
        kostenEtageGesamt[602] = 3.61319103892221E+26f;
        kostenEtageGesamt[603] = 3.97191665840914E+26f;
        kostenEtageGesamt[604] = 4.36705468079003E+26f;
        kostenEtageGesamt[605] = 4.80674340564561E+26f;
        kostenEtageGesamt[606] = 5.28304527662244E+26f;
        kostenEtageGesamt[607] = 5.8054999082682E+26f;
        kostenEtageGesamt[608] = 6.37317769608239E+26f;
        kostenEtageGesamt[609] = 7.02351546534466E+26f;
        kostenEtageGesamt[610] = 7.73138515665593E+26f;
        kostenEtageGesamt[611] = 8.48658381827589E+26f;
        kostenEtageGesamt[612] = 9.31567478924717E+26f;
        kostenEtageGesamt[613] = 1.02740250779509E+27f;
        kostenEtageGesamt[614] = 1.1321312730318E+27f;
        kostenEtageGesamt[615] = 1.2433433199389E+27f;
        kostenEtageGesamt[616] = 1.36808494558317E+27f;
        kostenEtageGesamt[617] = 1.50937137196017E+27f;
        kostenEtageGesamt[618] = 1.65770483396421E+27f;
        kostenEtageGesamt[619] = 1.82465719850889E+27f;
        kostenEtageGesamt[620] = 2.00989995173134E+27f;
        kostenEtageGesamt[621] = 2.21971287256498E+27f;
        kostenEtageGesamt[622] = 2.44544458386533E+27f;
        kostenEtageGesamt[623] = 2.69176669313714E+27f;
        kostenEtageGesamt[624] = 2.96531045829162E+27f;
        kostenEtageGesamt[625] = 3.26307927515385E+27f;
        kostenEtageGesamt[626] = 3.59626427026151E+27f;
        kostenEtageGesamt[627] = 3.95159326518711E+27f;
        kostenEtageGesamt[628] = 4.34911827189794E+27f;
        kostenEtageGesamt[629] = 4.79158559725845E+27f;
        kostenEtageGesamt[630] = 5.26878159348228E+27f;
        kostenEtageGesamt[631] = 5.79706864038724E+27f;
        kostenEtageGesamt[632] = 6.38192150854806E+27f;
        kostenEtageGesamt[633] = 7.00818651137814E+27f;
        kostenEtageGesamt[634] = 7.72823955077068E+27f;
        kostenEtageGesamt[635] = 8.52893677276899E+27f;
        kostenEtageGesamt[636] = 9.38190787695935E+27f;
        kostenEtageGesamt[637] = 1.03185912479344E+28f;
        kostenEtageGesamt[638] = 1.13512772282192E+28f;
        kostenEtageGesamt[639] = 1.24913793464579E+28f;
        kostenEtageGesamt[640] = 1.37960478611753E+28f;
        kostenEtageGesamt[641] = 1.52262551267941E+28f;
        kostenEtageGesamt[642] = 1.67599488321148E+28f;
        kostenEtageGesamt[643] = 1.83787803323459E+28f;
        kostenEtageGesamt[644] = 2.01960792690389E+28f;
        kostenEtageGesamt[645] = 2.22303302617667E+28f;
        kostenEtageGesamt[646] = 2.44019202249095E+28f;
        kostenEtageGesamt[647] = 2.68086298938097E+28f;
        kostenEtageGesamt[648] = 2.95259473105693E+28f;
        kostenEtageGesamt[649] = 3.23925159579925E+28f;
        kostenEtageGesamt[650] = 3.56532733783327E+28f;
        kostenEtageGesamt[651] = 3.9215531080573E+28f;
        kostenEtageGesamt[652] = 4.31152721265884E+28f;
        kostenEtageGesamt[653] = 4.74246121049342E+28f;
        kostenEtageGesamt[654] = 5.22552269654061E+28f;
        kostenEtageGesamt[655] = 5.75087665138939E+28f;
        kostenEtageGesamt[656] = 6.34178054166883E+28f;
        kostenEtageGesamt[657] = 6.98306069804731E+28f;
        kostenEtageGesamt[658] = 7.68216377668788E+28f;
        kostenEtageGesamt[659] = 8.45201692554278E+28f;
        kostenEtageGesamt[660] = 9.29376452995062E+28f;
        kostenEtageGesamt[661] = 1.02373166135036E+29f;
        kostenEtageGesamt[662] = 1.12131574219296E+29f;
        kostenEtageGesamt[663] = 1.23544164277804E+29f;
        kostenEtageGesamt[664] = 1.35745053176033E+29f;
        kostenEtageGesamt[665] = 1.49309078221154E+29f;
        kostenEtageGesamt[666] = 1.63936158010583E+29f;
        kostenEtageGesamt[667] = 1.80566805362816E+29f;
        kostenEtageGesamt[668] = 1.98662366613306E+29f;
        kostenEtageGesamt[669] = 2.18697506995865E+29f;
        kostenEtageGesamt[670] = 2.40859135558643E+29f;
        kostenEtageGesamt[671] = 2.6497877550363E+29f;
        kostenEtageGesamt[672] = 2.91006330593965E+29f;
        kostenEtageGesamt[673] = 3.20784493839426E+29f;
        kostenEtageGesamt[674] = 3.522967993647E+29f;
        kostenEtageGesamt[675] = 3.88025366219295E+29f;
        kostenEtageGesamt[676] = 4.28139364728053E+29f;
        kostenEtageGesamt[677] = 4.71790258222355E+29f;
        kostenEtageGesamt[678] = 5.18933961097313E+29f;
        kostenEtageGesamt[679] = 5.72576634232493E+29f;
        kostenEtageGesamt[680] = 6.29508109266934E+29f;
        kostenEtageGesamt[681] = 6.93011006324629E+29f;
        kostenEtageGesamt[682] = 7.63260908087752E+29f;
        kostenEtageGesamt[683] = 8.42842420432724E+29f;
        kostenEtageGesamt[684] = 9.25432898527647E+29f;
        kostenEtageGesamt[685] = 1.01698721247891E+30f;
        kostenEtageGesamt[686] = 1.1194645110128E+30f;
        kostenEtageGesamt[687] = 1.2343630848764E+30f;
        kostenEtageGesamt[688] = 1.35457441784291E+30f;
        kostenEtageGesamt[689] = 1.48957940900624E+30f;
        kostenEtageGesamt[690] = 1.63776607530163E+30f;
        kostenEtageGesamt[691] = 1.80170274352043E+30f;
        kostenEtageGesamt[692] = 1.98783366443495E+30f;
        kostenEtageGesamt[693] = 2.18552820073336E+30f;
        kostenEtageGesamt[694] = 2.39474065059726E+30f;
        kostenEtageGesamt[695] = 2.63502549700458E+30f;
        kostenEtageGesamt[696] = 2.90509316412843E+30f;
        kostenEtageGesamt[697] = 3.1966215776979E+30f;
        kostenEtageGesamt[698] = 3.52372930535831E+30f;
        kostenEtageGesamt[699] = 3.87300924371906E+30f;
        kostenEtageGesamt[700] = 4.25499960119452E+30f;
        kostenEtageGesamt[701] = 4.69955682354619E+30f;
        kostenEtageGesamt[702] = 5.16022854114979E+30f;
        kostenEtageGesamt[703] = 5.67104838179465E+30f;
        kostenEtageGesamt[704] = 6.24761261559061E+30f;
        kostenEtageGesamt[705] = 6.87864310004211E+30f;
        kostenEtageGesamt[706] = 7.54724598069073E+30f;
        kostenEtageGesamt[707] = 8.30272012177428E+30f;
        kostenEtageGesamt[708] = 9.10665536713515E+30f;
        kostenEtageGesamt[709] = 9.99995953267509E+30f;
        kostenEtageGesamt[710] = 1.10142457842388E+31f;
        kostenEtageGesamt[711] = 1.21195709851501E+31f;
        kostenEtageGesamt[712] = 1.33089591087389E+31f;
        kostenEtageGesamt[713] = 1.46486455079142E+31f;
        kostenEtageGesamt[714] = 1.61644825926373E+31f;
        kostenEtageGesamt[715] = 1.7783307323513E+31f;
        kostenEtageGesamt[716] = 1.96115962279889E+31f;
        kostenEtageGesamt[717] = 2.1512597954779E+31f;
        kostenEtageGesamt[718] = 2.36479523507746E+31f;
        kostenEtageGesamt[719] = 2.59570397835523E+31f;
        kostenEtageGesamt[720] = 2.8483526263137E+31f;
        kostenEtageGesamt[721] = 3.12961958017878E+31f;
        kostenEtageGesamt[722] = 3.44563573244675E+31f;
        kostenEtageGesamt[723] = 3.78138440154901E+31f;
        kostenEtageGesamt[724] = 4.15767846237262E+31f;
        kostenEtageGesamt[725] = 4.56809864664161E+31f;
        kostenEtageGesamt[726] = 5.03844339049668E+31f;
        kostenEtageGesamt[727] = 5.53894093508143E+31f;
        kostenEtageGesamt[728] = 6.08768947935587E+31f;
        kostenEtageGesamt[729] = 6.71155389194555E+31f;
        kostenEtageGesamt[730] = 7.36193443357405E+31f;
        kostenEtageGesamt[731] = 8.12451518491724E+31f;
        kostenEtageGesamt[732] = 8.93487574236872E+31f;
        kostenEtageGesamt[733] = 9.83590890229519E+31f;
        kostenEtageGesamt[734] = 1.08235432408558E+32f;
        kostenEtageGesamt[735] = 1.18912588973765E+32f;
        kostenEtageGesamt[736] = 1.30826305320572E+32f;
        kostenEtageGesamt[737] = 1.44485913612234E+32f;
        kostenEtageGesamt[738] = 1.59372037831476E+32f;
        kostenEtageGesamt[739] = 1.75390105496532E+32f;
        kostenEtageGesamt[740] = 1.92798614562913E+32f;
        kostenEtageGesamt[741] = 2.13187845741514E+32f;
        kostenEtageGesamt[742] = 2.34641935515239E+32f;
        kostenEtageGesamt[743] = 2.57933657183643E+32f;
        kostenEtageGesamt[744] = 2.82966339478261E+32f;
        kostenEtageGesamt[745] = 3.1211123980429E+32f;
        kostenEtageGesamt[746] = 3.4386092135121E+32f;
        kostenEtageGesamt[747] = 3.77432354372243E+32f;
        kostenEtageGesamt[748] = 4.15289513000896E+32f;
        kostenEtageGesamt[749] = 4.56018864594303E+32f;
        kostenEtageGesamt[750] = 5.00496074533457E+32f;
        kostenEtageGesamt[751] = 5.49802524387309E+32f;
        kostenEtageGesamt[752] = 6.04677189154702E+32f;
        kostenEtageGesamt[753] = 6.64193778285104E+32f;
        kostenEtageGesamt[754] = 7.30845465729217E+32f;
        kostenEtageGesamt[755] = 8.04716244135771E+32f;
        kostenEtageGesamt[756] = 8.85668355354812E+32f;
        kostenEtageGesamt[757] = 9.69185196700966E+32f;
        kostenEtageGesamt[758] = 1.06814246874606E+33f;
        kostenEtageGesamt[759] = 1.17767162046064E+33f;
        kostenEtageGesamt[760] = 1.29360133882471E+33f;
        kostenEtageGesamt[761] = 1.42226280414597E+33f;
        kostenEtageGesamt[762] = 1.57139619493755E+33f;
        kostenEtageGesamt[763] = 1.72816787462716E+33f;
        kostenEtageGesamt[764] = 1.90190723208459E+33f;
        kostenEtageGesamt[765] = 2.09757694768026E+33f;
        kostenEtageGesamt[766] = 2.30876030048758E+33f;
        kostenEtageGesamt[767] = 2.54799124360993E+33f;
        kostenEtageGesamt[768] = 2.81018222364192E+33f;
        kostenEtageGesamt[769] = 3.08210341316483E+33f;
        kostenEtageGesamt[770] = 3.39414911921936E+33f;
        kostenEtageGesamt[771] = 3.72301519969199E+33f;
        kostenEtageGesamt[772] = 4.09892688254696E+33f;
        kostenEtageGesamt[773] = 4.52039077281331E+33f;
        kostenEtageGesamt[774] = 4.97431323830221E+33f;
        kostenEtageGesamt[775] = 5.47667403965773E+33f;
        kostenEtageGesamt[776] = 6.04431369865962E+33f;
        kostenEtageGesamt[777] = 6.67347572067187E+33f;
        kostenEtageGesamt[778] = 7.32318002133382E+33f;
        kostenEtageGesamt[779] = 8.05074380570956E+33f;
        kostenEtageGesamt[780] = 8.8648871240956E+33f;
        kostenEtageGesamt[781] = 9.74942032374491E+33f;
        kostenEtageGesamt[782] = 1.07003906957376E+34f;
        kostenEtageGesamt[783] = 1.1785562142013E+34f;
        kostenEtageGesamt[784] = 1.30033332771238E+34f;
        kostenEtageGesamt[785] = 1.43100969584215E+34f;
        kostenEtageGesamt[786] = 1.57100814085082E+34f;
        kostenEtageGesamt[787] = 1.72782603767433E+34f;
        kostenEtageGesamt[788] = 1.90085484588183E+34f;
        kostenEtageGesamt[789] = 2.09357877179106E+34f;
        kostenEtageGesamt[790] = 2.3026305836886E+34f;
        kostenEtageGesamt[791] = 2.54181057684371E+34f;
        kostenEtageGesamt[792] = 2.80000524717539E+34f;
        kostenEtageGesamt[793] = 3.08202909239946E+34f;
        kostenEtageGesamt[794] = 3.38976585521273E+34f;
        kostenEtageGesamt[795] = 3.73086428597138E+34f;
        kostenEtageGesamt[796] = 4.10563850240538E+34f;
        kostenEtageGesamt[797] = 4.51407051159457E+34f;
        kostenEtageGesamt[798] = 4.97202072685199E+34f;
        kostenEtageGesamt[799] = 5.97202072685199E+34f;
    }

    // Upgradekosten Etage
    public void upgradeEtage1() {
        if (PlayerPrefs.GetFloat("cash") >= PlayerPrefs.GetFloat("kostenEtage1")) {
            PlayerPrefs.SetFloat("cash", PlayerPrefs.GetFloat("cash") - kostenEtage);
            PlayerPrefs.Save();
            switch (levelEtage1) {
                case 1:
                    kostenEtage = kostenEtageGesamt[0];
                    PlayerPrefs.SetFloat("etage1CashGain", 0.4f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 0.47f);
                    everEtageTheSame();
                    break;
                case 2:
                    kostenEtage = kostenEtageGesamt[1];
                    PlayerPrefs.SetFloat("etage1CashGain", 0.47f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 0.55f);
                    everEtageTheSame();
                    break;
                case 3:
                    kostenEtage = kostenEtageGesamt[2];
                    PlayerPrefs.SetFloat("etage1CashGain", 0.55f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 0.64f);
                    everEtageTheSame();
                    break;
                case 4:
                    kostenEtage = kostenEtageGesamt[3];
                    PlayerPrefs.SetFloat("etage1CashGain", 0.64f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 0.74f);
                    everEtageTheSame();
                    break;
                case 5:
                    kostenEtage = kostenEtageGesamt[4];
                    PlayerPrefs.SetFloat("etage1CashGain", 0.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 0.85f);
                    everEtageTheSame();
                    break;
                case 6:
                    kostenEtage = kostenEtageGesamt[5];
                    PlayerPrefs.SetFloat("etage1CashGain", 0.85f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 0.97f);
                    everEtageTheSame();
                    break;
                case 7:
                    kostenEtage = kostenEtageGesamt[6];
                    PlayerPrefs.SetFloat("etage1CashGain", 0.97f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.11f);
                    everEtageTheSame();
                    break;

                case 8:
                    kostenEtage = kostenEtageGesamt[7];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.11f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2f);
                    futureWorkersText.text = "+1";
                    everEtageTheSame();
                    break;

                case 9:
                    kostenEtage = kostenEtageGesamt[8];
                    PlayerPrefs.SetFloat("etage1CashGain", 2f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.13f);
                    everEtageTheSame();
                    currentWorkersText.text = "2";
                    futureWorkersText.text = "+0";
                    break;

                case 10:
                    kostenEtage = kostenEtageGesamt[9];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.13f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.25f);
                    everEtageTheSame();
                    break;

                case 11:
                    kostenEtage = kostenEtageGesamt[10];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.25f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.37f);
                    everEtageTheSame();
                    break;

                case 12:
                    kostenEtage = kostenEtageGesamt[11];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.37f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.5f);
                    everEtageTheSame();
                    break;

                case 13:
                    kostenEtage = kostenEtageGesamt[12];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.5f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.85f);
                    everEtageTheSame();
                    break;

                case 14:
                    kostenEtage = kostenEtageGesamt[13];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.85f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.95f);
                    everEtageTheSame();
                    break;

                case 15:
                    kostenEtage = kostenEtageGesamt[14];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.95f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.29f);
                    everEtageTheSame();
                    break;

                case 16:
                    kostenEtage = kostenEtageGesamt[15];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.29f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.75f);
                    everEtageTheSame();
                    break;

                case 17:
                    kostenEtage = kostenEtageGesamt[16];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.75f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.21f);
                    everEtageTheSame();
                    break;

                case 18:
                    kostenEtage = kostenEtageGesamt[17];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.76f);
                    everEtageTheSame();
                    break;

                case 19:
                    kostenEtage = kostenEtageGesamt[18];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.03f);
                    everEtageTheSame();
                    break;

                case 20:
                    kostenEtage = kostenEtageGesamt[19];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.03f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.68f);
                    everEtageTheSame();
                    break;

                case 21:
                    kostenEtage = kostenEtageGesamt[20];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.68f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.09f);
                    everEtageTheSame();
                    break;

                case 22:
                    kostenEtage = kostenEtageGesamt[21];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.09f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.74f);
                    everEtageTheSame();
                    break;

                case 23:
                    kostenEtage = kostenEtageGesamt[22];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10.19f);
                    everEtageTheSame();
                    break;

                case 24:
                    kostenEtage = kostenEtageGesamt[23];
                    PlayerPrefs.SetFloat("etage1CashGain", 10.19f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10.93f);
                    everEtageTheSame();
                    break;

                case 25:
                    kostenEtage = kostenEtageGesamt[24];
                    PlayerPrefs.SetFloat("etage1CashGain", 10.93f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11.7f);
                    everEtageTheSame();
                    break;

                case 26:
                    kostenEtage = kostenEtageGesamt[25];
                    PlayerPrefs.SetFloat("etage1CashGain", 11.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12.57f);
                    everEtageTheSame();
                    break;

                case 27:
                    kostenEtage = kostenEtageGesamt[26];
                    PlayerPrefs.SetFloat("etage1CashGain", 12.57f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13.54f);
                    everEtageTheSame();
                    break;

                case 28:
                    kostenEtage = kostenEtageGesamt[27];
                    PlayerPrefs.SetFloat("etage1CashGain", 13.54f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 14.55f);
                    everEtageTheSame();
                    break;

                case 29:
                    kostenEtage = kostenEtageGesamt[28];
                    PlayerPrefs.SetFloat("etage1CashGain", 14.55f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 15.57f);
                    everEtageTheSame();
                    break;

                case 30:
                    kostenEtage = kostenEtageGesamt[29];
                    PlayerPrefs.SetFloat("etage1CashGain", 15.57f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 16.7f);
                    everEtageTheSame();
                    break;

                case 31:
                    kostenEtage = kostenEtageGesamt[30];
                    PlayerPrefs.SetFloat("etage1CashGain", 16.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 17.9f);
                    everEtageTheSame();
                    break;

                case 32:
                    kostenEtage = kostenEtageGesamt[31];
                    PlayerPrefs.SetFloat("etage1CashGain", 17.9f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 19.15f);
                    everEtageTheSame();
                    break;

                case 33:
                    kostenEtage = kostenEtageGesamt[32];
                    PlayerPrefs.SetFloat("etage1CashGain", 19.15f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 20.57f);
                    everEtageTheSame();
                    break;

                case 34:
                    kostenEtage = kostenEtageGesamt[33];
                    PlayerPrefs.SetFloat("etage1CashGain", 20.57f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 22.1f);
                    everEtageTheSame();
                    break;

                case 35:
                    kostenEtage = kostenEtageGesamt[34];
                    PlayerPrefs.SetFloat("etage1CashGain", 22.1f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 23.67f);
                    everEtageTheSame();
                    break;

                case 36:
                    kostenEtage = kostenEtageGesamt[35];
                    PlayerPrefs.SetFloat("etage1CashGain", 23.67f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 25.4f);
                    everEtageTheSame();
                    break;

                case 37:
                    kostenEtage = kostenEtageGesamt[36];
                    PlayerPrefs.SetFloat("etage1CashGain", 25.4f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 27.29f);
                    everEtageTheSame();
                    break;

                case 38:
                    kostenEtage = kostenEtageGesamt[37];
                    PlayerPrefs.SetFloat("etage1CashGain", 27.29f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 29.36f);
                    everEtageTheSame();
                    break;

                case 39:
                    kostenEtage = kostenEtageGesamt[38];
                    PlayerPrefs.SetFloat("etage1CashGain", 29.36f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 31.65f);
                    everEtageTheSame();
                    break;

                case 40:
                    kostenEtage = kostenEtageGesamt[39];
                    PlayerPrefs.SetFloat("etage1CashGain", 31.65f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 34.07f);
                    everEtageTheSame();
                    break;

                case 41:
                    kostenEtage = kostenEtageGesamt[40];
                    PlayerPrefs.SetFloat("etage1CashGain", 34.07f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 36.67f);
                    everEtageTheSame();
                    break;

                case 42:
                    kostenEtage = kostenEtageGesamt[41];
                    PlayerPrefs.SetFloat("etage1CashGain", 36.67f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 39.32f);
                    everEtageTheSame();
                    break;

                case 43:
                    kostenEtage = kostenEtageGesamt[42];
                    PlayerPrefs.SetFloat("etage1CashGain", 39.32f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 42.15f);
                    everEtageTheSame();
                    break;

                case 44:
                    kostenEtage = kostenEtageGesamt[43];
                    PlayerPrefs.SetFloat("etage1CashGain", 42.15f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 45.14f);
                    everEtageTheSame();
                    break;

                case 45:
                    kostenEtage = kostenEtageGesamt[44];
                    PlayerPrefs.SetFloat("etage1CashGain", 45.14f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 48.63f);
                    everEtageTheSame();
                    break;

                case 46:
                    kostenEtage = kostenEtageGesamt[45];
                    PlayerPrefs.SetFloat("etage1CashGain", 48.63f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 52.16f);
                    everEtageTheSame();
                    break;

                case 47:
                    kostenEtage = kostenEtageGesamt[46];
                    PlayerPrefs.SetFloat("etage1CashGain", 52.16f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 56.08f);
                    everEtageTheSame();
                    break;

                case 48:
                    kostenEtage = kostenEtageGesamt[47];
                    PlayerPrefs.SetFloat("etage1CashGain", 56.08f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 77.33f);
                    futureWorkersText.text = "+1";
                    everEtageTheSame();
                    break;

                case 49:
                    kostenEtage = kostenEtageGesamt[48];
                    PlayerPrefs.SetFloat("etage1CashGain", 77.33f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 82.93f);
                    everEtageTheSame();
                    currentWorkersText.text = "3";
                    futureWorkersText.text = "+0";
                    break;

                case 50:
                    kostenEtage = kostenEtageGesamt[49];
                    PlayerPrefs.SetFloat("etage1CashGain", 82.93f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 88.88f);
                    everEtageTheSame();
                    break;

                case 51:
                    kostenEtage = kostenEtageGesamt[50];
                    PlayerPrefs.SetFloat("etage1CashGain", 88.88f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 95.5f);
                    everEtageTheSame();
                    break;

                case 52:
                    kostenEtage = kostenEtageGesamt[51];
                    PlayerPrefs.SetFloat("etage1CashGain", 95.5f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 102.48f);
                    everEtageTheSame();
                    break;

                case 53:
                    kostenEtage = kostenEtageGesamt[52];
                    PlayerPrefs.SetFloat("etage1CashGain", 102.48f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 109.63f);
                    everEtageTheSame();
                    break;

                case 54:
                    kostenEtage = kostenEtageGesamt[53];
                    PlayerPrefs.SetFloat("etage1CashGain", 109.63f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 117.83f);
                    everEtageTheSame();
                    break;

                case 55:
                    kostenEtage = kostenEtageGesamt[54];
                    PlayerPrefs.SetFloat("etage1CashGain", 117.83f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 126.71f);
                    everEtageTheSame();
                    break;

                case 56:
                    kostenEtage = kostenEtageGesamt[55];
                    PlayerPrefs.SetFloat("etage1CashGain", 126.71f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 136.55f);
                    everEtageTheSame();
                    break;

                case 57:
                    kostenEtage = kostenEtageGesamt[56];
                    PlayerPrefs.SetFloat("etage1CashGain", 136.55f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 146.74f);
                    everEtageTheSame();
                    break;

                case 58:
                    kostenEtage = kostenEtageGesamt[57];
                    PlayerPrefs.SetFloat("etage1CashGain", 146.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 158.57f);
                    everEtageTheSame();
                    break;

                case 59:
                    kostenEtage = kostenEtageGesamt[58];
                    PlayerPrefs.SetFloat("etage1CashGain", 158.57f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 170.89f);
                    everEtageTheSame();
                    break;

                case 60:
                    kostenEtage = kostenEtageGesamt[59];
                    PlayerPrefs.SetFloat("etage1CashGain", 170.89f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 184.94f);
                    everEtageTheSame();
                    break;

                case 61:
                    kostenEtage = kostenEtageGesamt[60];
                    PlayerPrefs.SetFloat("etage1CashGain", 184.94f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 198.08f);
                    everEtageTheSame();
                    break;

                case 62:
                    kostenEtage = kostenEtageGesamt[61];
                    PlayerPrefs.SetFloat("etage1CashGain", 198.08f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 212.55f);
                    everEtageTheSame();
                    break;

                case 63:
                    kostenEtage = kostenEtageGesamt[62];
                    PlayerPrefs.SetFloat("etage1CashGain", 212.55f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 227.99f);
                    everEtageTheSame();
                    break;

                case 64:
                    kostenEtage = kostenEtageGesamt[63];
                    PlayerPrefs.SetFloat("etage1CashGain", 227.99f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 244.9f);
                    everEtageTheSame();
                    break;

                case 65:
                    kostenEtage = kostenEtageGesamt[64];
                    PlayerPrefs.SetFloat("etage1CashGain", 244.9f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 262.89f);
                    everEtageTheSame();
                    break;

                case 66:
                    kostenEtage = kostenEtageGesamt[65];
                    PlayerPrefs.SetFloat("etage1CashGain", 262.89f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 281.13f);
                    everEtageTheSame();
                    break;

                case 67:
                    kostenEtage = kostenEtageGesamt[66];
                    PlayerPrefs.SetFloat("etage1CashGain", 281.13f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 301.54f);
                    everEtageTheSame();
                    break;

                case 68:
                    kostenEtage = kostenEtageGesamt[67];
                    PlayerPrefs.SetFloat("etage1CashGain", 301.54f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 324.36f);
                    everEtageTheSame();
                    break;

                case 69:
                    kostenEtage = kostenEtageGesamt[68];
                    PlayerPrefs.SetFloat("etage1CashGain", 324.36f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 349.11f);
                    everEtageTheSame();
                    break;

                case 70:
                    kostenEtage = kostenEtageGesamt[69];
                    PlayerPrefs.SetFloat("etage1CashGain", 349.11f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 374.84f);
                    everEtageTheSame();
                    break;

                case 71:
                    kostenEtage = kostenEtageGesamt[70];
                    PlayerPrefs.SetFloat("etage1CashGain", 374.84f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 404.43f);
                    everEtageTheSame();
                    break;

                case 72:
                    kostenEtage = kostenEtageGesamt[71];
                    PlayerPrefs.SetFloat("etage1CashGain", 404.43f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 434.53f);
                    everEtageTheSame();
                    break;

                case 73:
                    kostenEtage = kostenEtageGesamt[72];
                    PlayerPrefs.SetFloat("etage1CashGain", 434.53f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 465.51f);
                    everEtageTheSame();
                    break;

                case 74:
                    kostenEtage = kostenEtageGesamt[73];
                    PlayerPrefs.SetFloat("etage1CashGain", 465.51f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 499.94f);
                    everEtageTheSame();
                    break;

                case 75:
                    kostenEtage = kostenEtageGesamt[74];
                    PlayerPrefs.SetFloat("etage1CashGain", 499.94f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 536.78f);
                    everEtageTheSame();
                    break;

                case 76:
                    kostenEtage = kostenEtageGesamt[75];
                    PlayerPrefs.SetFloat("etage1CashGain", 536.78f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 577.53f);
                    everEtageTheSame();
                    break;

                case 77:
                    kostenEtage = kostenEtageGesamt[76];
                    PlayerPrefs.SetFloat("etage1CashGain", 577.53f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 621.14f);
                    everEtageTheSame();
                    break;

                case 78:
                    kostenEtage = kostenEtageGesamt[77];
                    PlayerPrefs.SetFloat("etage1CashGain", 621.14f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 668.76f);
                    everEtageTheSame();
                    break;

                case 79:
                    kostenEtage = kostenEtageGesamt[78];
                    PlayerPrefs.SetFloat("etage1CashGain", 668.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 719.99f);
                    everEtageTheSame();
                    break;

                case 80:
                    kostenEtage = kostenEtageGesamt[79];
                    PlayerPrefs.SetFloat("etage1CashGain", 719.99f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 776.44f);
                    everEtageTheSame();
                    break;

                case 81:
                    kostenEtage = kostenEtageGesamt[80];
                    PlayerPrefs.SetFloat("etage1CashGain", 776.44f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 829.24f);
                    everEtageTheSame();
                    break;

                case 82:
                    kostenEtage = kostenEtageGesamt[81];
                    PlayerPrefs.SetFloat("etage1CashGain", 829.24f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 893.76f);
                    everEtageTheSame();
                    break;

                case 83:
                    kostenEtage = kostenEtageGesamt[82];
                    PlayerPrefs.SetFloat("etage1CashGain", 893.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 962.39f);
                    everEtageTheSame();
                    break;

                case 84:
                    kostenEtage = kostenEtageGesamt[83];
                    PlayerPrefs.SetFloat("etage1CashGain", 962.39f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1035.74f);
                    everEtageTheSame();
                    break;

                case 85:
                    kostenEtage = kostenEtageGesamt[84];
                    PlayerPrefs.SetFloat("etage1CashGain", 1035.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1111.69f);
                    everEtageTheSame();
                    break;

                case 86:
                    kostenEtage = kostenEtageGesamt[85];
                    PlayerPrefs.SetFloat("etage1CashGain", 1111.69f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1193f);
                    everEtageTheSame();
                    break;

                case 87:
                    kostenEtage = kostenEtageGesamt[86];
                    PlayerPrefs.SetFloat("etage1CashGain", 1193f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1276.57f);
                    everEtageTheSame();
                    break;

                case 88:
                    kostenEtage = kostenEtageGesamt[87];
                    PlayerPrefs.SetFloat("etage1CashGain", 1276.57f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1371.26f);
                    everEtageTheSame();
                    break;

                case 89:
                    kostenEtage = kostenEtageGesamt[88];
                    PlayerPrefs.SetFloat("etage1CashGain", 1371.26f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1476.83f);
                    everEtageTheSame();
                    break;

                case 90:
                    kostenEtage = kostenEtageGesamt[89];
                    PlayerPrefs.SetFloat("etage1CashGain", 1476.83f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1587.82f);
                    everEtageTheSame();
                    break;

                case 91:
                    kostenEtage = kostenEtageGesamt[90];
                    PlayerPrefs.SetFloat("etage1CashGain", 1587.82f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1710.96f);
                    everEtageTheSame();
                    break;

                case 92:
                    kostenEtage = kostenEtageGesamt[91];
                    PlayerPrefs.SetFloat("etage1CashGain", 1710.96f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1839.42f);
                    everEtageTheSame();
                    break;

                case 93:
                    kostenEtage = kostenEtageGesamt[92];
                    PlayerPrefs.SetFloat("etage1CashGain", 1839.42f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1983.32f);
                    everEtageTheSame();
                    break;

                case 94:
                    kostenEtage = kostenEtageGesamt[93];
                    PlayerPrefs.SetFloat("etage1CashGain", 1983.32f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2125.18f);
                    everEtageTheSame();
                    break;

                case 95:
                    kostenEtage = kostenEtageGesamt[94];
                    PlayerPrefs.SetFloat("etage1CashGain", 2125.18f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2290.36f);
                    everEtageTheSame();
                    break;

                case 96:
                    kostenEtage = kostenEtageGesamt[95];
                    PlayerPrefs.SetFloat("etage1CashGain", 2290.36f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2466.57f);
                    everEtageTheSame();
                    break;

                case 97:
                    kostenEtage = kostenEtageGesamt[96];
                    PlayerPrefs.SetFloat("etage1CashGain", 2466.57f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2644.16f);
                    everEtageTheSame();
                    break;

                case 98:
                    kostenEtage = kostenEtageGesamt[97];
                    PlayerPrefs.SetFloat("etage1CashGain", 2644.16f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4776.54f);
                    everEtageTheSame();
                    futureWorkersText.text = "+1";
                    break;

                case 99:
                    kostenEtage = kostenEtageGesamt[98];
                    PlayerPrefs.SetFloat("etage1CashGain", 4776.54f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5088.25f);
                    everEtageTheSame();
                    currentWorkersText.text = "4";
                    futureWorkersText.text = "+0";
                    break;

                case 100:
                    kostenEtage = kostenEtageGesamt[99];
                    PlayerPrefs.SetFloat("etage1CashGain", 5088.25f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5449.49f);
                    everEtageTheSame();
                    break;

                case 101:
                    kostenEtage = kostenEtageGesamt[100];
                    PlayerPrefs.SetFloat("etage1CashGain", 5449.49f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5821.56f);
                    everEtageTheSame();
                    break;

                case 102:
                    kostenEtage = kostenEtageGesamt[101];
                    PlayerPrefs.SetFloat("etage1CashGain", 5821.56f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6176.85f);
                    everEtageTheSame();
                    break;

                case 103:
                    kostenEtage = kostenEtageGesamt[102];
                    PlayerPrefs.SetFloat("etage1CashGain", 6176.85f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6553.22f);
                    everEtageTheSame();
                    break;

                case 104:
                    kostenEtage = kostenEtageGesamt[103];
                    PlayerPrefs.SetFloat("etage1CashGain", 6553.22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6978.8f);
                    everEtageTheSame();
                    break;

                case 105:
                    kostenEtage = kostenEtageGesamt[104];
                    PlayerPrefs.SetFloat("etage1CashGain", 6978.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7426f);
                    everEtageTheSame();
                    break;

                case 106:
                    kostenEtage = kostenEtageGesamt[105];
                    PlayerPrefs.SetFloat("etage1CashGain", 7426f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7930.86f);
                    everEtageTheSame();
                    break;

                case 107:
                    kostenEtage = kostenEtageGesamt[106];
                    PlayerPrefs.SetFloat("etage1CashGain", 7930.86f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8426.44f);
                    everEtageTheSame();
                    break;

                case 108:
                    kostenEtage = kostenEtageGesamt[107];
                    PlayerPrefs.SetFloat("etage1CashGain", 8426.44f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8936f);
                    everEtageTheSame();
                    break;

                case 109:
                    kostenEtage = kostenEtageGesamt[108];
                    PlayerPrefs.SetFloat("etage1CashGain", 8936f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9509.53f);
                    everEtageTheSame();
                    break;

                case 110:
                    kostenEtage = kostenEtageGesamt[109];
                    PlayerPrefs.SetFloat("etage1CashGain", 9509.53f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10148.32f);
                    everEtageTheSame();
                    break;

                case 111:
                    kostenEtage = kostenEtageGesamt[110];
                    PlayerPrefs.SetFloat("etage1CashGain", 10148.32f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10839.43f);
                    everEtageTheSame();
                    break;

                case 112:
                    kostenEtage = kostenEtageGesamt[111];
                    PlayerPrefs.SetFloat("etage1CashGain", 10839.43f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11523.67f);
                    everEtageTheSame();
                    break;

                case 113:
                    kostenEtage = kostenEtageGesamt[112];
                    PlayerPrefs.SetFloat("etage1CashGain", 11523.67f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12265.64f);
                    everEtageTheSame();
                    break;

                case 114:
                    kostenEtage = kostenEtageGesamt[113];
                    PlayerPrefs.SetFloat("etage1CashGain", 12265.64f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13055.45f);
                    everEtageTheSame();
                    break;

                case 115:
                    kostenEtage = kostenEtageGesamt[114];
                    PlayerPrefs.SetFloat("etage1CashGain", 13055.45f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13925.63f);
                    everEtageTheSame();
                    break;

                case 116:
                    kostenEtage = kostenEtageGesamt[115];
                    PlayerPrefs.SetFloat("etage1CashGain", 13925.63f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 14828f);
                    everEtageTheSame();
                    break;

                case 117:
                    kostenEtage = kostenEtageGesamt[116];
                    PlayerPrefs.SetFloat("etage1CashGain", 14828f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 15824.15f);
                    everEtageTheSame();
                    break;

                case 118:
                    kostenEtage = kostenEtageGesamt[117];
                    PlayerPrefs.SetFloat("etage1CashGain", 15824.15f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 16882.12f);
                    everEtageTheSame();
                    break;

                case 119:
                    kostenEtage = kostenEtageGesamt[118];
                    PlayerPrefs.SetFloat("etage1CashGain", 16882.12f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 17998.89f);
                    everEtageTheSame();
                    break;

                case 120:
                    kostenEtage = kostenEtageGesamt[119];
                    PlayerPrefs.SetFloat("etage1CashGain", 17998.89f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 19158.75f);
                    everEtageTheSame();
                    break;

                case 121:
                    kostenEtage = kostenEtageGesamt[120];
                    PlayerPrefs.SetFloat("etage1CashGain", 19158.75f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 20347.38f);
                    everEtageTheSame();
                    break;

                case 122:
                    kostenEtage = kostenEtageGesamt[121];
                    PlayerPrefs.SetFloat("etage1CashGain", 20347.38f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 21706.99f);
                    everEtageTheSame();
                    break;

                case 123:
                    kostenEtage = kostenEtageGesamt[122];
                    PlayerPrefs.SetFloat("etage1CashGain", 21706.99f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 23069.84f);
                    everEtageTheSame();
                    break;

                case 124:
                    kostenEtage = kostenEtageGesamt[123];
                    PlayerPrefs.SetFloat("etage1CashGain", 23069.84f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 24551.4f);
                    everEtageTheSame();
                    break;

                case 125:
                    kostenEtage = kostenEtageGesamt[124];
                    PlayerPrefs.SetFloat("etage1CashGain", 24551.4f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 26128.32f);
                    everEtageTheSame();
                    break;

                case 126:
                    kostenEtage = kostenEtageGesamt[125];
                    PlayerPrefs.SetFloat("etage1CashGain", 26128.32f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 27812.05f);
                    everEtageTheSame();
                    break;

                case 127:
                    kostenEtage = kostenEtageGesamt[126];
                    PlayerPrefs.SetFloat("etage1CashGain", 27812.05f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 29626.55f);
                    everEtageTheSame();
                    break;

                case 128:
                    kostenEtage = kostenEtageGesamt[127];
                    PlayerPrefs.SetFloat("etage1CashGain", 29626.55f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 31598.49f);
                    everEtageTheSame();
                    break;

                case 129:
                    kostenEtage = kostenEtageGesamt[128];
                    PlayerPrefs.SetFloat("etage1CashGain", 31598.49f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 33609.38f);
                    everEtageTheSame();
                    break;

                case 130:
                    kostenEtage = kostenEtageGesamt[129];
                    PlayerPrefs.SetFloat("etage1CashGain", 33609.38f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 35747.63f);
                    everEtageTheSame();
                    break;

                case 131:
                    kostenEtage = kostenEtageGesamt[130];
                    PlayerPrefs.SetFloat("etage1CashGain", 35747.63f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 38051.19f);
                    everEtageTheSame();
                    break;

                case 132:
                    kostenEtage = kostenEtageGesamt[131];
                    PlayerPrefs.SetFloat("etage1CashGain", 38051.19f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 40580.46f);
                    everEtageTheSame();
                    break;

                case 133:
                    kostenEtage = kostenEtageGesamt[132];
                    PlayerPrefs.SetFloat("etage1CashGain", 40580.46f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 42909.49f);
                    everEtageTheSame();
                    break;

                case 134:
                    kostenEtage = kostenEtageGesamt[133];
                    PlayerPrefs.SetFloat("etage1CashGain", 42909.49f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 45680.17f);
                    everEtageTheSame();
                    break;

                case 135:
                    kostenEtage = kostenEtageGesamt[134];
                    PlayerPrefs.SetFloat("etage1CashGain", 45680.17f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 48932.87f);
                    everEtageTheSame();
                    break;

                case 136:
                    kostenEtage = kostenEtageGesamt[135];
                    PlayerPrefs.SetFloat("etage1CashGain", 48932.87f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 52052.39f);
                    everEtageTheSame();
                    break;

                case 137:
                    kostenEtage = kostenEtageGesamt[136];
                    PlayerPrefs.SetFloat("etage1CashGain", 52052.39f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 55420.12f);
                    everEtageTheSame();
                    break;

                case 138:
                    kostenEtage = kostenEtageGesamt[137];
                    PlayerPrefs.SetFloat("etage1CashGain", 55420.12f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 59191.39f);
                    everEtageTheSame();
                    break;

                case 139:
                    kostenEtage = kostenEtageGesamt[138];
                    PlayerPrefs.SetFloat("etage1CashGain", 59191.39f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 62981.38f);
                    everEtageTheSame();
                    break;

                case 140:
                    kostenEtage = kostenEtageGesamt[139];
                    PlayerPrefs.SetFloat("etage1CashGain", 62981.38f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 67202.16f);
                    everEtageTheSame();
                    break;

                case 141:
                    kostenEtage = kostenEtageGesamt[140];
                    PlayerPrefs.SetFloat("etage1CashGain", 67202.16f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 71728.78f);
                    everEtageTheSame();
                    break;

                case 142:
                    kostenEtage = kostenEtageGesamt[141];
                    PlayerPrefs.SetFloat("etage1CashGain", 71728.78f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 76315.71f);
                    everEtageTheSame();
                    break;

                case 143:
                    kostenEtage = kostenEtageGesamt[142];
                    PlayerPrefs.SetFloat("etage1CashGain", 76315.71f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 81275.63f);
                    everEtageTheSame();
                    break;

                case 144:
                    kostenEtage = kostenEtageGesamt[143];
                    PlayerPrefs.SetFloat("etage1CashGain", 81275.63f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 86572.21f);
                    everEtageTheSame();
                    break;

                case 145:
                    kostenEtage = kostenEtageGesamt[144];
                    PlayerPrefs.SetFloat("etage1CashGain", 86572.21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 92799.05f);
                    everEtageTheSame();
                    break;

                case 146:
                    kostenEtage = kostenEtageGesamt[145];
                    PlayerPrefs.SetFloat("etage1CashGain", 92799.05f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 98992.6f);
                    everEtageTheSame();
                    break;

                case 147:
                    kostenEtage = kostenEtageGesamt[146];
                    PlayerPrefs.SetFloat("etage1CashGain", 98992.6f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 105015.78f);
                    everEtageTheSame();
                    break;

                case 148:
                    kostenEtage = kostenEtageGesamt[147];
                    PlayerPrefs.SetFloat("etage1CashGain", 105015.78f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 112152.83f);
                    everEtageTheSame();
                    break;

                case 149:
                    kostenEtage = kostenEtageGesamt[148];
                    PlayerPrefs.SetFloat("etage1CashGain", 112152.83f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 119707.42f);
                    everEtageTheSame();
                    break;

                case 150:
                    kostenEtage = kostenEtageGesamt[149];
                    PlayerPrefs.SetFloat("etage1CashGain", 119707.42f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 128195.71f);
                    everEtageTheSame();
                    break;

                case 151:
                    kostenEtage = kostenEtageGesamt[150];
                    PlayerPrefs.SetFloat("etage1CashGain", 128195.71f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 137375.02f);
                    everEtageTheSame();
                    break;

                case 152:
                    kostenEtage = kostenEtageGesamt[151];
                    PlayerPrefs.SetFloat("etage1CashGain", 137375.02f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 146263.02f);
                    everEtageTheSame();
                    break;

                case 153:
                    kostenEtage = kostenEtageGesamt[152];
                    PlayerPrefs.SetFloat("etage1CashGain", 146263.02f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 156504.82f);
                    everEtageTheSame();
                    break;

                case 154:
                    kostenEtage = kostenEtageGesamt[153];
                    PlayerPrefs.SetFloat("etage1CashGain", 156504.82f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 166285.6f);
                    everEtageTheSame();
                    break;

                case 155:
                    kostenEtage = kostenEtageGesamt[154];
                    PlayerPrefs.SetFloat("etage1CashGain", 166285.6f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 176301.08f);
                    everEtageTheSame();
                    break;

                case 156:
                    kostenEtage = kostenEtageGesamt[155];
                    PlayerPrefs.SetFloat("etage1CashGain", 176301.08f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 187258.2f);
                    everEtageTheSame();
                    break;

                case 157:
                    kostenEtage = kostenEtageGesamt[156];
                    PlayerPrefs.SetFloat("etage1CashGain", 187258.2f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 198593.95f);
                    everEtageTheSame();
                    break;

                case 158:
                    kostenEtage = kostenEtageGesamt[157];
                    PlayerPrefs.SetFloat("etage1CashGain", 198593.95f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 211334.16f);
                    everEtageTheSame();
                    break;

                case 159:
                    kostenEtage = kostenEtageGesamt[158];
                    PlayerPrefs.SetFloat("etage1CashGain", 211334.16f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 225533.8f);
                    everEtageTheSame();
                    break;

                case 160:
                    kostenEtage = kostenEtageGesamt[159];
                    PlayerPrefs.SetFloat("etage1CashGain", 225533.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 239593.81f);
                    everEtageTheSame();
                    break;

                case 161:
                    kostenEtage = kostenEtageGesamt[160];
                    PlayerPrefs.SetFloat("etage1CashGain", 239593.81f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 254731.59f);
                    everEtageTheSame();
                    break;

                case 162:
                    kostenEtage = kostenEtageGesamt[161];
                    PlayerPrefs.SetFloat("etage1CashGain", 254731.59f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 271629.12f);
                    everEtageTheSame();
                    break;

                case 163:
                    kostenEtage = kostenEtageGesamt[162];
                    PlayerPrefs.SetFloat("etage1CashGain", 271629.12f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 289338.35f);
                    everEtageTheSame();
                    break;

                case 164:
                    kostenEtage = kostenEtageGesamt[163];
                    PlayerPrefs.SetFloat("etage1CashGain", 289338.35f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 308539.24f);
                    everEtageTheSame();
                    break;

                case 165:
                    kostenEtage = kostenEtageGesamt[164];
                    PlayerPrefs.SetFloat("etage1CashGain", 308539.24f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 327766.41f);
                    everEtageTheSame();
                    break;

                case 166:
                    kostenEtage = kostenEtageGesamt[165];
                    PlayerPrefs.SetFloat("etage1CashGain", 327766.41f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 349451.13f);
                    everEtageTheSame();
                    break;

                case 167:
                    kostenEtage = kostenEtageGesamt[166];
                    PlayerPrefs.SetFloat("etage1CashGain", 349451.13f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 373557.05f);
                    everEtageTheSame();
                    break;

                case 168:
                    kostenEtage = kostenEtageGesamt[167];
                    PlayerPrefs.SetFloat("etage1CashGain", 373557.05f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 397848.76f);
                    everEtageTheSame();
                    break;

                case 169:
                    kostenEtage = kostenEtageGesamt[168];
                    PlayerPrefs.SetFloat("etage1CashGain", 397848.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 425152.53f);
                    everEtageTheSame();
                    break;

                case 170:
                    kostenEtage = kostenEtageGesamt[169];
                    PlayerPrefs.SetFloat("etage1CashGain", 425152.53f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 452873.85f);
                    everEtageTheSame();
                    break;

                case 171:
                    kostenEtage = kostenEtageGesamt[170];
                    PlayerPrefs.SetFloat("etage1CashGain", 452873.85f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 482630.74f);
                    everEtageTheSame();
                    break;

                case 172:
                    kostenEtage = kostenEtageGesamt[171];
                    PlayerPrefs.SetFloat("etage1CashGain", 482630.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 512932.97f);
                    everEtageTheSame();
                    break;

                case 173:
                    kostenEtage = kostenEtageGesamt[172];
                    PlayerPrefs.SetFloat("etage1CashGain", 512932.97f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 547372.91f);
                    everEtageTheSame();
                    break;

                case 174:
                    kostenEtage = kostenEtageGesamt[173];
                    PlayerPrefs.SetFloat("etage1CashGain", 547372.91f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 580744.5f);
                    everEtageTheSame();
                    break;

                case 175:
                    kostenEtage = kostenEtageGesamt[174];
                    PlayerPrefs.SetFloat("etage1CashGain", 580744.5f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 614407.37f);
                    everEtageTheSame();
                    break;

                case 176:
                    kostenEtage = kostenEtageGesamt[175];
                    PlayerPrefs.SetFloat("etage1CashGain", 614407.37f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 655665.32f);
                    everEtageTheSame();
                    break;

                case 177:
                    kostenEtage = kostenEtageGesamt[176];
                    PlayerPrefs.SetFloat("etage1CashGain", 655665.32f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 698270.7f);
                    everEtageTheSame();
                    break;

                case 178:
                    kostenEtage = kostenEtageGesamt[177];
                    PlayerPrefs.SetFloat("etage1CashGain", 698270.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 741248.35f);
                    everEtageTheSame();
                    break;

                case 179:
                    kostenEtage = kostenEtageGesamt[178];
                    PlayerPrefs.SetFloat("etage1CashGain", 741248.35f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 790074.72f);
                    everEtageTheSame();
                    break;

                case 180:
                    kostenEtage = kostenEtageGesamt[179];
                    PlayerPrefs.SetFloat("etage1CashGain", 790074.72f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 839567.07f);
                    everEtageTheSame();
                    break;

                case 181:
                    kostenEtage = kostenEtageGesamt[180];
                    PlayerPrefs.SetFloat("etage1CashGain", 839567.07f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 897618.66f);
                    everEtageTheSame();
                    break;

                case 182:
                    kostenEtage = kostenEtageGesamt[181];
                    PlayerPrefs.SetFloat("etage1CashGain", 897618.66f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 954449.58f);
                    everEtageTheSame();
                    break;

                case 183:
                    kostenEtage = kostenEtageGesamt[182];
                    PlayerPrefs.SetFloat("etage1CashGain", 954449.58f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1011459.88f);
                    everEtageTheSame();
                    break;

                case 184:
                    kostenEtage = kostenEtageGesamt[183];
                    PlayerPrefs.SetFloat("etage1CashGain", 1011459.88f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1080327.04f);
                    everEtageTheSame();
                    break;

                case 185:
                    kostenEtage = kostenEtageGesamt[184];
                    PlayerPrefs.SetFloat("etage1CashGain", 1080327.04f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1153254.49f);
                    everEtageTheSame();
                    break;

                case 186:
                    kostenEtage = kostenEtageGesamt[185];
                    PlayerPrefs.SetFloat("etage1CashGain", 1153254.49f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1228445.06f);
                    everEtageTheSame();
                    break;

                case 187:
                    kostenEtage = kostenEtageGesamt[186];
                    PlayerPrefs.SetFloat("etage1CashGain", 1228445.06f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1310482.68f);
                    everEtageTheSame();
                    break;

                case 188:
                    kostenEtage = kostenEtageGesamt[187];
                    PlayerPrefs.SetFloat("etage1CashGain", 1310482.68f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1393690.87f);
                    everEtageTheSame();
                    break;

                case 189:
                    kostenEtage = kostenEtageGesamt[188];
                    PlayerPrefs.SetFloat("etage1CashGain", 1393690.87f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1475518.36f);
                    everEtageTheSame();
                    break;

                case 190:
                    kostenEtage = kostenEtageGesamt[189];
                    PlayerPrefs.SetFloat("etage1CashGain", 1475518.36f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1567886.31f);
                    everEtageTheSame();
                    break;

                case 191:
                    kostenEtage = kostenEtageGesamt[190];
                    PlayerPrefs.SetFloat("etage1CashGain", 1567886.31f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1671846.63f);
                    everEtageTheSame();
                    break;

                case 192:
                    kostenEtage = kostenEtageGesamt[191];
                    PlayerPrefs.SetFloat("etage1CashGain", 1671846.63f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1784495.19f);
                    everEtageTheSame();
                    break;

                case 193:
                    kostenEtage = kostenEtageGesamt[192];
                    PlayerPrefs.SetFloat("etage1CashGain", 1784495.19f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1898389.78f);
                    everEtageTheSame();
                    break;

                case 194:
                    kostenEtage = kostenEtageGesamt[193];
                    PlayerPrefs.SetFloat("etage1CashGain", 1898389.78f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2023081.99f);
                    everEtageTheSame();
                    break;

                case 195:
                    kostenEtage = kostenEtageGesamt[194];
                    PlayerPrefs.SetFloat("etage1CashGain", 2023081.99f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2150210.57f);
                    everEtageTheSame();
                    break;

                case 196:
                    kostenEtage = kostenEtageGesamt[195];
                    PlayerPrefs.SetFloat("etage1CashGain", 2150210.57f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2286990.78f);
                    everEtageTheSame();
                    break;

                case 197:
                    kostenEtage = kostenEtageGesamt[196];
                    PlayerPrefs.SetFloat("etage1CashGain", 2286990.78f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2424757.82f);
                    everEtageTheSame();
                    break;

                case 198:
                    kostenEtage = kostenEtageGesamt[197];
                    PlayerPrefs.SetFloat("etage1CashGain", 2424757.82f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3879612.51f);
                    everEtageTheSame();
                    break;

                case 199:
                    kostenEtage = kostenEtageGesamt[198];
                    PlayerPrefs.SetFloat("etage1CashGain", 3879612.51f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4111397.32f);
                    everEtageTheSame();
                    break;

                case 200:
                    kostenEtage = kostenEtageGesamt[199];
                    PlayerPrefs.SetFloat("etage1CashGain", 4111397.32f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4365726.65f);
                    everEtageTheSame();
                    break;

                case 201:
                    kostenEtage = kostenEtageGesamt[200];
                    PlayerPrefs.SetFloat("etage1CashGain", 4365726.65f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4631805.93f);
                    everEtageTheSame();
                    break;

                case 202:
                    kostenEtage = kostenEtageGesamt[201];
                    PlayerPrefs.SetFloat("etage1CashGain", 4631805.93f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4918025.29f);
                    everEtageTheSame();
                    break;

                case 203:
                    kostenEtage = kostenEtageGesamt[202];
                    PlayerPrefs.SetFloat("etage1CashGain", 4918025.29f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5228909.65f);
                    everEtageTheSame();
                    break;

                case 204:
                    kostenEtage = kostenEtageGesamt[203];
                    PlayerPrefs.SetFloat("etage1CashGain", 5228909.65f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5592089.81f);
                    everEtageTheSame();
                    break;

                case 205:
                    kostenEtage = kostenEtageGesamt[204];
                    PlayerPrefs.SetFloat("etage1CashGain", 5592089.81f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5969330.97f);
                    everEtageTheSame();
                    break;

                case 206:
                    kostenEtage = kostenEtageGesamt[205];
                    PlayerPrefs.SetFloat("etage1CashGain", 5969330.97f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6351324.6f);
                    everEtageTheSame();
                    break;

                case 207:
                    kostenEtage = kostenEtageGesamt[206];
                    PlayerPrefs.SetFloat("etage1CashGain", 6351324.6f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6736655.63f);
                    everEtageTheSame();
                    break;

                case 208:
                    kostenEtage = kostenEtageGesamt[207];
                    PlayerPrefs.SetFloat("etage1CashGain", 6736655.63f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7216348.18f);
                    everEtageTheSame();
                    break;

                case 209:
                    kostenEtage = kostenEtageGesamt[208];
                    PlayerPrefs.SetFloat("etage1CashGain", 7216348.18f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7672190.3f);
                    everEtageTheSame();
                    break;

                case 210:
                    kostenEtage = kostenEtageGesamt[209];
                    PlayerPrefs.SetFloat("etage1CashGain", 7672190.3f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8162378.89f);
                    everEtageTheSame();
                    break;

                case 211:
                    kostenEtage = kostenEtageGesamt[210];
                    PlayerPrefs.SetFloat("etage1CashGain", 8162378.89f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8697758.12f);
                    everEtageTheSame();
                    break;

                case 212:
                    kostenEtage = kostenEtageGesamt[211];
                    PlayerPrefs.SetFloat("etage1CashGain", 8697758.12f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9280730.11f);
                    everEtageTheSame();
                    break;

                case 213:
                    kostenEtage = kostenEtageGesamt[212];
                    PlayerPrefs.SetFloat("etage1CashGain", 9280730.11f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9866195.35f);
                    everEtageTheSame();
                    break;

                case 214:
                    kostenEtage = kostenEtageGesamt[213];
                    PlayerPrefs.SetFloat("etage1CashGain", 9866195.35f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10499048.27f);
                    everEtageTheSame();
                    break;

                case 215:
                    kostenEtage = kostenEtageGesamt[214];
                    PlayerPrefs.SetFloat("etage1CashGain", 10499048.27f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11185974.47f);
                    everEtageTheSame();
                    break;

                case 216:
                    kostenEtage = kostenEtageGesamt[215];
                    PlayerPrefs.SetFloat("etage1CashGain", 11185974.47f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11889679.26f);
                    everEtageTheSame();
                    break;

                case 217:
                    kostenEtage = kostenEtageGesamt[216];
                    PlayerPrefs.SetFloat("etage1CashGain", 11889679.26f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12691654.92f);
                    everEtageTheSame();
                    break;

                case 218:
                    kostenEtage = kostenEtageGesamt[217];
                    PlayerPrefs.SetFloat("etage1CashGain", 12691654.92f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13539811.29f);
                    everEtageTheSame();
                    break;

                case 219:
                    kostenEtage = kostenEtageGesamt[218];
                    PlayerPrefs.SetFloat("etage1CashGain", 13539811.29f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 14434610.55f);
                    everEtageTheSame();
                    break;

                case 220:
                    kostenEtage = kostenEtageGesamt[219];
                    PlayerPrefs.SetFloat("etage1CashGain", 14434610.55f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 15363687.07f);
                    everEtageTheSame();
                    break;

                case 221:
                    kostenEtage = kostenEtageGesamt[220];
                    PlayerPrefs.SetFloat("etage1CashGain", 15363687.07f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 16360833.91f);
                    everEtageTheSame();
                    break;

                case 222:
                    kostenEtage = kostenEtageGesamt[221];
                    PlayerPrefs.SetFloat("etage1CashGain", 16360833.91f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 17367316.26f);
                    everEtageTheSame();
                    break;

                case 223:
                    kostenEtage = kostenEtageGesamt[222];
                    PlayerPrefs.SetFloat("etage1CashGain", 17367316.26f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 18551639f);
                    everEtageTheSame();
                    break;

                case 224:
                    kostenEtage = kostenEtageGesamt[223];
                    PlayerPrefs.SetFloat("etage1CashGain", 18551639f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 19863888f);
                    everEtageTheSame();
                    break;

                case 225:
                    kostenEtage = kostenEtageGesamt[224];
                    PlayerPrefs.SetFloat("etage1CashGain", 19863888f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 21123725.32f);
                    everEtageTheSame();
                    break;

                case 226:
                    kostenEtage = kostenEtageGesamt[225];
                    PlayerPrefs.SetFloat("etage1CashGain", 21123725.32f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 22479861.09f);
                    everEtageTheSame();
                    break;

                case 227:
                    kostenEtage = kostenEtageGesamt[226];
                    PlayerPrefs.SetFloat("etage1CashGain", 22479861.09f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 23989520.35f);
                    everEtageTheSame();
                    break;

                case 228:
                    kostenEtage = kostenEtageGesamt[227];
                    PlayerPrefs.SetFloat("etage1CashGain", 23989520.35f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 25651286.34f);
                    everEtageTheSame();
                    break;

                case 229:
                    kostenEtage = kostenEtageGesamt[228];
                    PlayerPrefs.SetFloat("etage1CashGain", 25651286.34f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 27365445.25f);
                    everEtageTheSame();
                    break;

                case 230:
                    kostenEtage = kostenEtageGesamt[229];
                    PlayerPrefs.SetFloat("etage1CashGain", 27365445.25f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 29178622.19f);
                    everEtageTheSame();
                    break;

                case 231:
                    kostenEtage = kostenEtageGesamt[230];
                    PlayerPrefs.SetFloat("etage1CashGain", 29178622.19f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 31009108.16f);
                    everEtageTheSame();
                    break;

                case 232:
                    kostenEtage = kostenEtageGesamt[231];
                    PlayerPrefs.SetFloat("etage1CashGain", 31009108.16f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 33066960.22f);
                    everEtageTheSame();
                    break;

                case 233:
                    kostenEtage = kostenEtageGesamt[232];
                    PlayerPrefs.SetFloat("etage1CashGain", 33066960.22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 35319902.88f);
                    everEtageTheSame();
                    break;

                case 234:
                    kostenEtage = kostenEtageGesamt[233];
                    PlayerPrefs.SetFloat("etage1CashGain", 35319902.88f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 37650178.97f);
                    everEtageTheSame();
                    break;

                case 235:
                    kostenEtage = kostenEtageGesamt[234];
                    PlayerPrefs.SetFloat("etage1CashGain", 37650178.97f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 40149420.12f);
                    everEtageTheSame();
                    break;

                case 236:
                    kostenEtage = kostenEtageGesamt[235];
                    PlayerPrefs.SetFloat("etage1CashGain", 40149420.12f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 42881792.99f);
                    everEtageTheSame();
                    break;

                case 237:
                    kostenEtage = kostenEtageGesamt[236];
                    PlayerPrefs.SetFloat("etage1CashGain", 42881792.99f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 45426049.49f);
                    everEtageTheSame();
                    break;

                case 238:
                    kostenEtage = kostenEtageGesamt[237];
                    PlayerPrefs.SetFloat("etage1CashGain", 45426049.49f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 48432457.55f);
                    everEtageTheSame();
                    break;

                case 239:
                    kostenEtage = kostenEtageGesamt[238];
                    PlayerPrefs.SetFloat("etage1CashGain", 48432457.55f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 51454655.22f);
                    everEtageTheSame();
                    break;

                case 240:
                    kostenEtage = kostenEtageGesamt[239];
                    PlayerPrefs.SetFloat("etage1CashGain", 51454655.22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 55028848.42f);
                    everEtageTheSame();
                    break;

                case 241:
                    kostenEtage = kostenEtageGesamt[240];
                    PlayerPrefs.SetFloat("etage1CashGain", 55028848.42f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 58789264.5f);
                    everEtageTheSame();
                    break;

                case 242:
                    kostenEtage = kostenEtageGesamt[241];
                    PlayerPrefs.SetFloat("etage1CashGain", 58789264.5f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 62383526.41f);
                    everEtageTheSame();
                    break;

                case 243:
                    kostenEtage = kostenEtageGesamt[242];
                    PlayerPrefs.SetFloat("etage1CashGain", 62383526.41f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 66348577.61f);
                    everEtageTheSame();
                    break;

                case 244:
                    kostenEtage = kostenEtageGesamt[243];
                    PlayerPrefs.SetFloat("etage1CashGain", 66348577.61f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 70608169.3f);
                    everEtageTheSame();
                    break;

                case 245:
                    kostenEtage = kostenEtageGesamt[244];
                    PlayerPrefs.SetFloat("etage1CashGain", 70608169.3f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 75354269.34f);
                    everEtageTheSame();
                    break;

                case 246:
                    kostenEtage = kostenEtageGesamt[245];
                    PlayerPrefs.SetFloat("etage1CashGain", 75354269.34f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 80182527.59f);
                    everEtageTheSame();
                    break;

                case 247:
                    kostenEtage = kostenEtageGesamt[246];
                    PlayerPrefs.SetFloat("etage1CashGain", 80182527.59f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 85148553.09f);
                    everEtageTheSame();
                    break;

                case 248:
                    kostenEtage = kostenEtageGesamt[247];
                    PlayerPrefs.SetFloat("etage1CashGain", 85148553.09f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 90703842.75f);
                    everEtageTheSame();
                    break;

                case 249:
                    kostenEtage = kostenEtageGesamt[248];
                    PlayerPrefs.SetFloat("etage1CashGain", 90703842.75f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 97065641.38f);
                    everEtageTheSame();
                    break;

                case 250:
                    kostenEtage = kostenEtageGesamt[249];
                    PlayerPrefs.SetFloat("etage1CashGain", 97065641.38f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 103604209.03f);
                    everEtageTheSame();
                    break;

                case 251:
                    kostenEtage = kostenEtageGesamt[250];
                    PlayerPrefs.SetFloat("etage1CashGain", 103604209.03f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 111025396.54f);
                    everEtageTheSame();
                    break;

                case 252:
                    kostenEtage = kostenEtageGesamt[251];
                    PlayerPrefs.SetFloat("etage1CashGain", 111025396.54f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 117795709.39f);
                    everEtageTheSame();
                    break;

                case 253:
                    kostenEtage = kostenEtageGesamt[252];
                    PlayerPrefs.SetFloat("etage1CashGain", 117795709.39f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 125052323.09f);
                    everEtageTheSame();
                    break;

                case 254:
                    kostenEtage = kostenEtageGesamt[253];
                    PlayerPrefs.SetFloat("etage1CashGain", 125052323.09f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 133161490.75f);
                    everEtageTheSame();
                    break;

                case 255:
                    kostenEtage = kostenEtageGesamt[254];
                    PlayerPrefs.SetFloat("etage1CashGain", 133161490.75f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 141561636.47f);
                    everEtageTheSame();
                    break;

                case 256:
                    kostenEtage = kostenEtageGesamt[255];
                    PlayerPrefs.SetFloat("etage1CashGain", 141561636.47f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 150862992.97f);
                    everEtageTheSame();
                    break;

                case 257:
                    kostenEtage = kostenEtageGesamt[256];
                    PlayerPrefs.SetFloat("etage1CashGain", 150862992.97f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 159699053.7f);
                    everEtageTheSame();
                    break;

                case 258:
                    kostenEtage = kostenEtageGesamt[257];
                    PlayerPrefs.SetFloat("etage1CashGain", 159699053.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 169645708.24f);
                    everEtageTheSame();
                    break;

                case 259:
                    kostenEtage = kostenEtageGesamt[258];
                    PlayerPrefs.SetFloat("etage1CashGain", 169645708.24f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 180914191.36f);
                    everEtageTheSame();
                    break;

                case 260:
                    kostenEtage = kostenEtageGesamt[259];
                    PlayerPrefs.SetFloat("etage1CashGain", 180914191.36f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 192783446.82f);
                    everEtageTheSame();
                    break;

                case 261:
                    kostenEtage = kostenEtageGesamt[260];
                    PlayerPrefs.SetFloat("etage1CashGain", 192783446.82f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 205720468.03f);
                    everEtageTheSame();
                    break;

                case 262:
                    kostenEtage = kostenEtageGesamt[261];
                    PlayerPrefs.SetFloat("etage1CashGain", 205720468.03f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 218723965.45f);
                    everEtageTheSame();
                    break;

                case 263:
                    kostenEtage = kostenEtageGesamt[262];
                    PlayerPrefs.SetFloat("etage1CashGain", 218723965.45f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 232858148.8f);
                    everEtageTheSame();
                    break;

                case 264:
                    kostenEtage = kostenEtageGesamt[263];
                    PlayerPrefs.SetFloat("etage1CashGain", 232858148.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 248235514.12f);
                    everEtageTheSame();
                    break;

                case 265:
                    kostenEtage = kostenEtageGesamt[264];
                    PlayerPrefs.SetFloat("etage1CashGain", 248235514.12f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 264360966.67f);
                    everEtageTheSame();
                    break;

                case 266:
                    kostenEtage = kostenEtageGesamt[265];
                    PlayerPrefs.SetFloat("etage1CashGain", 264360966.67f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 282046179.35f);
                    everEtageTheSame();
                    break;

                case 267:
                    kostenEtage = kostenEtageGesamt[266];
                    PlayerPrefs.SetFloat("etage1CashGain", 282046179.35f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 298707727.97f);
                    everEtageTheSame();
                    break;

                case 268:
                    kostenEtage = kostenEtageGesamt[267];
                    PlayerPrefs.SetFloat("etage1CashGain", 298707727.97f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 319128585.11f);
                    everEtageTheSame();
                    break;

                case 269:
                    kostenEtage = kostenEtageGesamt[268];
                    PlayerPrefs.SetFloat("etage1CashGain", 319128585.11f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 340869682.81f);
                    everEtageTheSame();
                    break;

                case 270:
                    kostenEtage = kostenEtageGesamt[269];
                    PlayerPrefs.SetFloat("etage1CashGain", 340869682.81f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 363324499.64f);
                    everEtageTheSame();
                    break;

                case 271:
                    kostenEtage = kostenEtageGesamt[270];
                    PlayerPrefs.SetFloat("etage1CashGain", 363324499.64f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 387562814.05f);
                    everEtageTheSame();
                    break;

                case 272:
                    kostenEtage = kostenEtageGesamt[271];
                    PlayerPrefs.SetFloat("etage1CashGain", 387562814.05f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 414862708.23f);
                    everEtageTheSame();
                    break;

                case 273:
                    kostenEtage = kostenEtageGesamt[272];
                    PlayerPrefs.SetFloat("etage1CashGain", 414862708.23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 440626893.08f);
                    everEtageTheSame();
                    break;

                case 274:
                    kostenEtage = kostenEtageGesamt[273];
                    PlayerPrefs.SetFloat("etage1CashGain", 440626893.08f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 471031539.76f);
                    everEtageTheSame();
                    break;

                case 275:
                    kostenEtage = kostenEtageGesamt[274];
                    PlayerPrefs.SetFloat("etage1CashGain", 471031539.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 501922788.74f);
                    everEtageTheSame();
                    break;

                case 276:
                    kostenEtage = kostenEtageGesamt[275];
                    PlayerPrefs.SetFloat("etage1CashGain", 501922788.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 534559752.36f);
                    everEtageTheSame();
                    break;

                case 277:
                    kostenEtage = kostenEtageGesamt[276];
                    PlayerPrefs.SetFloat("etage1CashGain", 534559752.36f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 568123944.7f);
                    everEtageTheSame();
                    break;

                case 278:
                    kostenEtage = kostenEtageGesamt[277];
                    PlayerPrefs.SetFloat("etage1CashGain", 568123944.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 603950020.22f);
                    everEtageTheSame();
                    break;

                case 279:
                    kostenEtage = kostenEtageGesamt[278];
                    PlayerPrefs.SetFloat("etage1CashGain", 603950020.22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 642441858.77f);
                    everEtageTheSame();
                    break;

                case 280:
                    kostenEtage = kostenEtageGesamt[279];
                    PlayerPrefs.SetFloat("etage1CashGain", 642441858.77f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 686098620.92f);
                    everEtageTheSame();
                    break;

                case 281:
                    kostenEtage = kostenEtageGesamt[280];
                    PlayerPrefs.SetFloat("etage1CashGain", 686098620.92f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 729161024.1f);
                    everEtageTheSame();
                    break;

                case 282:
                    kostenEtage = kostenEtageGesamt[281];
                    PlayerPrefs.SetFloat("etage1CashGain", 729161024.1f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 773698912.39f);
                    everEtageTheSame();
                    break;

                case 283:
                    kostenEtage = kostenEtageGesamt[282];
                    PlayerPrefs.SetFloat("etage1CashGain", 773698912.39f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 825184702.02f);
                    everEtageTheSame();
                    break;

                case 284:
                    kostenEtage = kostenEtageGesamt[283];
                    PlayerPrefs.SetFloat("etage1CashGain", 825184702.02f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 880610001.86f);
                    everEtageTheSame();
                    break;

                case 285:
                    kostenEtage = kostenEtageGesamt[284];
                    PlayerPrefs.SetFloat("etage1CashGain", 880610001.86f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 937518505.48f);
                    everEtageTheSame();
                    break;

                case 286:
                    kostenEtage = kostenEtageGesamt[285];
                    PlayerPrefs.SetFloat("etage1CashGain", 937518505.48f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 996939157.99f);
                    everEtageTheSame();
                    break;

                case 287:
                    kostenEtage = kostenEtageGesamt[286];
                    PlayerPrefs.SetFloat("etage1CashGain", 996939157.99f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1059914940.57f);
                    everEtageTheSame();
                    break;

                case 288:
                    kostenEtage = kostenEtageGesamt[287];
                    PlayerPrefs.SetFloat("etage1CashGain", 1059914940.57f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1126219456.83f);
                    everEtageTheSame();
                    break;

                case 289:
                    kostenEtage = kostenEtageGesamt[288];
                    PlayerPrefs.SetFloat("etage1CashGain", 1126219456.83f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1204577151.7f);
                    everEtageTheSame();
                    break;

                case 290:
                    kostenEtage = kostenEtageGesamt[289];
                    PlayerPrefs.SetFloat("etage1CashGain", 1204577151.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1284857117.46f);
                    everEtageTheSame();
                    break;

                case 291:
                    kostenEtage = kostenEtageGesamt[290];
                    PlayerPrefs.SetFloat("etage1CashGain", 1284857117.46f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1372935823.19f);
                    everEtageTheSame();
                    break;

                case 292:
                    kostenEtage = kostenEtageGesamt[291];
                    PlayerPrefs.SetFloat("etage1CashGain", 1372935823.19f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1460537978.1f);
                    everEtageTheSame();
                    break;

                case 293:
                    kostenEtage = kostenEtageGesamt[292];
                    PlayerPrefs.SetFloat("etage1CashGain", 1460537978.1f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1550722665.25f);
                    everEtageTheSame();
                    break;

                case 294:
                    kostenEtage = kostenEtageGesamt[293];
                    PlayerPrefs.SetFloat("etage1CashGain", 1550722665.25f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1655041045.41f);
                    everEtageTheSame();
                    break;

                case 295:
                    kostenEtage = kostenEtageGesamt[294];
                    PlayerPrefs.SetFloat("etage1CashGain", 1655041045.41f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1760539416.17f);
                    everEtageTheSame();
                    break;

                case 296:
                    kostenEtage = kostenEtageGesamt[295];
                    PlayerPrefs.SetFloat("etage1CashGain", 1760539416.17f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1875939835.69f);
                    everEtageTheSame();
                    break;

                case 297:
                    kostenEtage = kostenEtageGesamt[296];
                    PlayerPrefs.SetFloat("etage1CashGain", 1875939835.69f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1994567540.73f);
                    everEtageTheSame();
                    break;

                case 298:
                    kostenEtage = kostenEtageGesamt[297];
                    PlayerPrefs.SetFloat("etage1CashGain", 1994567540.73f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2987635872.38f);
                    everEtageTheSame();
                    break;

                case 299:
                    kostenEtage = kostenEtageGesamt[298];
                    PlayerPrefs.SetFloat("etage1CashGain", 2987635872.38f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3171529088.95f);
                    everEtageTheSame();
                    break;

                case 300:
                    kostenEtage = kostenEtageGesamt[299];
                    PlayerPrefs.SetFloat("etage1CashGain", 3171529088.95f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3397818672.58f);
                    everEtageTheSame();
                    break;

                case 301:
                    kostenEtage = kostenEtageGesamt[300];
                    PlayerPrefs.SetFloat("etage1CashGain", 3397818672.58f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3606305209.07f);
                    everEtageTheSame();
                    break;

                case 302:
                    kostenEtage = kostenEtageGesamt[301];
                    PlayerPrefs.SetFloat("etage1CashGain", 3606305209.07f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3824997349.45f);
                    everEtageTheSame();
                    break;

                case 303:
                    kostenEtage = kostenEtageGesamt[302];
                    PlayerPrefs.SetFloat("etage1CashGain", 3824997349.45f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4077723758.4f);
                    everEtageTheSame();
                    break;

                case 304:
                    kostenEtage = kostenEtageGesamt[303];
                    PlayerPrefs.SetFloat("etage1CashGain", 4077723758.4f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4339058687.71f);
                    everEtageTheSame();
                    break;

                case 305:
                    kostenEtage = kostenEtageGesamt[304];
                    PlayerPrefs.SetFloat("etage1CashGain", 4339058687.71f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4628222507.69f);
                    everEtageTheSame();
                    break;

                case 306:
                    kostenEtage = kostenEtageGesamt[305];
                    PlayerPrefs.SetFloat("etage1CashGain", 4628222507.69f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4928354234.41f);
                    everEtageTheSame();
                    break;

                case 307:
                    kostenEtage = kostenEtageGesamt[306];
                    PlayerPrefs.SetFloat("etage1CashGain", 4928354234.41f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5269837583.25f);
                    everEtageTheSame();
                    break;

                case 308:
                    kostenEtage = kostenEtageGesamt[307];
                    PlayerPrefs.SetFloat("etage1CashGain", 5269837583.25f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5605506447.87f);
                    everEtageTheSame();
                    break;

                case 309:
                    kostenEtage = kostenEtageGesamt[308];
                    PlayerPrefs.SetFloat("etage1CashGain", 5605506447.87f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5963929083.62f);
                    everEtageTheSame();
                    break;

                case 310:
                    kostenEtage = kostenEtageGesamt[309];
                    PlayerPrefs.SetFloat("etage1CashGain", 5963929083.62f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6345374826.46f);
                    everEtageTheSame();
                    break;

                case 311:
                    kostenEtage = kostenEtageGesamt[310];
                    PlayerPrefs.SetFloat("etage1CashGain", 6345374826.46f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6766266201.81f);
                    everEtageTheSame();
                    break;

                case 312:
                    kostenEtage = kostenEtageGesamt[311];
                    PlayerPrefs.SetFloat("etage1CashGain", 6766266201.81f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7190344249.74f);
                    everEtageTheSame();
                    break;

                case 313:
                    kostenEtage = kostenEtageGesamt[312];
                    PlayerPrefs.SetFloat("etage1CashGain", 7190344249.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7651257878.33f);
                    everEtageTheSame();
                    break;

                case 314:
                    kostenEtage = kostenEtageGesamt[313];
                    PlayerPrefs.SetFloat("etage1CashGain", 7651257878.33f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8161469286.91f);
                    everEtageTheSame();
                    break;

                case 315:
                    kostenEtage = kostenEtageGesamt[314];
                    PlayerPrefs.SetFloat("etage1CashGain", 8161469286.91f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8656598146.67f);
                    everEtageTheSame();
                    break;

                case 316:
                    kostenEtage = kostenEtageGesamt[315];
                    PlayerPrefs.SetFloat("etage1CashGain", 8656598146.67f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9211308968f);
                    everEtageTheSame();
                    break;

                case 317:
                    kostenEtage = kostenEtageGesamt[316];
                    PlayerPrefs.SetFloat("etage1CashGain", 9211308968f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9784306701.84f);
                    everEtageTheSame();
                    break;

                case 318:
                    kostenEtage = kostenEtageGesamt[317];
                    PlayerPrefs.SetFloat("etage1CashGain", 9784306701.84f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10426923259.02f);
                    everEtageTheSame();
                    break;

                case 319:
                    kostenEtage = kostenEtageGesamt[318];
                    PlayerPrefs.SetFloat("etage1CashGain", 10426923259.02f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11119943714.82f);
                    everEtageTheSame();
                    break;

                case 320:
                    kostenEtage = kostenEtageGesamt[319];
                    PlayerPrefs.SetFloat("etage1CashGain", 11119943714.82f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11858989665.95f);
                    everEtageTheSame();
                    break;

                case 321:
                    kostenEtage = kostenEtageGesamt[320];
                    PlayerPrefs.SetFloat("etage1CashGain", 11858989665.95f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12677069704.46f);
                    everEtageTheSame();
                    break;

                case 322:
                    kostenEtage = kostenEtageGesamt[321];
                    PlayerPrefs.SetFloat("etage1CashGain", 12677069704.46f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13501093238.06f);
                    everEtageTheSame();
                    break;

                case 323:
                    kostenEtage = kostenEtageGesamt[322];
                    PlayerPrefs.SetFloat("etage1CashGain", 13501093238.06f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 14354042056.21f);
                    everEtageTheSame();
                    break;

                case 324:
                    kostenEtage = kostenEtageGesamt[323];
                    PlayerPrefs.SetFloat("etage1CashGain", 14354042056.21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 15264560715.19f);
                    everEtageTheSame();
                    break;

                case 325:
                    kostenEtage = kostenEtageGesamt[324];
                    PlayerPrefs.SetFloat("etage1CashGain", 15264560715.19f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 16246469278.47f);
                    everEtageTheSame();
                    break;

                case 326:
                    kostenEtage = kostenEtageGesamt[325];
                    PlayerPrefs.SetFloat("etage1CashGain", 16246469278.47f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 17326995310.23f);
                    everEtageTheSame();
                    break;

                case 327:
                    kostenEtage = kostenEtageGesamt[326];
                    PlayerPrefs.SetFloat("etage1CashGain", 17326995310.23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 18462376724.59f);
                    everEtageTheSame();
                    break;

                case 328:
                    kostenEtage = kostenEtageGesamt[327];
                    PlayerPrefs.SetFloat("etage1CashGain", 18462376724.59f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 19620114952.3f);
                    everEtageTheSame();
                    break;

                case 329:
                    kostenEtage = kostenEtageGesamt[328];
                    PlayerPrefs.SetFloat("etage1CashGain", 19620114952.3f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 20814504590.55f);
                    everEtageTheSame();
                    break;

                case 330:
                    kostenEtage = kostenEtageGesamt[329];
                    PlayerPrefs.SetFloat("etage1CashGain", 20814504590.55f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 22147915553.3f);
                    everEtageTheSame();
                    break;

                case 331:
                    kostenEtage = kostenEtageGesamt[330];
                    PlayerPrefs.SetFloat("etage1CashGain", 22147915553.3f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 23753173509.28f);
                    everEtageTheSame();
                    break;

                case 332:
                    kostenEtage = kostenEtageGesamt[331];
                    PlayerPrefs.SetFloat("etage1CashGain", 23753173509.28f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 25429273594.74f);
                    everEtageTheSame();
                    break;

                case 333:
                    kostenEtage = kostenEtageGesamt[332];
                    PlayerPrefs.SetFloat("etage1CashGain", 25429273594.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 26944244601.74f);
                    everEtageTheSame();
                    break;

                case 334:
                    kostenEtage = kostenEtageGesamt[333];
                    PlayerPrefs.SetFloat("etage1CashGain", 26944244601.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 28741518159.86f);
                    everEtageTheSame();
                    break;

                case 335:
                    kostenEtage = kostenEtageGesamt[334];
                    PlayerPrefs.SetFloat("etage1CashGain", 28741518159.86f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 30477891591.53f);
                    everEtageTheSame();
                    break;

                case 336:
                    kostenEtage = kostenEtageGesamt[335];
                    PlayerPrefs.SetFloat("etage1CashGain", 30477891591.53f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 32510953397.02f);
                    everEtageTheSame();
                    break;

                case 337:
                    kostenEtage = kostenEtageGesamt[336];
                    PlayerPrefs.SetFloat("etage1CashGain", 32510953397.02f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 34631094900.11f);
                    everEtageTheSame();
                    break;

                case 338:
                    kostenEtage = kostenEtageGesamt[337];
                    PlayerPrefs.SetFloat("etage1CashGain", 34631094900.11f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 36820351428.08f);
                    everEtageTheSame();
                    break;

                case 339:
                    kostenEtage = kostenEtageGesamt[338];
                    PlayerPrefs.SetFloat("etage1CashGain", 36820351428.08f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 39383795561.54f);
                    everEtageTheSame();
                    break;

                case 340:
                    kostenEtage = kostenEtageGesamt[339];
                    PlayerPrefs.SetFloat("etage1CashGain", 39383795561.54f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 41773330469.37f);
                    everEtageTheSame();
                    break;

                case 341:
                    kostenEtage = kostenEtageGesamt[340];
                    PlayerPrefs.SetFloat("etage1CashGain", 41773330469.37f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 44283302783.79f);
                    everEtageTheSame();
                    break;

                case 342:
                    kostenEtage = kostenEtageGesamt[341];
                    PlayerPrefs.SetFloat("etage1CashGain", 44283302783.79f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 47095082618.47f);
                    everEtageTheSame();
                    break;

                case 343:
                    kostenEtage = kostenEtageGesamt[342];
                    PlayerPrefs.SetFloat("etage1CashGain", 47095082618.47f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 50154094200.23f);
                    everEtageTheSame();
                    break;

                case 344:
                    kostenEtage = kostenEtageGesamt[343];
                    PlayerPrefs.SetFloat("etage1CashGain", 50154094200.23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 53485385310.95f);
                    everEtageTheSame();
                    break;

                case 345:
                    kostenEtage = kostenEtageGesamt[344];
                    PlayerPrefs.SetFloat("etage1CashGain", 53485385310.95f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 57120834378.61f);
                    everEtageTheSame();
                    break;

                case 346:
                    kostenEtage = kostenEtageGesamt[345];
                    PlayerPrefs.SetFloat("etage1CashGain", 57120834378.61f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 60427570266.33f);
                    everEtageTheSame();
                    break;

                case 347:
                    kostenEtage = kostenEtageGesamt[346];
                    PlayerPrefs.SetFloat("etage1CashGain", 60427570266.33f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 64243962658.28f);
                    everEtageTheSame();
                    break;

                case 348:
                    kostenEtage = kostenEtageGesamt[347];
                    PlayerPrefs.SetFloat("etage1CashGain", 64243962658.28f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 68017937199.13f);
                    everEtageTheSame();
                    break;

                case 349:
                    kostenEtage = kostenEtageGesamt[348];
                    PlayerPrefs.SetFloat("etage1CashGain", 68017937199.13f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 72208115020.29f);
                    everEtageTheSame();
                    break;

                case 350:
                    kostenEtage = kostenEtageGesamt[349];
                    PlayerPrefs.SetFloat("etage1CashGain", 72208115020.29f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 76688972297.76f);
                    everEtageTheSame();
                    break;

                case 351:
                    kostenEtage = kostenEtageGesamt[350];
                    PlayerPrefs.SetFloat("etage1CashGain", 76688972297.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 81357698932.31f);
                    everEtageTheSame();
                    break;

                case 352:
                    kostenEtage = kostenEtageGesamt[351];
                    PlayerPrefs.SetFloat("etage1CashGain", 81357698932.31f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 86558963538.93f);
                    everEtageTheSame();
                    break;

                case 353:
                    kostenEtage = kostenEtageGesamt[352];
                    PlayerPrefs.SetFloat("etage1CashGain", 86558963538.93f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 92244181466.26f);
                    everEtageTheSame();
                    break;

                case 354:
                    kostenEtage = kostenEtageGesamt[353];
                    PlayerPrefs.SetFloat("etage1CashGain", 92244181466.26f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 98461450878.66f);
                    everEtageTheSame();
                    break;

                case 355:
                    kostenEtage = kostenEtageGesamt[354];
                    PlayerPrefs.SetFloat("etage1CashGain", 98461450878.66f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 104937320808.74f);
                    everEtageTheSame();
                    break;

                case 356:
                    kostenEtage = kostenEtageGesamt[355];
                    PlayerPrefs.SetFloat("etage1CashGain", 104937320808.74f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 111404789918.18f);
                    everEtageTheSame();
                    break;

                case 357:
                    kostenEtage = kostenEtageGesamt[356];
                    PlayerPrefs.SetFloat("etage1CashGain", 111404789918.18f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 118848918158f);
                    everEtageTheSame();
                    break;

                case 358:
                    kostenEtage = kostenEtageGesamt[357];
                    PlayerPrefs.SetFloat("etage1CashGain", 118848918158f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 126854259563.82f);
                    everEtageTheSame();
                    break;

                case 359:
                    kostenEtage = kostenEtageGesamt[358];
                    PlayerPrefs.SetFloat("etage1CashGain", 126854259563.82f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 134723027222.29f);
                    everEtageTheSame();
                    break;

                case 360:
                    kostenEtage = kostenEtageGesamt[359];
                    PlayerPrefs.SetFloat("etage1CashGain", 134723027222.29f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 142800051830.86f);
                    everEtageTheSame();
                    break;

                case 361:
                    kostenEtage = kostenEtageGesamt[360];
                    PlayerPrefs.SetFloat("etage1CashGain", 142800051830.86f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 151762876697.4f);
                    everEtageTheSame();
                    break;

                case 362:
                    kostenEtage = kostenEtageGesamt[361];
                    PlayerPrefs.SetFloat("etage1CashGain", 151762876697.4f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 161674068677.76f);
                    everEtageTheSame();
                    break;

                case 363:
                    kostenEtage = kostenEtageGesamt[362];
                    PlayerPrefs.SetFloat("etage1CashGain", 161674068677.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 171857826035.56f);
                    everEtageTheSame();
                    break;

                case 364:
                    kostenEtage = kostenEtageGesamt[363];
                    PlayerPrefs.SetFloat("etage1CashGain", 171857826035.56f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 182492262133.77f);
                    everEtageTheSame();
                    break;

                case 365:
                    kostenEtage = kostenEtageGesamt[364];
                    PlayerPrefs.SetFloat("etage1CashGain", 182492262133.77f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 193081251485.14f);
                    everEtageTheSame();
                    break;

                case 366:
                    kostenEtage = kostenEtageGesamt[365];
                    PlayerPrefs.SetFloat("etage1CashGain", 193081251485.14f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 206472377877.23f);
                    everEtageTheSame();
                    break;

                case 367:
                    kostenEtage = kostenEtageGesamt[366];
                    PlayerPrefs.SetFloat("etage1CashGain", 206472377877.23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 219465381290.19f);
                    everEtageTheSame();
                    break;

                case 368:
                    kostenEtage = kostenEtageGesamt[367];
                    PlayerPrefs.SetFloat("etage1CashGain", 219465381290.19f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 234023839656.83f);
                    everEtageTheSame();
                    break;

                case 369:
                    kostenEtage = kostenEtageGesamt[368];
                    PlayerPrefs.SetFloat("etage1CashGain", 234023839656.83f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 248950259977.18f);
                    everEtageTheSame();
                    break;

                case 370:
                    kostenEtage = kostenEtageGesamt[369];
                    PlayerPrefs.SetFloat("etage1CashGain", 248950259977.18f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 264942473218.21f);
                    everEtageTheSame();
                    break;

                case 371:
                    kostenEtage = kostenEtageGesamt[370];
                    PlayerPrefs.SetFloat("etage1CashGain", 264942473218.21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 281686236256.1f);
                    everEtageTheSame();
                    break;

                case 372:
                    kostenEtage = kostenEtageGesamt[371];
                    PlayerPrefs.SetFloat("etage1CashGain", 281686236256.1f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 301019620794.38f);
                    everEtageTheSame();
                    break;

                case 373:
                    kostenEtage = kostenEtageGesamt[372];
                    PlayerPrefs.SetFloat("etage1CashGain", 301019620794.38f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 320045169326.35f);
                    everEtageTheSame();
                    break;

                case 374:
                    kostenEtage = kostenEtageGesamt[373];
                    PlayerPrefs.SetFloat("etage1CashGain", 320045169326.35f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 341466925965.88f);
                    everEtageTheSame();
                    break;

                case 375:
                    kostenEtage = kostenEtageGesamt[374];
                    PlayerPrefs.SetFloat("etage1CashGain", 341466925965.88f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 362231821141.29f);
                    everEtageTheSame();
                    break;

                case 376:
                    kostenEtage = kostenEtageGesamt[375];
                    PlayerPrefs.SetFloat("etage1CashGain", 362231821141.29f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 385505740851.34f);
                    everEtageTheSame();
                    break;

                case 377:
                    kostenEtage = kostenEtageGesamt[376];
                    PlayerPrefs.SetFloat("etage1CashGain", 385505740851.34f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 410873259505.75f);
                    everEtageTheSame();
                    break;

                case 378:
                    kostenEtage = kostenEtageGesamt[377];
                    PlayerPrefs.SetFloat("etage1CashGain", 410873259505.75f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 435651421711.76f);
                    everEtageTheSame();
                    break;

                case 379:
                    kostenEtage = kostenEtageGesamt[378];
                    PlayerPrefs.SetFloat("etage1CashGain", 435651421711.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 463822449181.91f);
                    everEtageTheSame();
                    break;

                case 380:
                    kostenEtage = kostenEtageGesamt[379];
                    PlayerPrefs.SetFloat("etage1CashGain", 463822449181.91f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 493558508157.75f);
                    everEtageTheSame();
                    break;

                case 381:
                    kostenEtage = kostenEtageGesamt[380];
                    PlayerPrefs.SetFloat("etage1CashGain", 493558508157.75f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 525552649920.03f);
                    everEtageTheSame();
                    break;

                case 382:
                    kostenEtage = kostenEtageGesamt[381];
                    PlayerPrefs.SetFloat("etage1CashGain", 525552649920.03f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 558827431233.38f);
                    everEtageTheSame();
                    break;

                case 383:
                    kostenEtage = kostenEtageGesamt[382];
                    PlayerPrefs.SetFloat("etage1CashGain", 558827431233.38f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 593054956016.34f);
                    everEtageTheSame();
                    break;

                case 384:
                    kostenEtage = kostenEtageGesamt[383];
                    PlayerPrefs.SetFloat("etage1CashGain", 593054956016.34f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 634223417542.07f);
                    everEtageTheSame();
                    break;

                case 385:
                    kostenEtage = kostenEtageGesamt[384];
                    PlayerPrefs.SetFloat("etage1CashGain", 634223417542.07f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 674206123321.05f);
                    everEtageTheSame();
                    break;

                case 386:
                    kostenEtage = kostenEtageGesamt[385];
                    PlayerPrefs.SetFloat("etage1CashGain", 674206123321.05f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 717755905783.11f);
                    everEtageTheSame();
                    break;

                case 387:
                    kostenEtage = kostenEtageGesamt[386];
                    PlayerPrefs.SetFloat("etage1CashGain", 717755905783.11f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 767768947384.72f);
                    everEtageTheSame();
                    break;

                case 388:
                    kostenEtage = kostenEtageGesamt[387];
                    PlayerPrefs.SetFloat("etage1CashGain", 767768947384.72f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 817946299399.31f);
                    everEtageTheSame();
                    break;

                case 389:
                    kostenEtage = kostenEtageGesamt[388];
                    PlayerPrefs.SetFloat("etage1CashGain", 817946299399.31f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 868368089066.04f);
                    everEtageTheSame();
                    break;

                case 390:
                    kostenEtage = kostenEtageGesamt[389];
                    PlayerPrefs.SetFloat("etage1CashGain", 868368089066.04f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 927031959668.79f);
                    everEtageTheSame();
                    break;

                case 391:
                    kostenEtage = kostenEtageGesamt[390];
                    PlayerPrefs.SetFloat("etage1CashGain", 927031959668.79f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 988574514452.54f);
                    everEtageTheSame();
                    break;

                case 392:
                    kostenEtage = kostenEtageGesamt[391];
                    PlayerPrefs.SetFloat("etage1CashGain", 988574514452.54f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1057580946927.3f);
                    everEtageTheSame();
                    break;

                case 393:
                    kostenEtage = kostenEtageGesamt[392];
                    PlayerPrefs.SetFloat("etage1CashGain", 1057580946927.3f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1121000586317.28f);
                    everEtageTheSame();
                    break;

                case 394:
                    kostenEtage = kostenEtageGesamt[393];
                    PlayerPrefs.SetFloat("etage1CashGain", 1121000586317.28f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1188970901202.76f);
                    everEtageTheSame();
                    break;

                case 395:
                    kostenEtage = kostenEtageGesamt[394];
                    PlayerPrefs.SetFloat("etage1CashGain", 1188970901202.76f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1262331799505.71f);
                    everEtageTheSame();
                    break;

                case 396:
                    kostenEtage = kostenEtageGesamt[395];
                    PlayerPrefs.SetFloat("etage1CashGain", 1262331799505.71f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1356776158253.41f);
                    everEtageTheSame();
                    break;

                case 397:
                    kostenEtage = kostenEtageGesamt[396];
                    PlayerPrefs.SetFloat("etage1CashGain", 1356776158253.41f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1433500038791.8f);
                    everEtageTheSame();
                    break;

                case 398:
                    kostenEtage = kostenEtageGesamt[397];
                    PlayerPrefs.SetFloat("etage1CashGain", 1433500038791.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2844995851988.15f);
                    everEtageTheSame();
                    break;

                case 399:
                    kostenEtage = kostenEtageGesamt[398];
                    PlayerPrefs.SetFloat("etage1CashGain", 2844995851988.15f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3028991624470.05f);
                    everEtageTheSame();
                    break;

                case 400:
                    kostenEtage = kostenEtageGesamt[399];
                    PlayerPrefs.SetFloat("etage1CashGain", 3028991624470.05f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3219302787401.67f);
                    everEtageTheSame();
                    break;

                case 401:
                    kostenEtage = kostenEtageGesamt[400];
                    PlayerPrefs.SetFloat("etage1CashGain", 3219302787401.67f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3436023272932.14f);
                    everEtageTheSame();
                    break;

                case 402:
                    kostenEtage = kostenEtageGesamt[401];
                    PlayerPrefs.SetFloat("etage1CashGain", 3436023272932.14f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3663485068247.82f);
                    everEtageTheSame();
                    break;

                case 403:
                    kostenEtage = kostenEtageGesamt[402];
                    PlayerPrefs.SetFloat("etage1CashGain", 3663485068247.82f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3899910242934.65f);
                    everEtageTheSame();
                    break;

                case 404:
                    kostenEtage = kostenEtageGesamt[403];
                    PlayerPrefs.SetFloat("etage1CashGain", 3899910242934.65f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4165594783798.95f);
                    everEtageTheSame();
                    break;

                case 405:
                    kostenEtage = kostenEtageGesamt[404];
                    PlayerPrefs.SetFloat("etage1CashGain", 4165594783798.95f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4429237987738.06f);
                    everEtageTheSame();
                    break;

                case 406:
                    kostenEtage = kostenEtageGesamt[405];
                    PlayerPrefs.SetFloat("etage1CashGain", 4429237987738.06f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4694431006830.68f);
                    everEtageTheSame();
                    break;

                case 407:
                    kostenEtage = kostenEtageGesamt[406];
                    PlayerPrefs.SetFloat("etage1CashGain", 4694431006830.68f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4988156166903.17f);
                    everEtageTheSame();
                    break;

                case 408:
                    kostenEtage = kostenEtageGesamt[407];
                    PlayerPrefs.SetFloat("etage1CashGain", 4988156166903.17f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5322189450082.63f);
                    everEtageTheSame();
                    break;

                case 409:
                    kostenEtage = kostenEtageGesamt[408];
                    PlayerPrefs.SetFloat("etage1CashGain", 5322189450082.63f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5681813590790.53f);
                    everEtageTheSame();
                    break;

                case 410:
                    kostenEtage = kostenEtageGesamt[409];
                    PlayerPrefs.SetFloat("etage1CashGain", 5681813590790.53f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6072773478457.52f);
                    everEtageTheSame();
                    break;

                case 411:
                    kostenEtage = kostenEtageGesamt[410];
                    PlayerPrefs.SetFloat("etage1CashGain", 6072773478457.52f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6465309162670.1f);
                    everEtageTheSame();
                    break;

                case 412:
                    kostenEtage = kostenEtageGesamt[411];
                    PlayerPrefs.SetFloat("etage1CashGain", 6465309162670.1f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6883960087395.65f);
                    everEtageTheSame();
                    break;

                case 413:
                    kostenEtage = kostenEtageGesamt[412];
                    PlayerPrefs.SetFloat("etage1CashGain", 6883960087395.65f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7318888080832.01f);
                    everEtageTheSame();
                    break;

                case 414:
                    kostenEtage = kostenEtageGesamt[413];
                    PlayerPrefs.SetFloat("etage1CashGain", 7318888080832.01f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7792665058555.08f);
                    everEtageTheSame();
                    break;

                case 415:
                    kostenEtage = kostenEtageGesamt[414];
                    PlayerPrefs.SetFloat("etage1CashGain", 7792665058555.08f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8254490449580.79f);
                    everEtageTheSame();
                    break;

                case 416:
                    kostenEtage = kostenEtageGesamt[415];
                    PlayerPrefs.SetFloat("etage1CashGain", 8254490449580.79f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8736639657542.21f);
                    everEtageTheSame();
                    break;

                case 417:
                    kostenEtage = kostenEtageGesamt[416];
                    PlayerPrefs.SetFloat("etage1CashGain", 8736639657542.21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9268508619923.12f);
                    everEtageTheSame();
                    break;

                case 418:
                    kostenEtage = kostenEtageGesamt[417];
                    PlayerPrefs.SetFloat("etage1CashGain", 9268508619923.12f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9880599553295.39f);
                    everEtageTheSame();
                    break;

                case 419:
                    kostenEtage = kostenEtageGesamt[418];
                    PlayerPrefs.SetFloat("etage1CashGain", 9880599553295.39f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10480166464136.2f);
                    everEtageTheSame();
                    break;

                case 420:
                    kostenEtage = kostenEtageGesamt[419];
                    PlayerPrefs.SetFloat("etage1CashGain", 10480166464136.2f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11154911361062f);
                    everEtageTheSame();
                    break;

                case 421:
                    kostenEtage = kostenEtageGesamt[420];
                    PlayerPrefs.SetFloat("etage1CashGain", 11154911361062f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11873277383622.7f);
                    everEtageTheSame();
                    break;

                case 422:
                    kostenEtage = kostenEtageGesamt[421];
                    PlayerPrefs.SetFloat("etage1CashGain", 11873277383622.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12662679549773.8f);
                    everEtageTheSame();
                    break;

                case 423:
                    kostenEtage = kostenEtageGesamt[422];
                    PlayerPrefs.SetFloat("etage1CashGain", 12662679549773.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13492395670580.8f);
                    everEtageTheSame();
                    break;

                case 424:
                    kostenEtage = kostenEtageGesamt[423];
                    PlayerPrefs.SetFloat("etage1CashGain", 13492395670580.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 14370905591863.1f);
                    everEtageTheSame();
                    break;

                case 425:
                    kostenEtage = kostenEtageGesamt[424];
                    PlayerPrefs.SetFloat("etage1CashGain", 14370905591863.1f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 15331133523875.9f);
                    everEtageTheSame();
                    break;

                case 426:
                    kostenEtage = kostenEtageGesamt[425];
                    PlayerPrefs.SetFloat("etage1CashGain", 15331133523875.9f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 16333416798308.2f);
                    everEtageTheSame();
                    break;

                case 427:
                    kostenEtage = kostenEtageGesamt[426];
                    PlayerPrefs.SetFloat("etage1CashGain", 16333416798308.2f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 17440190297737.8f);
                    everEtageTheSame();
                    break;

                case 428:
                    kostenEtage = kostenEtageGesamt[427];
                    PlayerPrefs.SetFloat("etage1CashGain", 17440190297737.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 18596984551634.7f);
                    everEtageTheSame();
                    break;

                case 429:
                    kostenEtage = kostenEtageGesamt[428];
                    PlayerPrefs.SetFloat("etage1CashGain", 18596984551634.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 19720967044782.3f);
                    everEtageTheSame();
                    break;

                case 430:
                    kostenEtage = kostenEtageGesamt[429];
                    PlayerPrefs.SetFloat("etage1CashGain", 19720967044782.3f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 20919863058571f);
                    everEtageTheSame();
                    break;

                case 431:
                    kostenEtage = kostenEtageGesamt[430];
                    PlayerPrefs.SetFloat("etage1CashGain", 20919863058571f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 22274926180950.4f);
                    everEtageTheSame();
                    break;

                case 432:
                    kostenEtage = kostenEtageGesamt[431];
                    PlayerPrefs.SetFloat("etage1CashGain", 22274926180950.4f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 23748415711207.5f);
                    everEtageTheSame();
                    break;

                case 433:
                    kostenEtage = kostenEtageGesamt[432];
                    PlayerPrefs.SetFloat("etage1CashGain", 23748415711207.5f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 25241793331393f);
                    everEtageTheSame();
                    break;

                case 434:
                    kostenEtage = kostenEtageGesamt[433];
                    PlayerPrefs.SetFloat("etage1CashGain", 25241793331393f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 26938567005039.5f);
                    everEtageTheSame();
                    break;

                case 435:
                    kostenEtage = kostenEtageGesamt[434];
                    PlayerPrefs.SetFloat("etage1CashGain", 26938567005039.5f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 28634837053399.6f);
                    everEtageTheSame();
                    break;

                case 436:
                    kostenEtage = kostenEtageGesamt[435];
                    PlayerPrefs.SetFloat("etage1CashGain", 28634837053399.6f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 30465459241162.4f);
                    everEtageTheSame();
                    break;

                case 437:
                    kostenEtage = kostenEtageGesamt[436];
                    PlayerPrefs.SetFloat("etage1CashGain", 30465459241162.4f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 32482711958021.6f);
                    everEtageTheSame();
                    break;

                case 438:
                    kostenEtage = kostenEtageGesamt[437];
                    PlayerPrefs.SetFloat("etage1CashGain", 32482711958021.6f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 34558584862118.7f);
                    everEtageTheSame();
                    break;

                case 439:
                    kostenEtage = kostenEtageGesamt[438];
                    PlayerPrefs.SetFloat("etage1CashGain", 34558584862118.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 36868129657700.7f);
                    everEtageTheSame();
                    break;

                case 440:
                    kostenEtage = kostenEtageGesamt[439];
                    PlayerPrefs.SetFloat("etage1CashGain", 36868129657700.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 39329107244276.1f);
                    everEtageTheSame();
                    break;

                case 441:
                    kostenEtage = kostenEtageGesamt[440];
                    PlayerPrefs.SetFloat("etage1CashGain", 39329107244276.1f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 41723961731450.5f);
                    everEtageTheSame();
                    break;

                case 442:
                    kostenEtage = kostenEtageGesamt[441];
                    PlayerPrefs.SetFloat("etage1CashGain", 41723961731450.5f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 44469119265484.7f);
                    everEtageTheSame();
                    break;

                case 443:
                    kostenEtage = kostenEtageGesamt[442];
                    PlayerPrefs.SetFloat("etage1CashGain", 44469119265484.7f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 47627781977326.8f);
                    everEtageTheSame();
                    break;

                case 444:
                    kostenEtage = kostenEtageGesamt[443];
                    PlayerPrefs.SetFloat("etage1CashGain", 47627781977326.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 50789792645008.4f);
                    everEtageTheSame();
                    break;

                case 445:
                    kostenEtage = kostenEtageGesamt[444];
                    PlayerPrefs.SetFloat("etage1CashGain", 50789792645008.4f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 54078075949739f);
                    everEtageTheSame();
                    break;

                case 446:
                    kostenEtage = kostenEtageGesamt[445];
                    PlayerPrefs.SetFloat("etage1CashGain", 54078075949739f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 57330494324573.8f);
                    everEtageTheSame();
                    break;

                case 447:
                    kostenEtage = kostenEtageGesamt[446];
                    PlayerPrefs.SetFloat("etage1CashGain", 57330494324573.8f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 61261926842271.9f);
                    everEtageTheSame();
                    break;

                case 448:
                    kostenEtage = kostenEtageGesamt[447];
                    PlayerPrefs.SetFloat("etage1CashGain", 61261926842271.9f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 65503285808774.6f);
                    everEtageTheSame();
                    break;

                case 449:
                    kostenEtage = kostenEtageGesamt[448];
                    PlayerPrefs.SetFloat("etage1CashGain", 65503285808774.6f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 69641752238378.5f);
                    everEtageTheSame();
                    break;

                case 450:
                    kostenEtage = kostenEtageGesamt[449];
                    PlayerPrefs.SetFloat("etage1CashGain", 69641752238378.5f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 73969832800444f);
                    everEtageTheSame();
                    break;

                case 451:
                    kostenEtage = kostenEtageGesamt[450];
                    PlayerPrefs.SetFloat("etage1CashGain", 73969832800444f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 78878748849214.1f);
                    everEtageTheSame();
                    break;

                case 452:
                    kostenEtage = kostenEtageGesamt[451];
                    PlayerPrefs.SetFloat("etage1CashGain", 78878748849214.1f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 83820499887798.3f);
                    everEtageTheSame();
                    break;

                case 453:
                    kostenEtage = kostenEtageGesamt[452];
                    PlayerPrefs.SetFloat("etage1CashGain", 83820499887798.3f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 89198322738263f);
                    everEtageTheSame();
                    break;

                case 454:
                    kostenEtage = kostenEtageGesamt[453];
                    PlayerPrefs.SetFloat("etage1CashGain", 89198322738263f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 95197040347902.2f);
                    everEtageTheSame();
                    break;

                case 455:
                    kostenEtage = kostenEtageGesamt[454];
                    PlayerPrefs.SetFloat("etage1CashGain", 95197040347902.2f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 100935479430983f);
                    everEtageTheSame();
                    break;

                case 456:
                    kostenEtage = kostenEtageGesamt[455];
                    PlayerPrefs.SetFloat("etage1CashGain", 100935479430983f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 107796476535036f);
                    everEtageTheSame();
                    break;

                case 457:
                    kostenEtage = kostenEtageGesamt[456];
                    PlayerPrefs.SetFloat("etage1CashGain", 107796476535036f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 115387480835255f);
                    everEtageTheSame();
                    break;

                case 458:
                    kostenEtage = kostenEtageGesamt[457];
                    PlayerPrefs.SetFloat("etage1CashGain", 115387480835255f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 122813175477963f);
                    everEtageTheSame();
                    break;

                case 459:
                    kostenEtage = kostenEtageGesamt[458];
                    PlayerPrefs.SetFloat("etage1CashGain", 122813175477963f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 130906813613684f);
                    everEtageTheSame();
                    break;

                case 460:
                    kostenEtage = kostenEtageGesamt[459];
                    PlayerPrefs.SetFloat("etage1CashGain", 130906813613684f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 138721185974214f);
                    everEtageTheSame();
                    break;

                case 461:
                    kostenEtage = kostenEtageGesamt[460];
                    PlayerPrefs.SetFloat("etage1CashGain", 138721185974214f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 147539846145878f);
                    everEtageTheSame();
                    break;

                case 462:
                    kostenEtage = kostenEtageGesamt[461];
                    PlayerPrefs.SetFloat("etage1CashGain", 147539846145878f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 156539927891191f);
                    everEtageTheSame();
                    break;

                case 463:
                    kostenEtage = kostenEtageGesamt[462];
                    PlayerPrefs.SetFloat("etage1CashGain", 156539927891191f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 165592370819510f);
                    everEtageTheSame();
                    break;

                case 464:
                    kostenEtage = kostenEtageGesamt[463];
                    PlayerPrefs.SetFloat("etage1CashGain", 165592370819510f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 177021796921931f);
                    everEtageTheSame();
                    break;

                case 465:
                    kostenEtage = kostenEtageGesamt[464];
                    PlayerPrefs.SetFloat("etage1CashGain", 177021796921931f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 188484563105963f);
                    everEtageTheSame();
                    break;

                case 466:
                    kostenEtage = kostenEtageGesamt[465];
                    PlayerPrefs.SetFloat("etage1CashGain", 188484563105963f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 202048959705076f);
                    everEtageTheSame();
                    break;

                case 467:
                    kostenEtage = kostenEtageGesamt[466];
                    PlayerPrefs.SetFloat("etage1CashGain", 202048959705076f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 216054558457052f);
                    everEtageTheSame();
                    break;

                case 468:
                    kostenEtage = kostenEtageGesamt[467];
                    PlayerPrefs.SetFloat("etage1CashGain", 216054558457052f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 229655401872784f);
                    everEtageTheSame();
                    break;

                case 469:
                    kostenEtage = kostenEtageGesamt[468];
                    PlayerPrefs.SetFloat("etage1CashGain", 229655401872784f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 245946384138896f);
                    everEtageTheSame();
                    break;

                case 470:
                    kostenEtage = kostenEtageGesamt[469];
                    PlayerPrefs.SetFloat("etage1CashGain", 245946384138896f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 262085445493856f);
                    everEtageTheSame();
                    break;

                case 471:
                    kostenEtage = kostenEtageGesamt[470];
                    PlayerPrefs.SetFloat("etage1CashGain", 262085445493856f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 278932990840077f);
                    everEtageTheSame();
                    break;

                case 472:
                    kostenEtage = kostenEtageGesamt[471];
                    PlayerPrefs.SetFloat("etage1CashGain", 278932990840077f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 297693101349479f);
                    everEtageTheSame();
                    break;

                case 473:
                    kostenEtage = kostenEtageGesamt[472];
                    PlayerPrefs.SetFloat("etage1CashGain", 297693101349479f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 317584154597915f);
                    everEtageTheSame();
                    break;

                case 474:
                    kostenEtage = kostenEtageGesamt[473];
                    PlayerPrefs.SetFloat("etage1CashGain", 317584154597915f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 338027936823711f);
                    everEtageTheSame();
                    break;

                case 475:
                    kostenEtage = kostenEtageGesamt[474];
                    PlayerPrefs.SetFloat("etage1CashGain", 338027936823711f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 361273818555183f);
                    everEtageTheSame();
                    break;

                case 476:
                    kostenEtage = kostenEtageGesamt[475];
                    PlayerPrefs.SetFloat("etage1CashGain", 361273818555183f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 385032389988995f);
                    everEtageTheSame();
                    break;

                case 477:
                    kostenEtage = kostenEtageGesamt[476];
                    PlayerPrefs.SetFloat("etage1CashGain", 385032389988995f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 411745248399160f);
                    everEtageTheSame();
                    break;

                case 478:
                    kostenEtage = kostenEtageGesamt[477];
                    PlayerPrefs.SetFloat("etage1CashGain", 411745248399160f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 439031341710401f);
                    everEtageTheSame();
                    break;

                case 479:
                    kostenEtage = kostenEtageGesamt[478];
                    PlayerPrefs.SetFloat("etage1CashGain", 439031341710401f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 466866183465299f);
                    everEtageTheSame();
                    break;

                case 480:
                    kostenEtage = kostenEtageGesamt[479];
                    PlayerPrefs.SetFloat("etage1CashGain", 466866183465299f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 497222016290764f);
                    everEtageTheSame();
                    break;

                case 481:
                    kostenEtage = kostenEtageGesamt[480];
                    PlayerPrefs.SetFloat("etage1CashGain", 497222016290764f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 528149474156452f);
                    everEtageTheSame();
                    break;

                case 482:
                    kostenEtage = kostenEtageGesamt[481];
                    PlayerPrefs.SetFloat("etage1CashGain", 528149474156452f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 563269080063657f);
                    everEtageTheSame();
                    break;

                case 483:
                    kostenEtage = kostenEtageGesamt[482];
                    PlayerPrefs.SetFloat("etage1CashGain", 563269080063657f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 601437623902456f);
                    everEtageTheSame();
                    break;

                case 484:
                    kostenEtage = kostenEtageGesamt[483];
                    PlayerPrefs.SetFloat("etage1CashGain", 601437623902456f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 642717481513309f);
                    everEtageTheSame();
                    break;

                case 485:
                    kostenEtage = kostenEtageGesamt[484];
                    PlayerPrefs.SetFloat("etage1CashGain", 642717481513309f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 684950415762075f);
                    everEtageTheSame();
                    break;

                case 486:
                    kostenEtage = kostenEtageGesamt[485];
                    PlayerPrefs.SetFloat("etage1CashGain", 684950415762075f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 732738304206696f);
                    everEtageTheSame();
                    break;

                case 487:
                    kostenEtage = kostenEtageGesamt[486];
                    PlayerPrefs.SetFloat("etage1CashGain", 732738304206696f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 780199617301574f);
                    everEtageTheSame();
                    break;

                case 488:
                    kostenEtage = kostenEtageGesamt[487];
                    PlayerPrefs.SetFloat("etage1CashGain", 780199617301574f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 832759026167999f);
                    everEtageTheSame();
                    break;

                case 489:
                    kostenEtage = kostenEtageGesamt[488];
                    PlayerPrefs.SetFloat("etage1CashGain", 832759026167999f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 888443130999240f);
                    everEtageTheSame();
                    break;

                case 490:
                    kostenEtage = kostenEtageGesamt[489];
                    PlayerPrefs.SetFloat("etage1CashGain", 888443130999240f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 948969072646693f);
                    everEtageTheSame();
                    break;

                case 491:
                    kostenEtage = kostenEtageGesamt[490];
                    PlayerPrefs.SetFloat("etage1CashGain", 948969072646693f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1009397595451430f);
                    everEtageTheSame();
                    break;

                case 492:
                    kostenEtage = kostenEtageGesamt[491];
                    PlayerPrefs.SetFloat("etage1CashGain", 1009397595451430f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1071806876977410f);
                    everEtageTheSame();
                    break;

                case 493:
                    kostenEtage = kostenEtageGesamt[492];
                    PlayerPrefs.SetFloat("etage1CashGain", 1071806876977410f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1143038231751080f);
                    everEtageTheSame();
                    break;

                case 494:
                    kostenEtage = kostenEtageGesamt[493];
                    PlayerPrefs.SetFloat("etage1CashGain", 1143038231751080f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1213112690386180f);
                    everEtageTheSame();
                    break;

                case 495:
                    kostenEtage = kostenEtageGesamt[494];
                    PlayerPrefs.SetFloat("etage1CashGain", 1213112690386180f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1285731346181090f);
                    everEtageTheSame();
                    break;

                case 496:
                    kostenEtage = kostenEtageGesamt[495];
                    PlayerPrefs.SetFloat("etage1CashGain", 1285731346181090f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1372362220779140f);
                    everEtageTheSame();
                    break;

                case 497:
                    kostenEtage = kostenEtageGesamt[496];
                    PlayerPrefs.SetFloat("etage1CashGain", 1372362220779140f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1459123317706950f);
                    everEtageTheSame();
                    break;

                case 498:
                    kostenEtage = kostenEtageGesamt[497];
                    PlayerPrefs.SetFloat("etage1CashGain", 1459123317706950f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1754674582201760f);
                    everEtageTheSame();
                    break;

                case 499:
                    kostenEtage = kostenEtageGesamt[498];
                    PlayerPrefs.SetFloat("etage1CashGain", 1754674582201760f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1874176673159150f);
                    everEtageTheSame();
                    break;

                case 500:
                    kostenEtage = kostenEtageGesamt[499];
                    PlayerPrefs.SetFloat("etage1CashGain", 1874176673159150f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1995921928486780f);
                    everEtageTheSame();
                    break;

                case 501:
                    kostenEtage = kostenEtageGesamt[500];
                    PlayerPrefs.SetFloat("etage1CashGain", 1995921928486780f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2114722278621450f);
                    everEtageTheSame();
                    break;

                case 502:
                    kostenEtage = kostenEtageGesamt[501];
                    PlayerPrefs.SetFloat("etage1CashGain", 2114722278621450f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2247126589407800f);
                    everEtageTheSame();
                    break;

                case 503:
                    kostenEtage = kostenEtageGesamt[502];
                    PlayerPrefs.SetFloat("etage1CashGain", 2247126589407800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2390439749089670f);
                    everEtageTheSame();
                    break;

                case 504:
                    kostenEtage = kostenEtageGesamt[503];
                    PlayerPrefs.SetFloat("etage1CashGain", 2390439749089670f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2555177375861850f);
                    everEtageTheSame();
                    break;

                case 505:
                    kostenEtage = kostenEtageGesamt[504];
                    PlayerPrefs.SetFloat("etage1CashGain", 2555177375861850f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2709169168240020f);
                    everEtageTheSame();
                    break;

                case 506:
                    kostenEtage = kostenEtageGesamt[505];
                    PlayerPrefs.SetFloat("etage1CashGain", 2709169168240020f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2890072014704430f);
                    everEtageTheSame();
                    break;

                case 507:
                    kostenEtage = kostenEtageGesamt[506];
                    PlayerPrefs.SetFloat("etage1CashGain", 2890072014704430f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3083314953034660f);
                    everEtageTheSame();
                    break;

                case 508:
                    kostenEtage = kostenEtageGesamt[507];
                    PlayerPrefs.SetFloat("etage1CashGain", 3083314953034660f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3301111266786270f);
                    everEtageTheSame();
                    break;

                case 509:
                    kostenEtage = kostenEtageGesamt[508];
                    PlayerPrefs.SetFloat("etage1CashGain", 3301111266786270f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3515893622543460f);
                    everEtageTheSame();
                    break;

                case 510:
                    kostenEtage = kostenEtageGesamt[509];
                    PlayerPrefs.SetFloat("etage1CashGain", 3515893622543460f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3749814463995190f);
                    everEtageTheSame();
                    break;

                case 511:
                    kostenEtage = kostenEtageGesamt[510];
                    PlayerPrefs.SetFloat("etage1CashGain", 3749814463995190f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3986367655113260f);
                    everEtageTheSame();
                    break;

                case 512:
                    kostenEtage = kostenEtageGesamt[511];
                    PlayerPrefs.SetFloat("etage1CashGain", 3986367655113260f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4233120856072230f);
                    everEtageTheSame();
                    break;

                case 513:
                    kostenEtage = kostenEtageGesamt[512];
                    PlayerPrefs.SetFloat("etage1CashGain", 4233120856072230f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4499387245218910f);
                    everEtageTheSame();
                    break;

                case 514:
                    kostenEtage = kostenEtageGesamt[513];
                    PlayerPrefs.SetFloat("etage1CashGain", 4499387245218910f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4787116407566440f);
                    everEtageTheSame();
                    break;

                case 515:
                    kostenEtage = kostenEtageGesamt[514];
                    PlayerPrefs.SetFloat("etage1CashGain", 4787116407566440f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5089232560123720f);
                    everEtageTheSame();
                    break;

                case 516:
                    kostenEtage = kostenEtageGesamt[515];
                    PlayerPrefs.SetFloat("etage1CashGain", 5089232560123720f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5428794095802590f);
                    everEtageTheSame();
                    break;

                case 517:
                    kostenEtage = kostenEtageGesamt[516];
                    PlayerPrefs.SetFloat("etage1CashGain", 5428794095802590f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5755874553006640f);
                    everEtageTheSame();
                    break;

                case 518:
                    kostenEtage = kostenEtageGesamt[517];
                    PlayerPrefs.SetFloat("etage1CashGain", 5755874553006640f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6117052152067450f);
                    everEtageTheSame();
                    break;

                case 519:
                    kostenEtage = kostenEtageGesamt[518];
                    PlayerPrefs.SetFloat("etage1CashGain", 6117052152067450f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6560304986561460f);
                    everEtageTheSame();
                    break;

                case 520:
                    kostenEtage = kostenEtageGesamt[519];
                    PlayerPrefs.SetFloat("etage1CashGain", 6560304986561460f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6973246688985440f);
                    everEtageTheSame();
                    break;

                case 521:
                    kostenEtage = kostenEtageGesamt[520];
                    PlayerPrefs.SetFloat("etage1CashGain", 6973246688985440f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7424303706817490f);
                    everEtageTheSame();
                    break;

                case 522:
                    kostenEtage = kostenEtageGesamt[521];
                    PlayerPrefs.SetFloat("etage1CashGain", 7424303706817490f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7888519025394410f);
                    everEtageTheSame();
                    break;

                case 523:
                    kostenEtage = kostenEtageGesamt[522];
                    PlayerPrefs.SetFloat("etage1CashGain", 7888519025394410f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8401095657169780f);
                    everEtageTheSame();
                    break;

                case 524:
                    kostenEtage = kostenEtageGesamt[523];
                    PlayerPrefs.SetFloat("etage1CashGain", 8401095657169780f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8941697575464590f);
                    everEtageTheSame();
                    break;

                case 525:
                    kostenEtage = kostenEtageGesamt[524];
                    PlayerPrefs.SetFloat("etage1CashGain", 8941697575464590f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9557249349754410f);
                    everEtageTheSame();
                    break;

                case 526:
                    kostenEtage = kostenEtageGesamt[525];
                    PlayerPrefs.SetFloat("etage1CashGain", 9557249349754410f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10174271298766300f);
                    everEtageTheSame();
                    break;

                case 527:
                    kostenEtage = kostenEtageGesamt[526];
                    PlayerPrefs.SetFloat("etage1CashGain", 10174271298766300f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10833906076038700f);
                    everEtageTheSame();
                    break;

                case 528:
                    kostenEtage = kostenEtageGesamt[527];
                    PlayerPrefs.SetFloat("etage1CashGain", 10833906076038700f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11568356626883400f);
                    everEtageTheSame();
                    break;

                case 529:
                    kostenEtage = kostenEtageGesamt[528];
                    PlayerPrefs.SetFloat("etage1CashGain", 11568356626883400f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12330245009154100f);
                    everEtageTheSame();
                    break;

                case 530:
                    kostenEtage = kostenEtageGesamt[529];
                    PlayerPrefs.SetFloat("etage1CashGain", 12330245009154100f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13161397499194500f);
                    everEtageTheSame();
                    break;

                case 531:
                    kostenEtage = kostenEtageGesamt[530];
                    PlayerPrefs.SetFloat("etage1CashGain", 13161397499194500f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13998282722170500f);
                    everEtageTheSame();
                    break;

                case 532:
                    kostenEtage = kostenEtageGesamt[531];
                    PlayerPrefs.SetFloat("etage1CashGain", 13998282722170500f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 14972046366518200f);
                    everEtageTheSame();
                    break;

                case 533:
                    kostenEtage = kostenEtageGesamt[532];
                    PlayerPrefs.SetFloat("etage1CashGain", 14972046366518200f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 15981622487607800f);
                    everEtageTheSame();
                    break;

                case 534:
                    kostenEtage = kostenEtageGesamt[533];
                    PlayerPrefs.SetFloat("etage1CashGain", 15981622487607800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 17065229257976000f);
                    everEtageTheSame();
                    break;

                case 535:
                    kostenEtage = kostenEtageGesamt[534];
                    PlayerPrefs.SetFloat("etage1CashGain", 17065229257976000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 18194365914436900f);
                    everEtageTheSame();
                    break;

                case 536:
                    kostenEtage = kostenEtageGesamt[535];
                    PlayerPrefs.SetFloat("etage1CashGain", 18194365914436900f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 19466505432718700f);
                    everEtageTheSame();
                    break;

                case 537:
                    kostenEtage = kostenEtageGesamt[536];
                    PlayerPrefs.SetFloat("etage1CashGain", 19466505432718700f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 20687589127276600f);
                    everEtageTheSame();
                    break;

                case 538:
                    kostenEtage = kostenEtageGesamt[537];
                    PlayerPrefs.SetFloat("etage1CashGain", 20687589127276600f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 21991948728828400f);
                    everEtageTheSame();
                    break;

                case 539:
                    kostenEtage = kostenEtageGesamt[538];
                    PlayerPrefs.SetFloat("etage1CashGain", 21991948728828400f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 23482780687299700f);
                    everEtageTheSame();
                    break;

                case 540:
                    kostenEtage = kostenEtageGesamt[539];
                    PlayerPrefs.SetFloat("etage1CashGain", 23482780687299700f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 25040481593222300f);
                    everEtageTheSame();
                    break;

                case 541:
                    kostenEtage = kostenEtageGesamt[540];
                    PlayerPrefs.SetFloat("etage1CashGain", 25040481593222300f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 26711110510310700f);
                    everEtageTheSame();
                    break;

                case 542:
                    kostenEtage = kostenEtageGesamt[541];
                    PlayerPrefs.SetFloat("etage1CashGain", 26711110510310700f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 28587733908177100f);
                    everEtageTheSame();
                    break;

                case 543:
                    kostenEtage = kostenEtageGesamt[542];
                    PlayerPrefs.SetFloat("etage1CashGain", 28587733908177100f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 30435029019706800f);
                    everEtageTheSame();
                    break;

                case 544:
                    kostenEtage = kostenEtageGesamt[543];
                    PlayerPrefs.SetFloat("etage1CashGain", 30435029019706800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 32359196700228800f);
                    everEtageTheSame();
                    break;

                case 545:
                    kostenEtage = kostenEtageGesamt[544];
                    PlayerPrefs.SetFloat("etage1CashGain", 32359196700228800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 34664384829170800f);
                    everEtageTheSame();
                    break;

                case 546:
                    kostenEtage = kostenEtageGesamt[545];
                    PlayerPrefs.SetFloat("etage1CashGain", 34664384829170800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 36888130881164800f);
                    everEtageTheSame();
                    break;

                case 547:
                    kostenEtage = kostenEtageGesamt[546];
                    PlayerPrefs.SetFloat("etage1CashGain", 36888130881164800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 39195861302533800f);
                    everEtageTheSame();
                    break;

                case 548:
                    kostenEtage = kostenEtageGesamt[547];
                    PlayerPrefs.SetFloat("etage1CashGain", 39195861302533800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 41689568656944800f);
                    everEtageTheSame();
                    break;

                case 549:
                    kostenEtage = kostenEtageGesamt[548];
                    PlayerPrefs.SetFloat("etage1CashGain", 41689568656944800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 44281029688583400f);
                    everEtageTheSame();
                    break;

                case 550:
                    kostenEtage = kostenEtageGesamt[549];
                    PlayerPrefs.SetFloat("etage1CashGain", 44281029688583400f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 47251596652451900f);
                    everEtageTheSame();
                    break;

                case 551:
                    kostenEtage = kostenEtageGesamt[550];
                    PlayerPrefs.SetFloat("etage1CashGain", 47251596652451900f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 50520349468752900f);
                    everEtageTheSame();
                    break;

                case 552:
                    kostenEtage = kostenEtageGesamt[551];
                    PlayerPrefs.SetFloat("etage1CashGain", 50520349468752900f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 53707116113882100f);
                    everEtageTheSame();
                    break;

                case 553:
                    kostenEtage = kostenEtageGesamt[552];
                    PlayerPrefs.SetFloat("etage1CashGain", 53707116113882100f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 57198811681097400f);
                    everEtageTheSame();
                    break;

                case 554:
                    kostenEtage = kostenEtageGesamt[553];
                    PlayerPrefs.SetFloat("etage1CashGain", 57198811681097400f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 61073700982486500f);
                    everEtageTheSame();
                    break;

                case 555:
                    kostenEtage = kostenEtageGesamt[554];
                    PlayerPrefs.SetFloat("etage1CashGain", 61073700982486500f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 64939343500168500f);
                    everEtageTheSame();
                    break;

                case 556:
                    kostenEtage = kostenEtageGesamt[555];
                    PlayerPrefs.SetFloat("etage1CashGain", 64939343500168500f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 69137497681640900f);
                    everEtageTheSame();
                    break;

                case 557:
                    kostenEtage = kostenEtageGesamt[556];
                    PlayerPrefs.SetFloat("etage1CashGain", 69137497681640900f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 73099140421021400f);
                    everEtageTheSame();
                    break;

                case 558:
                    kostenEtage = kostenEtageGesamt[557];
                    PlayerPrefs.SetFloat("etage1CashGain", 73099140421021400f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 77782684804083300f);
                    everEtageTheSame();
                    break;

                case 559:
                    kostenEtage = kostenEtageGesamt[558];
                    PlayerPrefs.SetFloat("etage1CashGain", 77782684804083300f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 82698918296106500f);
                    everEtageTheSame();
                    break;

                case 560:
                    kostenEtage = kostenEtageGesamt[559];
                    PlayerPrefs.SetFloat("etage1CashGain", 82698918296106500f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 88246464717818800f);
                    everEtageTheSame();
                    break;

                case 561:
                    kostenEtage = kostenEtageGesamt[560];
                    PlayerPrefs.SetFloat("etage1CashGain", 88246464717818800f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 93793515365940000f);
                    everEtageTheSame();
                    break;

                case 562:
                    kostenEtage = kostenEtageGesamt[561];
                    PlayerPrefs.SetFloat("etage1CashGain", 93793515365940000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 99510926135055300f);
                    everEtageTheSame();
                    break;

                case 563:
                    kostenEtage = kostenEtageGesamt[562];
                    PlayerPrefs.SetFloat("etage1CashGain", 99510926135055300f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 105861104558239000f);
                    everEtageTheSame();
                    break;

                case 564:
                    kostenEtage = kostenEtageGesamt[563];
                    PlayerPrefs.SetFloat("etage1CashGain", 105861104558239000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 112917462992895000f);
                    everEtageTheSame();
                    break;

                case 565:
                    kostenEtage = kostenEtageGesamt[564];
                    PlayerPrefs.SetFloat("etage1CashGain", 112917462992895000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 120379959158802000f);
                    everEtageTheSame();
                    break;

                case 566:
                    kostenEtage = kostenEtageGesamt[565];
                    PlayerPrefs.SetFloat("etage1CashGain", 120379959158802000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 127696260784340000f);
                    everEtageTheSame();
                    break;

                case 567:
                    kostenEtage = kostenEtageGesamt[566];
                    PlayerPrefs.SetFloat("etage1CashGain", 127696260784340000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 135734933314343000f);
                    everEtageTheSame();
                    break;

                case 568:
                    kostenEtage = kostenEtageGesamt[567];
                    PlayerPrefs.SetFloat("etage1CashGain", 135734933314343000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 144537066396384000f);
                    everEtageTheSame();
                    break;

                case 569:
                    kostenEtage = kostenEtageGesamt[568];
                    PlayerPrefs.SetFloat("etage1CashGain", 144537066396384000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 153546678463416000f);
                    everEtageTheSame();
                    break;

                case 570:
                    kostenEtage = kostenEtageGesamt[569];
                    PlayerPrefs.SetFloat("etage1CashGain", 153546678463416000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 164094497970442000f);
                    everEtageTheSame();
                    break;

                case 571:
                    kostenEtage = kostenEtageGesamt[570];
                    PlayerPrefs.SetFloat("etage1CashGain", 164094497970442000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 175174597607297000f);
                    everEtageTheSame();
                    break;

                case 572:
                    kostenEtage = kostenEtageGesamt[571];
                    PlayerPrefs.SetFloat("etage1CashGain", 175174597607297000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 187099715411646000f);
                    everEtageTheSame();
                    break;

                case 573:
                    kostenEtage = kostenEtageGesamt[572];
                    PlayerPrefs.SetFloat("etage1CashGain", 187099715411646000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 198976781681360000f);
                    everEtageTheSame();
                    break;

                case 574:
                    kostenEtage = kostenEtageGesamt[573];
                    PlayerPrefs.SetFloat("etage1CashGain", 198976781681360000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 212140170531870000f);
                    everEtageTheSame();
                    break;

                case 575:
                    kostenEtage = kostenEtageGesamt[574];
                    PlayerPrefs.SetFloat("etage1CashGain", 212140170531870000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 226647034202808000f);
                    everEtageTheSame();
                    break;

                case 576:
                    kostenEtage = kostenEtageGesamt[575];
                    PlayerPrefs.SetFloat("etage1CashGain", 226647034202808000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 240651444753698000f);
                    everEtageTheSame();
                    break;

                case 577:
                    kostenEtage = kostenEtageGesamt[576];
                    PlayerPrefs.SetFloat("etage1CashGain", 240651444753698000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 256149187305861000f);
                    everEtageTheSame();
                    break;

                case 578:
                    kostenEtage = kostenEtageGesamt[577];
                    PlayerPrefs.SetFloat("etage1CashGain", 256149187305861000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 272184826169229000f);
                    everEtageTheSame();
                    break;

                case 579:
                    kostenEtage = kostenEtageGesamt[578];
                    PlayerPrefs.SetFloat("etage1CashGain", 272184826169229000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 289517479481070000f);
                    everEtageTheSame();
                    break;

                case 580:
                    kostenEtage = kostenEtageGesamt[579];
                    PlayerPrefs.SetFloat("etage1CashGain", 289517479481070000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 308489038086366000f);
                    everEtageTheSame();
                    break;

                case 581:
                    kostenEtage = kostenEtageGesamt[580];
                    PlayerPrefs.SetFloat("etage1CashGain", 308489038086366000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 328930017372940000f);
                    everEtageTheSame();
                    break;

                case 582:
                    kostenEtage = kostenEtageGesamt[581];
                    PlayerPrefs.SetFloat("etage1CashGain", 328930017372940000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 350499623726752000f);
                    everEtageTheSame();
                    break;

                case 583:
                    kostenEtage = kostenEtageGesamt[582];
                    PlayerPrefs.SetFloat("etage1CashGain", 350499623726752000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 373289691117522000f);
                    everEtageTheSame();
                    break;

                case 584:
                    kostenEtage = kostenEtageGesamt[583];
                    PlayerPrefs.SetFloat("etage1CashGain", 373289691117522000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 397837046412724000f);
                    everEtageTheSame();
                    break;

                case 585:
                    kostenEtage = kostenEtageGesamt[584];
                    PlayerPrefs.SetFloat("etage1CashGain", 397837046412724000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 424485574337392000f);
                    everEtageTheSame();
                    break;

                case 586:
                    kostenEtage = kostenEtageGesamt[585];
                    PlayerPrefs.SetFloat("etage1CashGain", 424485574337392000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 450944476970984000f);
                    everEtageTheSame();
                    break;

                case 587:
                    kostenEtage = kostenEtageGesamt[586];
                    PlayerPrefs.SetFloat("etage1CashGain", 450944476970984000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 477760537275867000f);
                    everEtageTheSame();
                    break;

                case 588:
                    kostenEtage = kostenEtageGesamt[587];
                    PlayerPrefs.SetFloat("etage1CashGain", 477760537275867000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 505953028981485000f);
                    everEtageTheSame();
                    break;

                case 589:
                    kostenEtage = kostenEtageGesamt[588];
                    PlayerPrefs.SetFloat("etage1CashGain", 505953028981485000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 538178968835159000f);
                    everEtageTheSame();
                    break;

                case 590:
                    kostenEtage = kostenEtageGesamt[589];
                    PlayerPrefs.SetFloat("etage1CashGain", 538178968835159000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 574382715389508000f);
                    everEtageTheSame();
                    break;

                case 591:
                    kostenEtage = kostenEtageGesamt[590];
                    PlayerPrefs.SetFloat("etage1CashGain", 574382715389508000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 611471478849811000f);
                    everEtageTheSame();
                    break;

                case 592:
                    kostenEtage = kostenEtageGesamt[591];
                    PlayerPrefs.SetFloat("etage1CashGain", 611471478849811000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 651298405118833000f);
                    everEtageTheSame();
                    break;

                case 593:
                    kostenEtage = kostenEtageGesamt[592];
                    PlayerPrefs.SetFloat("etage1CashGain", 651298405118833000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 694017758339459000f);
                    everEtageTheSame();
                    break;

                case 594:
                    kostenEtage = kostenEtageGesamt[593];
                    PlayerPrefs.SetFloat("etage1CashGain", 694017758339459000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 739557580094079000f);
                    everEtageTheSame();
                    break;

                case 595:
                    kostenEtage = kostenEtageGesamt[594];
                    PlayerPrefs.SetFloat("etage1CashGain", 739557580094079000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 789336214022152000f);
                    everEtageTheSame();
                    break;

                case 596:
                    kostenEtage = kostenEtageGesamt[595];
                    PlayerPrefs.SetFloat("etage1CashGain", 789336214022152000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 836396348618100000f);
                    everEtageTheSame();
                    break;

                case 597:
                    kostenEtage = kostenEtageGesamt[596];
                    PlayerPrefs.SetFloat("etage1CashGain", 836396348618100000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 888964387737496000f);
                    everEtageTheSame();
                    break;
                case 598:
                    kostenEtage = kostenEtageGesamt[597];
                    PlayerPrefs.SetFloat("etage1CashGain", 888964387737496000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1090444485905630000f);
                    everEtageTheSame();
                    break;
                case 599:
                    kostenEtage = kostenEtageGesamt[598];
                    PlayerPrefs.SetFloat("etage1CashGain", 1090444485905630000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1170550767429910000f);
                    everEtageTheSame();
                    break;
                case 600:
                    kostenEtage = kostenEtageGesamt[599];
                    PlayerPrefs.SetFloat("etage1CashGain", 1170550767429910000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1242700541113810000f);
                    everEtageTheSame();
                    break;
                case 601:
                    kostenEtage = kostenEtageGesamt[600];
                    PlayerPrefs.SetFloat("etage1CashGain", 1242700541113810000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1321794513611760000f);
                    everEtageTheSame();
                    break;

                case 602:
                    kostenEtage = kostenEtageGesamt[601];
                    PlayerPrefs.SetFloat("etage1CashGain", 1321794513611760000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1415192902062310000f);
                    everEtageTheSame();
                    break;

                case 603:
                    kostenEtage = kostenEtageGesamt[602];
                    PlayerPrefs.SetFloat("etage1CashGain", 1415192902062310000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1505644936150200000f);
                    everEtageTheSame();
                    break;

                case 604:
                    kostenEtage = kostenEtageGesamt[603];
                    PlayerPrefs.SetFloat("etage1CashGain", 1505644936150200000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1602953079955290000f);
                    everEtageTheSame();
                    break;

                case 605:
                    kostenEtage = kostenEtageGesamt[604];
                    PlayerPrefs.SetFloat("etage1CashGain", 1602953079955290000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1709630542118440000f);
                    everEtageTheSame();
                    break;

                case 606:
                    kostenEtage = kostenEtageGesamt[605];
                    PlayerPrefs.SetFloat("etage1CashGain", 1709630542118440000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1839588292735230000f);
                    everEtageTheSame();
                    break;

                case 607:
                    kostenEtage = kostenEtageGesamt[606];
                    PlayerPrefs.SetFloat("etage1CashGain", 1839588292735230000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1952502288538240000f);
                    everEtageTheSame();
                    break;

                case 608:
                    kostenEtage = kostenEtageGesamt[607];
                    PlayerPrefs.SetFloat("etage1CashGain", 1952502288538240000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2078708414474960000f);
                    everEtageTheSame();
                    break;

                case 609:
                    kostenEtage = kostenEtageGesamt[608];
                    PlayerPrefs.SetFloat("etage1CashGain", 2078708414474960000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2210110076837650000f);
                    everEtageTheSame();
                    break;

                case 610:
                    kostenEtage = kostenEtageGesamt[609];
                    PlayerPrefs.SetFloat("etage1CashGain", 2210110076837650000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2355919803785860000f);
                    everEtageTheSame();
                    break;

                case 611:
                    kostenEtage = kostenEtageGesamt[610];
                    PlayerPrefs.SetFloat("etage1CashGain", 2355919803785860000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2505940533409960000f);
                    everEtageTheSame();
                    break;

                case 612:
                    kostenEtage = kostenEtageGesamt[611];
                    PlayerPrefs.SetFloat("etage1CashGain", 2505940533409960000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2669610517146810000f);
                    everEtageTheSame();
                    break;

                case 613:
                    kostenEtage = kostenEtageGesamt[612];
                    PlayerPrefs.SetFloat("etage1CashGain", 2669610517146810000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2832408568354190000f);
                    everEtageTheSame();
                    break;

                case 614:
                    kostenEtage = kostenEtageGesamt[613];
                    PlayerPrefs.SetFloat("etage1CashGain", 2832408568354190000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3013938816108270000f);
                    everEtageTheSame();
                    break;

                case 615:
                    kostenEtage = kostenEtageGesamt[614];
                    PlayerPrefs.SetFloat("etage1CashGain", 3013938816108270000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3201714319155490000f);
                    everEtageTheSame();
                    break;

                case 616:
                    kostenEtage = kostenEtageGesamt[615];
                    PlayerPrefs.SetFloat("etage1CashGain", 3201714319155490000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3406749321686350000f);
                    everEtageTheSame();
                    break;

                case 617:
                    kostenEtage = kostenEtageGesamt[616];
                    PlayerPrefs.SetFloat("etage1CashGain", 3406749321686350000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3636869290423800000f);
                    everEtageTheSame();
                    break;

                case 618:
                    kostenEtage = kostenEtageGesamt[617];
                    PlayerPrefs.SetFloat("etage1CashGain", 3636869290423800000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3868924998448510000f);
                    everEtageTheSame();
                    break;

                case 619:
                    kostenEtage = kostenEtageGesamt[618];
                    PlayerPrefs.SetFloat("etage1CashGain", 3868924998448510000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4126170403539470000f);
                    everEtageTheSame();
                    break;

                case 620:
                    kostenEtage = kostenEtageGesamt[619];
                    PlayerPrefs.SetFloat("etage1CashGain", 4126170403539470000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4406690638448090000f);
                    everEtageTheSame();
                    break;

                case 621:
                    kostenEtage = kostenEtageGesamt[620];
                    PlayerPrefs.SetFloat("etage1CashGain", 4406690638448090000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4707377540845200000f);
                    everEtageTheSame();
                    break;

                case 622:
                    kostenEtage = kostenEtageGesamt[621];
                    PlayerPrefs.SetFloat("etage1CashGain", 4707377540845200000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5014119296877170000f);
                    everEtageTheSame();
                    break;

                case 623:
                    kostenEtage = kostenEtageGesamt[622];
                    PlayerPrefs.SetFloat("etage1CashGain", 5014119296877170000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5336649284909850000f);
                    everEtageTheSame();
                    break;

                case 624:
                    kostenEtage = kostenEtageGesamt[623];
                    PlayerPrefs.SetFloat("etage1CashGain", 5336649284909850000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5698760927235310000f);
                    everEtageTheSame();
                    break;

                case 625:
                    kostenEtage = kostenEtageGesamt[624];
                    PlayerPrefs.SetFloat("etage1CashGain", 5698760927235310000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6057478311357940000f);
                    everEtageTheSame();
                    break;

                case 626:
                    kostenEtage = kostenEtageGesamt[625];
                    PlayerPrefs.SetFloat("etage1CashGain", 6057478311357940000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6434407526486820000f);
                    everEtageTheSame();
                    break;

                case 627:
                    kostenEtage = kostenEtageGesamt[626];
                    PlayerPrefs.SetFloat("etage1CashGain", 6434407526486820000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6824890777669810000f);
                    everEtageTheSame();
                    break;

                case 628:
                    kostenEtage = kostenEtageGesamt[627];
                    PlayerPrefs.SetFloat("etage1CashGain", 6824890777669810000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7242031494534010000f);
                    everEtageTheSame();
                    break;

                case 629:
                    kostenEtage = kostenEtageGesamt[628];
                    PlayerPrefs.SetFloat("etage1CashGain", 7242031494534010000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7733071372247010000f);
                    everEtageTheSame();
                    break;

                case 630:
                    kostenEtage = kostenEtageGesamt[629];
                    PlayerPrefs.SetFloat("etage1CashGain", 7733071372247010000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8280314638615560000f);
                    everEtageTheSame();
                    break;

                case 631:
                    kostenEtage = kostenEtageGesamt[630];
                    PlayerPrefs.SetFloat("etage1CashGain", 8280314638615560000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8828957761546500000f);
                    everEtageTheSame();
                    break;

                case 632:
                    kostenEtage = kostenEtageGesamt[631];
                    PlayerPrefs.SetFloat("etage1CashGain", 8828957761546500000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9430161540506160000f);
                    everEtageTheSame();
                    break;

                case 633:
                    kostenEtage = kostenEtageGesamt[632];
                    PlayerPrefs.SetFloat("etage1CashGain", 9430161540506160000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10040969363768600000f);
                    everEtageTheSame();
                    break;

                case 634:
                    kostenEtage = kostenEtageGesamt[633];
                    PlayerPrefs.SetFloat("etage1CashGain", 10040969363768600000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 10670339863007600000f);
                    everEtageTheSame();
                    break;

                case 635:
                    kostenEtage = kostenEtageGesamt[634];
                    PlayerPrefs.SetFloat("etage1CashGain", 10670339863007600000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 11392237463071000000f);
                    everEtageTheSame();
                    break;

                case 636:
                    kostenEtage = kostenEtageGesamt[635];
                    PlayerPrefs.SetFloat("etage1CashGain", 11392237463071000000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12190082595445300000f);
                    everEtageTheSame();
                    break;

                case 637:
                    kostenEtage = kostenEtageGesamt[636];
                    PlayerPrefs.SetFloat("etage1CashGain", 12190082595445300000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12912008468989600000f);
                    everEtageTheSame();
                    break;

                case 638:
                    kostenEtage = kostenEtageGesamt[637];
                    PlayerPrefs.SetFloat("etage1CashGain", 12912008468989600000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 13773320271983100000f);
                    everEtageTheSame();
                    break;

                case 639:
                    kostenEtage = kostenEtageGesamt[638];
                    PlayerPrefs.SetFloat("etage1CashGain", 13773320271983100000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 14661488620165500000f);
                    everEtageTheSame();
                    break;

                case 640:
                    kostenEtage = kostenEtageGesamt[639];
                    PlayerPrefs.SetFloat("etage1CashGain", 14661488620165500000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 15550448981640700000f);
                    everEtageTheSame();
                    break;

                case 641:
                    kostenEtage = kostenEtageGesamt[640];
                    PlayerPrefs.SetFloat("etage1CashGain", 15550448981640700000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 16582026631371300000f);
                    everEtageTheSame();
                    break;

                case 642:
                    kostenEtage = kostenEtageGesamt[641];
                    PlayerPrefs.SetFloat("etage1CashGain", 16582026631371300000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 17725023060042400000f);
                    everEtageTheSame();
                    break;
                case 643:
                    kostenEtage = kostenEtageGesamt[642];
                    PlayerPrefs.SetFloat("etage1CashGain", 17725023060042400000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 18785336519673100000f);
                    everEtageTheSame();
                    break;
                case 644:
                    kostenEtage = kostenEtageGesamt[643];
                    PlayerPrefs.SetFloat("etage1CashGain", 18785336519673100000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 20039270830521000000f);
                    everEtageTheSame();
                    break;
                case 645:
                    kostenEtage = kostenEtageGesamt[644];
                    PlayerPrefs.SetFloat("etage1CashGain", 20039270830521000000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 21374786003300400000f);
                    everEtageTheSame();
                    break;
                case 646:
                    kostenEtage = kostenEtageGesamt[645];
                    PlayerPrefs.SetFloat("etage1CashGain", 21374786003300400000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 22875689273037300000f);
                    everEtageTheSame();
                    break;
                case 647:
                    kostenEtage = kostenEtageGesamt[646];
                    PlayerPrefs.SetFloat("etage1CashGain", 22875689273037300000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 24386911062588400000f);
                    everEtageTheSame();
                    break;
                case 648:
                    kostenEtage = kostenEtageGesamt[647];
                    PlayerPrefs.SetFloat("etage1CashGain", 24386911062588400000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 26067592127517300000f);
                    everEtageTheSame();
                    break;
                case 649:
                    kostenEtage = kostenEtageGesamt[648];
                    PlayerPrefs.SetFloat("etage1CashGain", 26067592127517300000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 27661993361088400000f);
                    everEtageTheSame();
                    break;
                case 650:
                    kostenEtage = kostenEtageGesamt[649];
                    PlayerPrefs.SetFloat("etage1CashGain", 27661993361088400000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 29475815226108000000f);
                    everEtageTheSame();
                    break;
                case 651:
                    kostenEtage = kostenEtageGesamt[650];
                    PlayerPrefs.SetFloat("etage1CashGain", 29475815226108000000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 31219337184667000000f);
                    everEtageTheSame();
                    break;
                case 652:
                    kostenEtage = kostenEtageGesamt[651];
                    PlayerPrefs.SetFloat("etage1CashGain", 31219337184667000000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 33329373058099700000f);
                    everEtageTheSame();
                    break;
                case 653:
                    kostenEtage = kostenEtageGesamt[652];
                    PlayerPrefs.SetFloat("etage1CashGain", 33329373058099700000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 35481839226667200000f);
                    everEtageTheSame();
                    break;
                case 654:
                    kostenEtage = kostenEtageGesamt[653];
                    PlayerPrefs.SetFloat("etage1CashGain", 35481839226667200000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 37776464970714200000f);
                    everEtageTheSame();
                    break;
                case 655:
                    kostenEtage = kostenEtageGesamt[654];
                    PlayerPrefs.SetFloat("etage1CashGain", 37776464970714200000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 40219772030295700000f);
                    everEtageTheSame();
                    break;
                case 656:
                    kostenEtage = kostenEtageGesamt[655];
                    PlayerPrefs.SetFloat("etage1CashGain", 40219772030295700000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 42878704467679600000f);
                    everEtageTheSame();
                    break;
                case 657:
                    kostenEtage = kostenEtageGesamt[656];
                    PlayerPrefs.SetFloat("etage1CashGain", 42878704467679600000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 45449159905017900000f);
                    everEtageTheSame();
                    break;
                case 658:
                    kostenEtage = kostenEtageGesamt[657];
                    PlayerPrefs.SetFloat("etage1CashGain", 45449159905017900000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 48480952888045200000f);
                    everEtageTheSame();
                    break;
                case 659:
                    kostenEtage = kostenEtageGesamt[658];
                    PlayerPrefs.SetFloat("etage1CashGain", 48480952888045200000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 51669504524333400000f);
                    everEtageTheSame();
                    break;
                case 660:
                    kostenEtage = kostenEtageGesamt[659];
                    PlayerPrefs.SetFloat("etage1CashGain", 51669504524333400000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 55210738554179300000f);
                    everEtageTheSame();
                    break;
                case 661:
                    kostenEtage = kostenEtageGesamt[660];
                    PlayerPrefs.SetFloat("etage1CashGain", 55210738554179300000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 59005990466242100000f);
                    everEtageTheSame();
                    break;
                case 662:
                    kostenEtage = kostenEtageGesamt[661];
                    PlayerPrefs.SetFloat("etage1CashGain", 59005990466242100000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 63067414555959600000f);
                    everEtageTheSame();
                    break;
                case 663:
                    kostenEtage = kostenEtageGesamt[662];
                    PlayerPrefs.SetFloat("etage1CashGain", 63067414555959600000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 67421650681344100000f);
                    everEtageTheSame();
                    break;
                case 664:
                    kostenEtage = kostenEtageGesamt[663];
                    PlayerPrefs.SetFloat("etage1CashGain", 67421650681344100000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 71908192886210400000f);
                    everEtageTheSame();
                    break;
                case 665:
                    kostenEtage = kostenEtageGesamt[664];
                    PlayerPrefs.SetFloat("etage1CashGain", 71908192886210400000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 76256414687478300000f);
                    everEtageTheSame();
                    break;
                case 666:
                    kostenEtage = kostenEtageGesamt[665];
                    PlayerPrefs.SetFloat("etage1CashGain", 76256414687478300000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 81101255884797300000f);
                    everEtageTheSame();
                    break;
                case 667:
                    kostenEtage = kostenEtageGesamt[666];
                    PlayerPrefs.SetFloat("etage1CashGain", 81101255884797300000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 85933963881766200000f);
                    everEtageTheSame();
                    break;
                case 668:
                    kostenEtage = kostenEtageGesamt[667];
                    PlayerPrefs.SetFloat("etage1CashGain", 85933963881766200000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 91464674265834900000f);
                    everEtageTheSame();
                    break;
                case 669:
                    kostenEtage = kostenEtageGesamt[668];
                    PlayerPrefs.SetFloat("etage1CashGain", 91464674265834900000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 97501148507572100000f);
                    everEtageTheSame();
                    break;
                case 670:
                    kostenEtage = kostenEtageGesamt[669];
                    PlayerPrefs.SetFloat("etage1CashGain", 97501148507572100000f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.03858639888064E+20f);
                    everEtageTheSame();
                    break;
                case 671:
                    kostenEtage = kostenEtageGesamt[670];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.03858639888064E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.10500583045133E+20f);
                    everEtageTheSame();
                    break;
                case 672:
                    kostenEtage = kostenEtageGesamt[671];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.10500583045133E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.17717985982454E+20f);
                    everEtageTheSame();
                    break;
                case 673:
                    kostenEtage = kostenEtageGesamt[672];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.17717985982454E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.25507526767489E+20f);
                    everEtageTheSame();
                    break;
                case 674:
                    kostenEtage = kostenEtageGesamt[673];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.25507526767489E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.34304342720672E+20f);
                    everEtageTheSame();
                    break;
                case 675:
                    kostenEtage = kostenEtageGesamt[674];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.34304342720672E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.43111240804534E+20f);
                    everEtageTheSame();
                    break;
                case 676:
                    kostenEtage = kostenEtageGesamt[675];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.43111240804534E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.52461525933187E+20f);
                    everEtageTheSame();
                    break;
                case 677:
                    kostenEtage = kostenEtageGesamt[676];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.52461525933187E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.62717975145771E+20f);
                    everEtageTheSame();
                    break;
                case 678:
                    kostenEtage = kostenEtageGesamt[677];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.62717975145771E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.73412352232688E+20f);
                    everEtageTheSame();
                    break;
                case 679:
                    kostenEtage = kostenEtageGesamt[678];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.73412352232688E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.84016251832938E+20f);
                    everEtageTheSame();
                    break;
                case 680:
                    kostenEtage = kostenEtageGesamt[679];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.84016251832938E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.96274737089738E+20f);
                    everEtageTheSame();
                    break;
                case 681:
                    kostenEtage = kostenEtageGesamt[680];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.96274737089738E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.09072533608765E+20f);
                    everEtageTheSame();
                    break;
                case 682:
                    kostenEtage = kostenEtageGesamt[681];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.09072533608765E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.22886596270529E+20f);
                    everEtageTheSame();
                    break;
                case 683:
                    kostenEtage = kostenEtageGesamt[682];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.22886596270529E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.37498878741321E+20f);
                    everEtageTheSame();
                    break;
                case 684:
                    kostenEtage = kostenEtageGesamt[683];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.37498878741321E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.5308596754062E+20f);
                    everEtageTheSame();
                    break;
                case 685:
                    kostenEtage = kostenEtageGesamt[684];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.5308596754062E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.6929149450439E+20f);
                    everEtageTheSame();
                    break;
                case 686:
                    kostenEtage = kostenEtageGesamt[685];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.6929149450439E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.86340552742689E+20f);
                    everEtageTheSame();
                    break;
                case 687:
                    kostenEtage = kostenEtageGesamt[686];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.86340552742689E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.04176544264128E+20f);
                    everEtageTheSame();
                    break;
                case 688:
                    kostenEtage = kostenEtageGesamt[687];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.04176544264128E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.23780453659056E+20f);
                    everEtageTheSame();
                    break;
                case 689:
                    kostenEtage = kostenEtageGesamt[688];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.23780453659056E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.43536149530446E+20f);
                    everEtageTheSame();
                    break;
                case 690:
                    kostenEtage = kostenEtageGesamt[689];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.43536149530446E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.66390454881457E+20f);
                    everEtageTheSame();
                    break;
                case 691:
                    kostenEtage = kostenEtageGesamt[690];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.66390454881457E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.9037508330921E+20f);
                    everEtageTheSame();
                    break;
                case 692:
                    kostenEtage = kostenEtageGesamt[691];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.9037508330921E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.16748272758136E+20f);
                    everEtageTheSame();
                    break;
                case 693:
                    kostenEtage = kostenEtageGesamt[692];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.16748272758136E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.47128450784711E+20f);
                    everEtageTheSame();
                    break;
                case 694:
                    kostenEtage = kostenEtageGesamt[693];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.47128450784711E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.74218938512161E+20f);
                    everEtageTheSame();
                    break;
                case 695:
                    kostenEtage = kostenEtageGesamt[694];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.74218938512161E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.06138812936535E+20f);
                    everEtageTheSame();
                    break;
                case 696:
                    kostenEtage = kostenEtageGesamt[695];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.06138812936535E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.3803813384864E+20f);
                    everEtageTheSame();
                    break;
                case 697:
                    kostenEtage = kostenEtageGesamt[696];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.3803813384864E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.72927877922003E+20f);
                    everEtageTheSame();
                    break;
                case 698:
                    kostenEtage = kostenEtageGesamt[697];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.72927877922003E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.78724033123896E+20f);
                    everEtageTheSame();
                    break;
                case 699:
                    kostenEtage = kostenEtageGesamt[698];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.78724033123896E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7.20848910162138E+20f);
                    everEtageTheSame();
                    break;
                case 700:
                    kostenEtage = kostenEtageGesamt[699];
                    PlayerPrefs.SetFloat("etage1CashGain", 7.20848910162138E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7.64860056133882E+20f);
                    everEtageTheSame();
                    break;
                case 701:
                    kostenEtage = kostenEtageGesamt[700];
                    PlayerPrefs.SetFloat("etage1CashGain", 7.64860056133882E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8.14790070616139E+20f);
                    everEtageTheSame();
                    break;
                case 702:
                    kostenEtage = kostenEtageGesamt[701];
                    PlayerPrefs.SetFloat("etage1CashGain", 8.14790070616139E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8.65202162988193E+20f);
                    everEtageTheSame();
                    break;
                case 703:
                    kostenEtage = kostenEtageGesamt[702];
                    PlayerPrefs.SetFloat("etage1CashGain", 8.65202162988193E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9.27509349205679E+20f);
                    everEtageTheSame();
                    break;
                case 704:
                    kostenEtage = kostenEtageGesamt[703];
                    PlayerPrefs.SetFloat("etage1CashGain", 9.27509349205679E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9.87619959655938E+20f);
                    everEtageTheSame();
                    break;
                case 705:
                    kostenEtage = kostenEtageGesamt[704];
                    PlayerPrefs.SetFloat("etage1CashGain", 9.87619959655938E+20f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.05112729145014E+21f);
                    everEtageTheSame();
                    break;
                case 706:
                    kostenEtage = kostenEtageGesamt[705];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.05112729145014E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.1207353655442E+21f);
                    everEtageTheSame();
                    break;
                case 707:
                    kostenEtage = kostenEtageGesamt[706];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.1207353655442E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.19237934871737E+21f);
                    everEtageTheSame();
                    break;
                case 708:
                    kostenEtage = kostenEtageGesamt[707];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.19237934871737E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.26945714994549E+21f);
                    everEtageTheSame();
                    break;
                case 709:
                    kostenEtage = kostenEtageGesamt[708];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.26945714994549E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.35362361664922E+21f);
                    everEtageTheSame();
                    break;
                case 710:
                    kostenEtage = kostenEtageGesamt[709];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.35362361664922E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.44003895323865E+21f);
                    everEtageTheSame();
                    break;
                case 711:
                    kostenEtage = kostenEtageGesamt[710];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.44003895323865E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.52885848249967E+21f);
                    everEtageTheSame();
                    break;
                case 712:
                    kostenEtage = kostenEtageGesamt[711];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.52885848249967E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.63483938406744E+21f);
                    everEtageTheSame();
                    break;
                case 713:
                    kostenEtage = kostenEtageGesamt[712];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.63483938406744E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.74878317454284E+21f);
                    everEtageTheSame();
                    break;
                case 714:
                    kostenEtage = kostenEtageGesamt[713];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.74878317454284E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.85619938321094E+21f);
                    everEtageTheSame();
                    break;
                case 715:
                    kostenEtage = kostenEtageGesamt[714];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.85619938321094E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.9809673448339E+21f);
                    everEtageTheSame();
                    break;
                case 716:
                    kostenEtage = kostenEtageGesamt[715];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.9809673448339E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.11161786103099E+21f);
                    everEtageTheSame();
                    break;
                case 717:
                    kostenEtage = kostenEtageGesamt[716];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.11161786103099E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.26242479285592E+21f);
                    everEtageTheSame();
                    break;
                case 718:
                    kostenEtage = kostenEtageGesamt[717];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.26242479285592E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.41859258162939E+21f);
                    everEtageTheSame();
                    break;
                case 719:
                    kostenEtage = kostenEtageGesamt[718];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.41859258162939E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.56480326982276E+21f);
                    everEtageTheSame();
                    break;
                case 720:
                    kostenEtage = kostenEtageGesamt[719];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.56480326982276E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.73062969722796E+21f);
                    everEtageTheSame();
                    break;
                case 721:
                    kostenEtage = kostenEtageGesamt[720];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.73062969722796E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.90747359997911E+21f);
                    everEtageTheSame();
                    break;
                case 722:
                    kostenEtage = kostenEtageGesamt[721];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.90747359997911E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.10459627260064E+21f);
                    everEtageTheSame();
                    break;
                case 723:
                    kostenEtage = kostenEtageGesamt[722];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.10459627260064E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.30089051199155E+21f);
                    everEtageTheSame();
                    break;
                case 724:
                    kostenEtage = kostenEtageGesamt[723];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.30089051199155E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.52652352754434E+21f);
                    everEtageTheSame();
                    break;
                case 725:
                    kostenEtage = kostenEtageGesamt[724];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.52652352754434E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.77279667573883E+21f);
                    everEtageTheSame();
                    break;
                case 726:
                    kostenEtage = kostenEtageGesamt[725];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.77279667573883E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.02299711696753E+21f);
                    everEtageTheSame();
                    break;
                case 727:
                    kostenEtage = kostenEtageGesamt[726];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.02299711696753E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.26870743578389E+21f);
                    everEtageTheSame();
                    break;
                case 728:
                    kostenEtage = kostenEtageGesamt[727];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.26870743578389E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.55543110408423E+21f);
                    everEtageTheSame();
                    break;
                case 729:
                    kostenEtage = kostenEtageGesamt[728];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.55543110408423E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.8657518761103E+21f);
                    everEtageTheSame();
                    break;
                case 730:
                    kostenEtage = kostenEtageGesamt[729];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.8657518761103E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.1803906075646E+21f);
                    everEtageTheSame();
                    break;
                case 731:
                    kostenEtage = kostenEtageGesamt[730];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.1803906075646E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.50430230866096E+21f);
                    everEtageTheSame();
                    break;
                case 732:
                    kostenEtage = kostenEtageGesamt[731];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.50430230866096E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.86818793720144E+21f);
                    everEtageTheSame();
                    break;
                case 733:
                    kostenEtage = kostenEtageGesamt[732];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.86818793720144E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.27310751653334E+21f);
                    everEtageTheSame();
                    break;
                case 734:
                    kostenEtage = kostenEtageGesamt[733];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.27310751653334E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.68098319942685E+21f);
                    everEtageTheSame();
                    break;
                case 735:
                    kostenEtage = kostenEtageGesamt[734];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.68098319942685E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7.08757959098438E+21f);
                    everEtageTheSame();
                    break;
                case 736:
                    kostenEtage = kostenEtageGesamt[735];
                    PlayerPrefs.SetFloat("etage1CashGain", 7.08757959098438E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7.53135128653176E+21f);
                    everEtageTheSame();
                    break;
                case 737:
                    kostenEtage = kostenEtageGesamt[736];
                    PlayerPrefs.SetFloat("etage1CashGain", 7.53135128653176E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8.01989475497415E+21f);
                    everEtageTheSame();
                    break;
                case 738:
                    kostenEtage = kostenEtageGesamt[737];
                    PlayerPrefs.SetFloat("etage1CashGain", 8.01989475497415E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8.55665504136541E+21f);
                    everEtageTheSame();
                    break;
                case 739:
                    kostenEtage = kostenEtageGesamt[738];
                    PlayerPrefs.SetFloat("etage1CashGain", 8.55665504136541E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9.10751178544666E+21f);
                    everEtageTheSame();
                    break;
                case 740:
                    kostenEtage = kostenEtageGesamt[739];
                    PlayerPrefs.SetFloat("etage1CashGain", 9.10751178544666E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9.73628671305987E+21f);
                    everEtageTheSame();
                    break;
                case 741:
                    kostenEtage = kostenEtageGesamt[740];
                    PlayerPrefs.SetFloat("etage1CashGain", 9.73628671305987E+21f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.03593805265625E+22f);
                    everEtageTheSame();
                    break;
                case 742:
                    kostenEtage = kostenEtageGesamt[741];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.03593805265625E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.10447872548222E+22f);
                    everEtageTheSame();
                    break;
                case 743:
                    kostenEtage = kostenEtageGesamt[742];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.10447872548222E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.17078357511926E+22f);
                    everEtageTheSame();
                    break;
                case 744:
                    kostenEtage = kostenEtageGesamt[743];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.17078357511926E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.24594294374148E+22f);
                    everEtageTheSame();
                    break;
                case 745:
                    kostenEtage = kostenEtageGesamt[744];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.24594294374148E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.32631523851394E+22f);
                    everEtageTheSame();
                    break;
                case 746:
                    kostenEtage = kostenEtageGesamt[745];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.32631523851394E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.41321069033611E+22f);
                    everEtageTheSame();
                    break;
                case 747:
                    kostenEtage = kostenEtageGesamt[746];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.41321069033611E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.50095879438978E+22f);
                    everEtageTheSame();
                    break;
                case 748:
                    kostenEtage = kostenEtageGesamt[747];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.50095879438978E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.59654929742135E+22f);
                    everEtageTheSame();
                    break;
                case 749:
                    kostenEtage = kostenEtageGesamt[748];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.59654929742135E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.70404182064771E+22f);
                    everEtageTheSame();
                    break;
                case 750:
                    kostenEtage = kostenEtageGesamt[749];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.70404182064771E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.807269429433E+22f);
                    everEtageTheSame();
                    break;
                case 751:
                    kostenEtage = kostenEtageGesamt[750];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.807269429433E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.92518148400952E+22f);
                    everEtageTheSame();
                    break;
                case 752:
                    kostenEtage = kostenEtageGesamt[751];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.92518148400952E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.04909228112146E+22f);
                    everEtageTheSame();
                    break;
                case 753:
                    kostenEtage = kostenEtageGesamt[752];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.04909228112146E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.18763448565141E+22f);
                    everEtageTheSame();
                    break;
                case 754:
                    kostenEtage = kostenEtageGesamt[753];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.18763448565141E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.32729929509081E+22f);
                    everEtageTheSame();
                    break;
                case 755:
                    kostenEtage = kostenEtageGesamt[754];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.32729929509081E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.48666616699676E+22f);
                    everEtageTheSame();
                    break;
                case 756:
                    kostenEtage = kostenEtageGesamt[755];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.48666616699676E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.65720973495947E+22f);
                    everEtageTheSame();
                    break;
                case 757:
                    kostenEtage = kostenEtageGesamt[756];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.65720973495947E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.82468328902895E+22f);
                    everEtageTheSame();
                    break;
                case 758:
                    kostenEtage = kostenEtageGesamt[757];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.82468328902895E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.0019355232675E+22f);
                    everEtageTheSame();
                    break;
                case 759:
                    kostenEtage = kostenEtageGesamt[758];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.0019355232675E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.1921547469821E+22f);
                    everEtageTheSame();
                    break;
                case 760:
                    kostenEtage = kostenEtageGesamt[759];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.1921547469821E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.41185013817373E+22f);
                    everEtageTheSame();
                    break;
                case 761:
                    kostenEtage = kostenEtageGesamt[760];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.41185013817373E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.64418280010661E+22f);
                    everEtageTheSame();
                    break;
                case 762:
                    kostenEtage = kostenEtageGesamt[761];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.64418280010661E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.88744768755664E+22f);
                    everEtageTheSame();
                    break;
                case 763:
                    kostenEtage = kostenEtageGesamt[762];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.88744768755664E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.14277489960818E+22f);
                    everEtageTheSame();
                    break;
                case 764:
                    kostenEtage = kostenEtageGesamt[763];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.14277489960818E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.40213656629471E+22f);
                    everEtageTheSame();
                    break;
                case 765:
                    kostenEtage = kostenEtageGesamt[764];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.40213656629471E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 4.70246174485347E+22f);
                    everEtageTheSame();
                    break;
                case 766:
                    kostenEtage = kostenEtageGesamt[765];
                    PlayerPrefs.SetFloat("etage1CashGain", 4.70246174485347E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.01135118417883E+22f);
                    everEtageTheSame();
                    break;
                case 767:
                    kostenEtage = kostenEtageGesamt[766];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.01135118417883E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.33011591218478E+22f);
                    everEtageTheSame();
                    break;
                case 768:
                    kostenEtage = kostenEtageGesamt[767];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.33011591218478E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 5.65530720481622E+22f);
                    everEtageTheSame();
                    break;
                case 769:
                    kostenEtage = kostenEtageGesamt[768];
                    PlayerPrefs.SetFloat("etage1CashGain", 5.65530720481622E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.02461361488001E+22f);
                    everEtageTheSame();
                    break;
                case 770:
                    kostenEtage = kostenEtageGesamt[769];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.02461361488001E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.41817836149684E+22f);
                    everEtageTheSame();
                    break;
                case 771:
                    kostenEtage = kostenEtageGesamt[770];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.41817836149684E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.81240705320911E+22f);
                    everEtageTheSame();
                    break;
                case 772:
                    kostenEtage = kostenEtageGesamt[771];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.81240705320911E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7.24290846102525E+22f);
                    everEtageTheSame();
                    break;
                case 773:
                    kostenEtage = kostenEtageGesamt[772];
                    PlayerPrefs.SetFloat("etage1CashGain", 7.24290846102525E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 7.70657057572226E+22f);
                    everEtageTheSame();
                    break;
                case 774:
                    kostenEtage = kostenEtageGesamt[773];
                    PlayerPrefs.SetFloat("etage1CashGain", 7.70657057572226E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8.21077711325926E+22f);
                    everEtageTheSame();
                    break;
                case 775:
                    kostenEtage = kostenEtageGesamt[774];
                    PlayerPrefs.SetFloat("etage1CashGain", 8.21077711325926E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 8.74643721040662E+22f);
                    everEtageTheSame();
                    break;
                case 776:
                    kostenEtage = kostenEtageGesamt[775];
                    PlayerPrefs.SetFloat("etage1CashGain", 8.74643721040662E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9.2759567456994E+22f);
                    everEtageTheSame();
                    break;
                case 777:
                    kostenEtage = kostenEtageGesamt[776];
                    PlayerPrefs.SetFloat("etage1CashGain", 9.2759567456994E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 9.87077809538688E+22f);
                    everEtageTheSame();
                    break;
                case 778:
                    kostenEtage = kostenEtageGesamt[777];
                    PlayerPrefs.SetFloat("etage1CashGain", 9.87077809538688E+22f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.05436245863246E+23f);
                    everEtageTheSame();
                    break;
                case 779:
                    kostenEtage = kostenEtageGesamt[778];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.05436245863246E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.12699260611272E+23f);
                    everEtageTheSame();
                    break;
                case 780:
                    kostenEtage = kostenEtageGesamt[779];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.12699260611272E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.19965739372387E+23f);
                    everEtageTheSame();
                    break;
                case 781:
                    kostenEtage = kostenEtageGesamt[780];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.19965739372387E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.27584013093463E+23f);
                    everEtageTheSame();
                    break;
                case 782:
                    kostenEtage = kostenEtageGesamt[781];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.27584013093463E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.36449490119416E+23f);
                    everEtageTheSame();
                    break;
                case 783:
                    kostenEtage = kostenEtageGesamt[782];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.36449490119416E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.45169499643646E+23f);
                    everEtageTheSame();
                    break;
                case 784:
                    kostenEtage = kostenEtageGesamt[783];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.45169499643646E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.54481722598462E+23f);
                    everEtageTheSame();
                    break;
                case 785:
                    kostenEtage = kostenEtageGesamt[784];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.54481722598462E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.64363738050565E+23f);
                    everEtageTheSame();
                    break;
                case 786:
                    kostenEtage = kostenEtageGesamt[785];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.64363738050565E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.74362085240266E+23f);
                    everEtageTheSame();
                    break;
                case 787:
                    kostenEtage = kostenEtageGesamt[786];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.74362085240266E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.85770127444812E+23f);
                    everEtageTheSame();
                    break;
                case 788:
                    kostenEtage = kostenEtageGesamt[787];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.85770127444812E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 1.97154438027051E+23f);
                    everEtageTheSame();
                    break;
                case 789:
                    kostenEtage = kostenEtageGesamt[788];
                    PlayerPrefs.SetFloat("etage1CashGain", 1.97154438027051E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.09760010596307E+23f);
                    everEtageTheSame();
                    break;
                case 790:
                    kostenEtage = kostenEtageGesamt[789];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.09760010596307E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.22157762895121E+23f);
                    everEtageTheSame();
                    break;
                case 791:
                    kostenEtage = kostenEtageGesamt[790];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.22157762895121E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.36399928280548E+23f);
                    everEtageTheSame();
                    break;
                case 792:
                    kostenEtage = kostenEtageGesamt[791];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.36399928280548E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.50315329471106E+23f);
                    everEtageTheSame();
                    break;
                case 793:
                    kostenEtage = kostenEtageGesamt[792];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.50315329471106E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.67339639039837E+23f);
                    everEtageTheSame();
                    break;
                case 794:
                    kostenEtage = kostenEtageGesamt[793];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.67339639039837E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 2.8409911116949E+23f);
                    everEtageTheSame();
                    break;
                case 795:
                    kostenEtage = kostenEtageGesamt[794];
                    PlayerPrefs.SetFloat("etage1CashGain", 2.8409911116949E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.02262325695916E+23f);
                    everEtageTheSame();
                    break;
                case 796:
                    kostenEtage = kostenEtageGesamt[795];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.02262325695916E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.21067422111788E+23f);
                    everEtageTheSame();
                    break;
                case 797:
                    kostenEtage = kostenEtageGesamt[796];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.21067422111788E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 3.43415816278419E+23f);
                    everEtageTheSame();
                    break;
                case 798:
                    kostenEtage = kostenEtageGesamt[797];
                    PlayerPrefs.SetFloat("etage1CashGain", 3.43415816278419E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 6.18430049665553E+23f);
                    everEtageTheSame();
                    break;
                case 799:
                    kostenEtage = kostenEtageGesamt[798];
                    PlayerPrefs.SetFloat("etage1CashGain", 6.18430049665553E+23f);
                    PlayerPrefs.SetFloat("etage1CashGainFuture", 12.18430049665553E+23f);
                    everEtageTheSame();
                    break;
                case 800:
                    kostenEtage = kostenEtageGesamt[799];
                    PlayerPrefs.SetFloat("etage1CashGain", 12.18430049665553E+23f);
                    everEtageTheSame();
                    break;
                default:
                    break;
            }
        }
        PlayerPrefs.Save();
    }
}