﻿using UnityEngine;
using UnityEngine.UI;

// Class that holds all Data Stuff like Cash, World 1 Stuff, and so on.
public class Data : MonoBehaviour {

    public Text cashText;
    public GameObject building;
    public GameObject firstBuySign;
    public GameObject buttonPanel;
    public GameObject arrowFirstBuy;
    public GameObject buyNewEtage;
    public float kostenEtageNew = 10;
    public Text buyNewEtageText;
    public GameObject etage2;
    public GameObject etage3;
    public GameObject etage4;
    public GameObject etage5;
    public GameObject etage6;
    public GameObject etage7;
    public GameObject etage8;
    public GameObject etage9;
    public GameObject etage10;

    // ----------------------------------------------------------------------- //
    // Call it upon Application Start
    void Start() {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.DeleteAll();

        if (PlayerPrefs.GetInt("etage", 0) == 1) {
            building.SetActive(true);
            arrowFirstBuy.SetActive(false);
        }
        if (PlayerPrefs.GetInt("etageOwn", 1) == 2) {
            etage2.SetActive(true); 
            building.GetComponent<RectTransform>().offsetMin= new Vector2(-276.86f, 100);//right = 0
            building.GetComponent<RectTransform>().offsetMax = new Vector2(-276.86f, 100);//left = 1
        }
    }

    public void getEnoughCoins() {
        PlayerPrefs.SetFloat("cash", 9999999999);
    }

    // Called every frame - updates itself
    void Update() {
        // 2 Nachkommastellen für den cash
        cashText.text = System.Math.Round(PlayerPrefs.GetFloat("cash", 10), 2).ToString();
        // Cash >= 1.000 == 1k
        if (PlayerPrefs.GetFloat("cash") >= 1000) {
            Debug.Log("1k");
            cashText.text = System.Math.Round((PlayerPrefs.GetFloat("cash") / 1000), 1).ToString() + "k";
        }
        // Cash >= 1.000.000 == 1m
        if (PlayerPrefs.GetFloat("cash") >= (1000*1000)) {
            cashText.text = System.Math.Round((PlayerPrefs.GetFloat("cash") / 10000), 0).ToString() + "m";
            Debug.Log("1m");
        }
        // Cash >= 1.000.000.000 == 1t
        if (PlayerPrefs.GetFloat("cash") >= (1000 * 1000 * 1000)) {
            cashText.text = System.Math.Round((PlayerPrefs.GetFloat("cash") / 10000000), 0).ToString() + "t";
            Debug.Log("1m");
        }
        // -------------------------------------------------------------------------------------------- //
        if (PlayerPrefs.GetInt("levelEtage1") >= 10) {
            buyNewEtage.SetActive(true);
            buyNewEtageText.text = kostenEtageNew.ToString();
        }
        if (PlayerPrefs.GetInt("etageOwn", 1) == 2) {
            etage2.SetActive(true);
        }
    }

    // Buy First Etage (On Start)
    public void buyEtage() {
        GameObject.Find("SFX-Control").GetComponent<AudioSource>().Play();
        switch (PlayerPrefs.GetInt("etage", 0)) {
            case 0:
                arrowFirstBuy.SetActive(false);
                PlayerPrefs.SetFloat("cash", PlayerPrefs.GetFloat("cash", 10) - 10);
                cashText.text = PlayerPrefs.GetFloat("cash").ToString();
                PlayerPrefs.SetInt("etage", 1);
                PlayerPrefs.Save();
                building.SetActive(true);
                firstBuySign.SetActive(false);
                buttonPanel.SetActive(true);
                break;
            case 1:
                break;
            default: break;
        }
    }

    // Buy Etage 2 (World 1)
    public void buyNewEtageMethod() {
        if (PlayerPrefs.GetFloat("cash", 0) >= PlayerPrefs.GetFloat("MaxEtagenKosten", 10)) {
            PlayerPrefs.SetFloat("cash", PlayerPrefs.GetFloat("cash") - 10);
            PlayerPrefs.SetInt("etageOwn", 2);
            PlayerPrefs.Save();
            building.GetComponent<RectTransform>().sizeDelta = new Vector2(3.242493e-05f, 67.3f);
        }
    }
}
