﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TreppenWorker : MonoBehaviour {

    float xStandard = -289f;
    float yStandard = -179f;
    Boolean goLeft = true;
    Crate crateScript;
    float workerValueTreppe;
    public Text valueText;
    public Text containerText;
    public float containerValue = 0;

    // Use this for initialization
    void Start()  {
        StartCoroutine(movement());
        GameObject crate = GameObject.FindGameObjectWithTag("crate");
        crateScript = crate.GetComponent<Crate>();
    }

    // Update is called once per frame
    void Update() {
        //Immer im vordergrund
        transform.SetAsLastSibling();
    }

    // The standard Update method.
    IEnumerator movement() {
        if (goLeft) {
            xStandard -= 0.05f;
            yStandard -= 0.05f;
            transform.position += Vector3.left * xStandard * 0.02f;
            transform.position += Vector3.down * yStandard * 0.02f;
            yield return new WaitForSeconds(0.05f);
        }
        else {
            xStandard += 0.05f;
            yStandard += 0.05f;
            transform.position += Vector3.right * xStandard * 0.02f;
            transform.position += Vector3.up * yStandard * 0.02f;
            yield return new WaitForSeconds(0.05f);
        }
        StartCoroutine(movement());
    }

    void OnCollisionEnter2D(Collision2D coll)  {
        if (coll.gameObject.tag == "crate") {
            this.goLeft = false;         
            this.workerValueTreppe = crateScript.crateValue;
            valueText.text = workerValueTreppe.ToString() + " $";
            crateScript.crateText.text = "0 $";
            this.crateScript.crateValue = 0;
        }
        if (coll.gameObject.tag == "containerLeft"){
            this.goLeft = true;
            this.containerValue += workerValueTreppe;
            containerText.text = containerValue.ToString() + " $";
            this.workerValueTreppe = 0;
            valueText.text = "0 $";
        }
    }
}
