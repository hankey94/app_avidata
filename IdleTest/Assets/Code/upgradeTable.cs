﻿using UnityEngine.UI;
using UnityEngine;
using System;


public class upgradeTable : MonoBehaviour {

    //Wichtige Werte für die einzelnen Teile
    public int levelStockwerk1 = 1;
   // float speedFlaskStockwerk1;
    int workerStockwerk1 = 1;

    public float valueFlaskStockwerk1 = 0.33f;

    float kostenStockwerk1 = 1.16f;

    public Button btn_upgradeStockwerk1;
    public Text upgradeTextStockwerk1;

    public AudioClip clip;
    Button btn;
    ClickerValue clickerValue;

    // ----------------------------------- //

    // Use this for initialization
    void Start () {
        btn = btn_upgradeStockwerk1.GetComponent<Button>();
        btn.onClick.AddListener(onClick);
        btn.gameObject.AddComponent<AudioSource>();
        btn.GetComponent<AudioSource>().clip = clip;
        GameObject main = GameObject.Find("MAIN");
        clickerValue = main.GetComponent<ClickerValue>();
    }

    // BUTTON METHOD
    void onClick()  {
        kosten();
        if (clickerValue.clickerValueMax >= kostenStockwerk1) {
            btn.GetComponent<AudioSource>().Play();
            levelStockwerk1++;
            clickerValue.clickerValueMax -= kostenStockwerk1;
            clickerValue.txt_clickerValueMax.text = clickerValue.clickerValueMax.ToString();
            valueFlaskStockwerk1 = 0.4f;
            upgradeTextStockwerk1.text = levelStockwerk1.ToString();
        }
    }

    // UPGRADES DER STOCKWERKE
    void kosten()   {
        switch (levelStockwerk1)  {
            case 1: kostenStockwerk1 = 1.16f; break;
            case 2: kostenStockwerk1 = 1.31f; break;
            case 3: kostenStockwerk1 = 1.5f; break;
            case 4: kostenStockwerk1 = 1.76f; break;
            case 5: kostenStockwerk1 = 2.04f; break;
            case 6: kostenStockwerk1 = 2.38f; break;
            case 7: kostenStockwerk1 = 2.76f; break;
            case 8: kostenStockwerk1 = 3.14f; break;
            case 9: kostenStockwerk1 = 3.49f; clickerValue.worker2.SetActive(true);  break;
            case 10: kostenStockwerk1 = 4.01f; break;
            case 11: kostenStockwerk1 = 4.7f; break;
            case 12: kostenStockwerk1 = 5.33f; break;
            case 13: kostenStockwerk1 = 6.03f; break;
            case 14: kostenStockwerk1 = 6.93f; break;
            case 15: kostenStockwerk1 = 7.82f; break;
        }
    }
}
