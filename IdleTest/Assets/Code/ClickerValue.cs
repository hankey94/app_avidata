﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using System;

public class ClickerValue : MonoBehaviour {
    // INITIALIZING - VALUES
    float clickerValue = .75f;
    public float clickerValueMax;
    int diamonds = 1000;

    int valueLevelIndex = 1;
    long upgradePrice = 0;

    DateTime savedTime;
    DateTime currentTime;
    long temp;
    TimeSpan difference;
    float exitValue;

    // Graphics
    Image flask;
    Boolean firstBuild = false;
    public GameObject erdgeschoss, etage1, dach;
    public GameObject worker2;

    // EXCEL EXCTRACTION
    //string excel = "Driver={Microsoft Excel Driver (*.xls)};DriverId=790;Dbq=yourexcelfile.xls;";

    // INITIALIZING - UI
    public Text txt_clickerValue;
    public Text txt_clickerValueMax;
    public Text txt_diamonds;
    public Text txt_upgradePrice;
    public Button btn_upgrade;
    // ------------------------------------------------ //

    // Use this for initialization
    void Start () {
        //FIRST START AND SET THE CAMERA TO FIX SCREEN SIZES
        //Camera cam = this.GetComponent<Camera>();
       // cam.aspect = 4f / 3f;

        // button init
        Button btn = btn_upgrade.GetComponent<Button>();
        btn.onClick.AddListener(onClick);
        worker2 = GameObject.Find("MAIN");

        // Current Time to collect idle coins
        currentTime = System.DateTime.Now;
        temp = Convert.ToInt64(PlayerPrefs.GetString("exitTime"));
        savedTime = DateTime.FromBinary(temp);
        difference = currentTime.Subtract(savedTime);
        exitValue = (int)((float)difference.TotalSeconds)*clickerValue;
       // clickerValueMax += exitValue;

        // Update Method start
       // StartCoroutine(clicker());

        // .2f
        clickerValue = Mathf.Round(clickerValue * 100.0f) / 100.0f;
        clickerValueMax = Mathf.Round(clickerValueMax * 100.0f) / 100.0f;

        // change UI text at app start
        txt_upgradePrice.text = upgradePrice.ToString() + " $";
        txt_diamonds.text = diamonds.ToString();
    }
    
    // UPGRADES DER STOCKWERKE
    void valueLevel() {
        switch (valueLevelIndex)  {
            case 1: clickerValue = 1.5f; upgradePrice = 0; break;
            case 2: clickerValue = 3f;  upgradePrice = 100;  break;
            case 3: clickerValue = 6f;  upgradePrice = 3000; break;
            case 4: clickerValue = 12f; upgradePrice = 60000; break;
            case 5: clickerValue = 24f; upgradePrice = 1200000; break;
            case 6: clickerValue = 1.5f; upgradePrice = 240000000; break;
            case 7: clickerValue = 3f; upgradePrice = 48000000000; break;
            case 8: clickerValue = 6f; upgradePrice = 960000000000; break;
            case 9: clickerValue = 12f; upgradePrice = 19200000000000; break;
            case 10: clickerValue = 24f; upgradePrice = 384000000000000; break;
        }
    }

    // BUTTON METHOD
    void onClick() {
        if (clickerValueMax >= upgradePrice){
            //BEIM ERSTAUFRUF UND ERSTUPGRADE DAS HAUS BAUEN
            if(valueLevelIndex == 1) {
                erdgeschoss.SetActive(true);
                dach.SetActive(true);
                etage1.SetActive(true);
                firstBuild = true;
            }
            //clickerValueMax -= upgradePrice;
            valueLevelIndex++;
            valueLevel();
            txt_upgradePrice.text = upgradePrice.ToString() + " $";
        }
    }

    // Save current time
  /*  void OnApplicationQuit() {    
        PlayerPrefs.SetString("exitTime", System.DateTime.Now.ToBinary().ToString());
        PlayerPrefs.SetFloat("maxCoinValue", clickerValueMax);
        PlayerPrefs.SetFloat("coinValueCount", clickerValue);
        //level fehlt
        //diamonds fehlen
    }*/
}