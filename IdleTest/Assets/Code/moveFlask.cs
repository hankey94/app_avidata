﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using System;

public class moveFlask : MonoBehaviour {

    float pos = 409;
    // public GameObject flask;
    //  public GameObject flaskPrefab;
    Boolean movementBool = false;
    Boolean waitBool = false;
    Boolean waitBool2 = false;
    Text percent;
    Text percent2;
    GameObject prefabObject;
    GameObject prefabPanel;
    upgradeTable table;
    bool flask2 = false;

    // Use this for initialization
    void Start () {
        percent = GameObject.FindWithTag("percentText1").GetComponent<Text>();
        prefabPanel = GameObject.FindWithTag("house");
        GameObject container = GameObject.Find("MAIN");
        table = container.GetComponent<upgradeTable>();
        //  flask = GameObject.FindGameObjectWithTag("flask");
        // Instantiate(flaskPrefab, flask.transform.position, flask.transform.rotation);
    }
	
	// Update is called once per frame
	void Update () {
        //Immer im vordergrund
        transform.SetAsLastSibling();

        if(table.levelStockwerk1 == 10 && flask2 == false) {
            percent2 = GameObject.FindWithTag("percentText2").GetComponent<Text>();
            flask2 = !flask2;
            prefabObject = (GameObject)Instantiate(Instantiate(Resources.Load("flask")), new Vector3(409, 104, 0), Quaternion.identity);
            prefabObject.transform.SetParent(prefabPanel.transform, false);
            prefabObject.name = "flask2";
        }
    }

    void OnCollisionEnter2D(Collision2D coll)  {
        if (coll.gameObject.tag == "crate") {
            Destroy(this.gameObject);
            StopCoroutine(flaskMovement());
            prefabObject = (GameObject)Instantiate(Instantiate(Resources.Load("flask")), new Vector3(409, 104, 0), Quaternion.identity);
            prefabObject.transform.SetParent(prefabPanel.transform, false);
            prefabObject.name = "flask";
            flask2 = false;
        }
        if (coll.gameObject.tag == "worker") {
            StartCoroutine(wait());
            waitBool = !waitBool;
        }
        if (coll.gameObject.tag == "fliess") {
            if (movementBool == false && waitBool == false)   {
                StartCoroutine(flaskMovement());
                movementBool = !movementBool;
            }
        }
    }

    // The standard Update method.
    IEnumerator flaskMovement()  {
        while (!waitBool) {
            pos -= 0.05f;
            transform.position += Vector3.left * pos * 0.02f;
            yield return new WaitForSeconds(0.05f);
        }              
    }

    // The standard Update method.
    IEnumerator wait()  {
        StopCoroutine(flaskMovement());

       
            for (int i = 0; i <= 100; i++)
            {
                percent.text = i.ToString() + " %";
                yield return new WaitForSeconds(0.02f);
            }
            percent.text = "";
            waitBool = !waitBool;
            StartCoroutine(flaskMovement());
            //  }
            /* else {
                 for (int i = 0; i <= 100; i++)  {
                     //percent2.text = i.ToString() + " %";
                     yield return new WaitForSeconds(0.02f);
                 }
                 percent2.text = "";
                 waitBool2 = !waitBool2;
                 StartCoroutine(flaskMovement());
             }*/
        
    }
}
