﻿using UnityEngine;

public class Managers : MonoBehaviour {

    public GameObject managerEtage1;
    public GameObject arrow;
    public GameObject managerClickZone;

    public GameObject managerElevator;
    public GameObject managerClickZoneElevator;
    public GameObject arrow2;
    public GameObject managerKeller;
    public GameObject managerClickZoneKeller;
    public GameObject arrow3;

    public GameObject zoneTrucker;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (PlayerPrefs.GetInt("levelEtage1") == 2) {
            // SHOW MANAGER SILHOUETTE: ETAGE 1
            managerEtage1.SetActive(true);
            managerClickZone.SetActive(true);
        }
        if (PlayerPrefs.GetInt("automaticEtage1_2", 1) == 2) {
            // SHOW MANAGER SILHOUETTE: ELEVATOR
            managerClickZoneElevator.SetActive(true);
            managerElevator.SetActive(true);
        }
        if (PlayerPrefs.GetInt("automaticEtage1_3", 1) == 2)  {
            // SHOW MANAGER SILHOUETTE: KELLER
            managerClickZoneKeller.SetActive(true);
            managerKeller.SetActive(true);
        }
    }

    public void tutorialManagerClick()  {
        arrow.SetActive(false);
        managerElevator.SetActive(true);
        managerClickZoneElevator.SetActive(true);
        // EINSCHALTEN DER AUTO FUNKTION
        PlayerPrefs.SetInt("automaticEtage1", 1);
        PlayerPrefs.Save();
    }

    public void tutorialManagerClickElevator() {
        arrow2.SetActive(false);
        managerKeller.SetActive(true);
        managerClickZoneKeller.SetActive(true);
        zoneTrucker.SetActive(false);
        // EINSCHALTEN DER AUTO FUNKTION
        PlayerPrefs.SetInt("automaticEtage1_2", 1);
        PlayerPrefs.Save();
    }

    public void tutorialManagerClickKeller()  {
        arrow3.SetActive(false);
        // EINSCHALTEN DER AUTO FUNKTION
        PlayerPrefs.SetInt("automaticEtage1_3", 1);
        PlayerPrefs.Save();
    }
}
