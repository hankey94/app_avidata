﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Elevator : MonoBehaviour {

    public float coinsFromEtage1;
    private bool elevatorDrive = false;
    public GameObject arrow;
    public Text elevatorText;
    public GameObject trucker;
    public Text machineText;
    public GameObject truckerZone;

    // INITIALIZE STUFF
    void Start () {
        PlayerPrefs.SetInt("automaticElevatorON", 0);
    }

    // CHECK UPDATE STUFF
	void Update () {
        // START AUTOMATIC INSTANCE AND DONT LET THEM TOUCH THE WORKERS
        if (PlayerPrefs.GetInt("automaticEtage1_2", 0) == 1)  {
            elevatorDrive = true;
            if (PlayerPrefs.GetInt("automaticElevatorON", 0) == 0) {
                StartCoroutine(elevatorMovement());
                PlayerPrefs.SetInt("automaticElevatorON", 1);
            }
        }
    }

    // MANUALLY START THE ELEVATOR
    public void startElevator() {
        GameObject.Find("SFX-Control").GetComponent<AudioSource>().Play();
        if (!elevatorDrive) {
            arrow.SetActive(false);
            elevatorDrive = true;
            StartCoroutine(elevatorMovement());
        }
    }

    // Controls the elevator movement for 1 etage
    IEnumerator elevatorMovement() {
        // COLLECT COINS
        coinsFromEtage1 += GameObject.Find("HARDWORKER (1)").GetComponent<WorkerNew>().coinsEtage1Value;
        GameObject.Find("HARDWORKER (1)").GetComponent<WorkerNew>().coinsEtage1Value -= coinsFromEtage1;
        GameObject.Find("HARDWORKER (1)").GetComponent<WorkerNew>().coinseEtage1Text.text = "0";
        elevatorText.text = coinsFromEtage1.ToString();
        // GO DOWN
        for (float i = 208; i > -153.8f; i -= 3) {
            GameObject.Find("elevator101").GetComponent<Transform>().localPosition = new Vector3(-713.7f, i, -32000);
            yield return new WaitForSeconds(0.01f);
        }
        // SET INCOME TO 0 AND CHANGE TEXT 
        trucker.GetComponent<Trucker>().truckerMoney += coinsFromEtage1;
        machineText.text = trucker.GetComponent<Trucker>().truckerMoney.ToString();
        elevatorText.text = "0";
        coinsFromEtage1 = 0;
        // GO UP
        yield return new WaitForSeconds(.5f);
        for (float i = -153.8f; i < 208; i += 3)  {
            GameObject.Find("elevator101").GetComponent<Transform>().localPosition = new Vector3(-713.7f, i, -32000);
            yield return new WaitForSeconds(0.01f);
        }
        // SET ELEVATOR CLICKABLE AGAIN
        elevatorDrive = false;
        if (PlayerPrefs.GetInt("firstTrucker", 0) == 0)  {
            trucker.SetActive(true);
            truckerZone.SetActive(true);
        }
        PlayerPrefs.SetInt("firstTrucker", 1);
        PlayerPrefs.Save();

        if (PlayerPrefs.GetInt("automaticEtage1_2", 0) == 1){
            yield return new WaitForSeconds(1f);
            StartCoroutine(elevatorMovement());
        }
    }
}
